# This file is part of the second Python Lambda Experiment (pyle2),
# Copyright (C) 2020 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

JUNK = *~ */*~ *.pyc __pycache__ TAGS depgraph.* *.log
SRCS = *.py */*.py */*.l
INSTALLBASE = /opt/w21
LIBDIR = $(INSTALLBASE)/lib/pyle2
BINDIR = $(INSTALLBASE)/bin
SRC = builtin functions l lcore objects pglobal.py pio pyle2.py utils
VERSION = $(shell git describe --dirty)

default: tags test

# also runs regtests, in between
test:
	pypy3 -m unittest discover ./unittests

coverage:
	python3 -m coverage run -m unittest discover ./unittests
	python3 -m coverage report -m

py:
	which -a coverage
	python3

nextrt:
	thenext=$$(ls lingo-regtests/* | head -1)              ;\
	git mv $$thenext regtests/                            && \
	git commit -m "next regtest "$$(basename $$thenext)

graph:
	scripts/print-lcore-deps.l lcore/*.l     |\
            tee depgraph.raw                     | \
	    scripts/mkgraph.py                   |  \
	    tee depgraph.dot                     |   \
	    dot -Tpng > depgraph.png

tags: $(SRCS)
	etags $(SRCS)

count:
	wc $(SRCS)

install:
	rm -f $(BINDIR)/pc
	rm -f $(BINDIR)/pyle2
	rm -rf $(LIBDIR)
	mkdir $(LIBDIR)
	cp -a $(SRC) $(LIBDIR)
	sed -e s,%LIBDIR%,$(LIBDIR),g -e s,%VERSION%,$(VERSION),g  \
		scripts/pyle.sh-template > $(BINDIR)/pyle2
	sed -e s,%LIBDIR%,$(LIBDIR),g scripts/pc.sh-template > $(BINDIR)/pc
	sed -e s,%UNRELEASED%,$(VERSION),g pglobal.py > $(LIBDIR)/pglobal.py
	chmod +x $(BINDIR)/pc $(BINDIR)/pyle2

clean:
	rm -rf $(JUNK)

push:
	git push --all
	git push --tags
