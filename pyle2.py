#!/usr/bin/env pypy3 -O
# Pyle2 -- 2nd Python Lambda Experiment, main driver program

import io
import os
import sys
import platform
import jpylib as y

import pglobal

# predeclare
ovc = y.Namespace(recursion_limit=10000, raise_exceptions=False,
                  expression=None, trace_events=[], print_loading=False)


def init_all(interactive=False):
    """Do all initialisations. After that, pglobal.Eval() is fully functional.

    This is here so the initialisation can be done more easily from the test
    harness.

    """
    sys.setrecursionlimit(ovc.recursion_limit)
    y.alert_config(decoration=("Error:", ";;", None, "DBG", "TRC"))

    pglobal.raise_exceptions = ovc.raise_exceptions
    pglobal.basedir = os.path.dirname(os.path.realpath(sys.argv[0]))
    pglobal.interactive = interactive
    pglobal.print_loading = ovc.print_loading

    if interactive and y.is_notice():
        print("Pyle2", pglobal.pyle_version, "on",
              platform.python_implementation(), platform.python_version(),
              end=" ", flush=True)

    import objects

    for events in ovc.trace_events:
        for event in events.split(","):
            objects.Symbol("trace-"+event).bindValue(objects.T)
    
    import lcore
    import utils

    l_dir = os.path.join(pglobal.basedir, "l")
    pglobal.LoadPath.setValue(utils.Seq2List(map(objects.String,
                                                 [".", l_dir])))


def main():
    """Will be called if this module is run as a stand-alone program.

    It may be useful to call it from testing, but we'll see about that.
    """
    global ovc
    ovc, args = y.pgetopts({
        "_help_header": "The 2nd Python Lambda Experiment",
        "v": ("verbose", y.alert_level_up, y.alert_level(y.L_NOTICE),
              "increase verbosity (default: notice; then info, debug, trace)"),
        "r": ("recursion_limit", int, 10000, "set recursion limit"),
        "q": ("quiet", y.alert_level_zero, False, "no alerts except errors"),
        "e": ("expression", str, None, "expression to evaluate"),
        "E": ("raise_exceptions", bool, False,
              "re-raise exceptions for debugging; twice to print extra"),
        "l": ("load_files", str, [], "load one or more files before starting",
              "filename"),
        "t": ("trace_events", str, [],
              "set event trace symbols (comma-separated"),
        ".": ("print_loading", bool, False,
              "print loading indicator on startup even if quiet"),
        "_arguments": ["[load_file args...]"],
        "_help_footer": """\
If no load_file argument is present, Pyle will run an interactive
input loop after processing the options.""",
    })
    
    init_all(not args)
    import pio
    import utils
    import objects

    for file in ovc.load_files:
        pio.load_from_path(file)

    if ovc.expression:
        if args:
            pglobal.CmdArgs.bindValue(utils.Seq2List(
                map(objects.String, args)))
        exit_status = 0
        try:
            value = pio.load_string(ovc.expression, "*expr-arg*")
            print(value)
        except pglobal.PyleException as pe:
            if ovc.raise_exceptions:
                raise pe
            exit_status = str(pe)
        sys.exit(exit_status)

    if args:
        file, *other = args                 # other args in sys:args OTL later
        pglobal.CmdArgs.bindValue(utils.Seq2List(map(objects.String, other)))
        expr = pio.load_file(file)
        if expr is None:
            sys.exit(1)
        else:
            print(expr)
    else:
        pio.repl(pio.Reader(pglobal.Stdin),
                 raise_exceptions=ovc.raise_exceptions)


if __name__ == "__main__":
    main()
else:
    init_all()                          # now without args and things
