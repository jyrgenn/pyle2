# global definitions for Pyle2

import jpylib as y

# This must never depend on anything else in Pyle2, so we don't drag in
# dependency cycles, but rather can use it to resolve those.

y.alert_level(0)

# characters that don't need quoting in a symbol name (and also the uppercase
# counterparts of the letters) -- although "#" and "," do if they are at the
# start of the name
sym_no_quote_chars \
    = "abcdefghijklmnopqrstuvwxyz0123456789!#$%&*+,.-/:<=>?@^_{}~"

# delimiter characters, the ones that end a symbol name
delimiter_chars = "(),'`\""

# special characters to be escaped in string literals and similar items

# backward: the escaped char after a backslash => real char
escape2specialChar = {
    "a": "\a", "b": "\b", "f": "\f", "n": "\n",
    "r": "\r", "t": "\t", "v": "\v", "\"": "\"", "\\": "\\",
}
specialChar2escaped = { ch: esc for esc, ch in escape2specialChar.items() }

# The Eval function, the bane of all dependency cycle avoiders.
Eval = None
EvalProgn = None

# the global environment
theEnvironment = None

# Symbol creation
Symbol = None

PyleException = None

# from ovc
raise_exceptions = None
print_loading = None

# the base directory from where to load things
basedir = "."

# Are we currently in an errset context? We are not, initially.
inErrset = False

# The maximum eval nesting level reached in current or last top-level eval.
maxEvalLevel = 0

# Are we running in interactive mode? May well be. The Reader knows.
interactive = False

# Number of eval calls during this program run so far.
evalCount = 0

# File name of REPL history file in ~/
replHistoryFile = ".pyle-history"

# Last error message constructed is bound to this symbol
LastError = None

# standard I/O ports
Stdin = None
Stdout = None
Stderr = None

# max. length of history file
max_history_length = 1000

pyle_version = "%UNRELEASED%"

# EOF
