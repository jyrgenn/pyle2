# context managers for Pyle2

from contextlib import contextmanager

import pglobal

@contextmanager
def newEnvironment(env=None):
    """Contect manager to run code in a new environment.

    The env argument, if specified, will be the parent of the new environment
    (as with lambdas, that carry their own one); otherwise the current
    environment is the parent (usually with let constructs).

    The new environment will be the current environment. When the context is
    left, the current environment will be restored to its previous value.

    """
    savedEnvironment = pglobal.theEnvironment
    pglobal.theEnvironment = pglobal.Environment(env or savedEnvironment)

    try:
        yield
    finally:
        pglobal.theEnvironment = savedEnvironment
    

@contextmanager
def variableAs(symbol, value):
    """Context manager to have a variable with a certain value.

    Previous value will be restored on leaving.
    """
    savedValue = symbol.getValue(False)
    symbol.setValue(value)
    try:
        yield
    finally:
        symbol.setValue(savedValue)


@contextmanager
def printContext(obj, seen_set):
    """Context manager for data structure printing, possibly cyclic.

    To avoid endless recursion, if the obj is already in the seen_set of
    currently being repr'ed objects, it is not returned in full string
    representation, but rather tersely indicating the recursion.

    If obj isn't in the set, it is put there, and the repr'ing commences.
    Finally the object is removed from the set again.

    """
    have_it = None
    if obj in seen_set:
        yield "[" + obj.pid() + "...]"
    else:
        seen_set.add(obj)
        try:
            yield
        finally:
            seen_set.remove(obj)

