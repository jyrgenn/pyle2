# smallish list stuff

import pglobal

class ListCollector():
    """Efficiently collect lists that are being appended to.

    This is done by keeping a reference to the list's last pair, so an append
    operation has constant effort, rather than linear, so the collecting of the
    list as a whole has linear effort, not quadratic.

    """
    
    def __init__(self, *args):
        """Create a new ListCollector.

        Any arguments are appended to the list as initial elements.

        """
        self.head = pglobal.Nil
        self.lastpair = None
        self.extend(args)

    def append(self, arg):
        """Append a new element to the list being collected."""
        assert isinstance(arg, pglobal.Object) ,\
                "lc.append() arg not an Object: {}".format(repr(arg))
        newpair = pglobal.Pair(arg, pglobal.Nil)
        if self.lastpair is None:
            self.head = newpair
        else:
            self.lastpair.rplacd(newpair)
        self.lastpair = newpair

    def extend(self, args):
        """Append the elements of ARGS (a list) to the list being collected."""
        for arg in args:
            self.append(arg)

    def lastcdr(self, arg):
        """Set the CDR of the last pair of the list being collected.

        After doing that, the list collection cannot continue without giving
        errneous results.

        """
        if not self.lastpair:
            self.lastpair = self.head = arg
        else:
            self.lastpair.rplacd(arg)

    def list(self):
        """Return the list that has been collected."""
        return self.head


def copyListLastpair(list):
    """Copy a list (shallow); return (newlist, lastpair).
    If list is improper, return (newlist, Nil)
    """
    lc = ListCollector()
    while list.isPair():
        head, list = list.cxr()
        lc.append(head)
    if list is pglobal.Nil:
        return lc.list(), lc.lastpair
    return lc.list(), pglobal.Nil


def Seq2Array(seq):
    """Convert  to a Python array."""
    result = []
    assert seq.isSequence(), "Seq2Array needs a sequence argument"
    for elem in seq:
        result.append(elem)
    return result


def Seq2String(seq):
    """Convert a sequence to a Python string."""
    return "".join(map(str, seq))


def Seq2List(seq):
    """Convert a sequence to a List."""
    return ListCollector(*seq).list()

