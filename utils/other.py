# other small helpers with no specific place

import jpylib as y

import pglobal
from objects import *

def dashify(name):
    """Replace underscores in the passed name by underscores.

    This is meant to convert Python function names to Pyle names.
    """
    return name.replace("_", "-")

def print_if_interactive(*args, **kwargs):
    "Print something iff running in interactive mode."
    if pglobal.interactive and y.is_notice():
        print(*args, **kwargs)

def boolOb(arg):
    "Return T if the argument is a true value in Python; Nil otherwise."
    if arg:
        return pglobal.T
    return pglobal.Nil
