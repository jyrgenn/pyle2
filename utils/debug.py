
import pglobal



def trap_if(condition, formatstr, *args, tag="", abort=False):
    """Print formatted message if condition is true.

    The condition refers to the value of a target language variable,
    which can be set from there.

    The condition arguments may be a string, in which case the
    corresponding # Symbol is used, or directly a symbol; the value
    bound to the symbol is the condition value, or false if none.

    Return True if the condition was.

    """

    if isinstance(condition, str):
        condition = pglobal.Symbol("trace-"+condition)
    if condition.isSymbol():
        condition_value = condition.getValue(must_exist=False)
    else:
        raise PyleException("trap_if: condition value not string or symbol: {}",
                            repr(condition))
    if condition_value:
        message = ""
        if tag:
            message = tag + " " + condition.name() + ": "
        if args:
            mapped = []
            for arg in args:
                if callable(arg):
                    # This is an optimisation for cases (like in theCaller that
                    # calls builtins) where the argument construction is
                    # expensive, so it can be delayed here. Turns out to be not
                    # premature at all (tips hat to Mr Shivers) and speeds up
                    # the whole thing by roughly a third (wow).
                    value = arg()
                else:
                    value = arg
                mapped.append(value)
            message += formatstr.format(*mapped)
        else:
            message += str(formatstr)
        print(message)
        if abort:
            raise PyleProgramAbort(message)
        return True
    return False


def print_if(condition, formatstr, *args):
    trap_if(condition, formatstr, *args)


def break_if(condition, formatstr, *args):
    if trap_if(condition, formatstr, *args, tag="BREAK"):
        input("press Enter to continue: ")


def abort_if(condition, formatstr, *args):
    trap_if(condition, formatstr, *args, tag="ABORT")


def trace_if(condition, formatstr, *args):
    trap_if(condition, formatstr, *args, tag="DEBUG")


def tracefn_if(symbol):
    """Decorator: trace decorated function's calls if symbol's value is true.
    """
    def the_decorator(func):
        def wrapper(*args, **kwargs):
            def kwargslist():
                if kwargs:
                    return ", "+", ".join(
                        [f"{k}={repr(v)}" for k, v in kwargs.items()])
                else:
                    return ""

            trap_if(symbol, "{}({}{})", func.__name__,
                    lambda: ", ".join(map(repr, args)),
                    kwargslist, tag="CALL")
            return func(*args, **kwargs)
        return wrapper
    return the_decorator
