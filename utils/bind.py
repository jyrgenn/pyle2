# binding

import pglobal


def bindValues(params, values):
    """Bind values to parameters.

    params may be a list, proper or not, or a symbol. The Right Thing will
    be done in each case. See the docstring for the lambda builtin if
    necessary. values must be a list.

    The length of the lists is not checked, rather remaining values are bound to
    the list-ending symbol of params (if any) or ignored; remaining params will
    be bound to Nil. This will not happen for function calls, as the number of
    arguments is checked there.

    Other than with the previous attempt this does not try a fully destructuring
    bind. To be honest I haven't much felt the urge to use it.

    """
    #print("bind", params, "to", values)
    if params and params.isSymbol():    # the most common case, I guess
        params.bindValue(values)
        return
    while params.isPair() and values.isPair():
        param, params = params.cxr()
        value, values = values.cxr()
        if param.isSymbol():
            param.bindValue(value)
        else:
            raise pglobal.PyleArgTypeError(
                "binding", "parameter", param, "symbol")
    # remaining params will be bound to Nil; as arguments are checked on a
    # function call, this will not happen there
    while params.isPair():
        param, params = params.cxr()
        param.bindValue(pglobal.Nil)
    if params:
        if params.isSymbol():
            params.bindValue(values)
        else:
            raise pglobal.PyleArgTypeError(
                "binding", "parameter", param, "symbol")
    # remaining values will not be bound
