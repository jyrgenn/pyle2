# os interfaces, as far as they need an abstraction

import os
import atexit
import readline

import pglobal


def _maybe_in_home(pathname):
    """Maybe treat the pathname as being in the home directory.

    If the pathname does not contain a slash, it is taken as relative to the
    home directory and returned as such. If there is no HOME defined in the
    environment, the pathname is returned unmodified.

    """
    if "/" not in pathname:
        homedir = os.environ.get("HOME")
        if homedir:
            pathname = os.path.join(homedir, pathname)
    return pathname


def read_history_file(pathname, save_on_exit=True):

    """Read the specified history file into readline.

    If the pathname does not contain a slash, it is taken as relative to the
    home directory. If there is no HOME defined in the environment, the file
    will not be read.

    If save_on_exit is defined as a Python function, it will be registered as an
    atexit handler.

    """
    # Due to the bizarre errors thrown by readline.read_history_file(), I am
    # doing this by myself.
    histfile = _maybe_in_home(str(pathname))
    try:
        with open(histfile) as f:
            for line in f:
                line = line.rstrip()
                if line:
                    readline.add_history(line)
    except FileNotFoundError:
        open(histfile, "w").close()     # touch it, as we want to chmod anyway
    os.chmod(histfile, 0o600)
    if save_on_exit:
        atexit.register(save_history_file, histfile)
        

def save_history_file(pathname):
    histfile = _maybe_in_home(str(pathname))
    try:
        with open(histfile, "w") as f:
            os.chmod(histfile, 0o600)
            index = 1
            for _ in range(readline.get_current_history_length()):
                item = readline.get_history_item(index).rstrip()
                if item:
                    print(readline.get_history_item(index), file=f)
                index += 1
    except OSError:
        pass

