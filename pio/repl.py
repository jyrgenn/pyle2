# the read-eval-print loop for Pyle2

import os
import datetime
import readline
import jpylib as y

import pglobal
import utils
from objects import *
from . import *


def repl(reader, prompt="> ", raise_exceptions=True):
    """Pyle2 interactive read-eval-print loop.
    
    The repl shall be implemented in the target language eventually, but until
    the target language is capable enough, I need this one.

    Return the last value on EOF.

    """
    histfname = os.path.join(y.real_home, pglobal.replHistoryFile)
    if os.path.exists(histfname):
        readline.read_history_file(histfname)
    value = None
    while True:
        try:
            pglobal.maxEvalLevel = 0
            form = reader.read(prompt)
            y.trace("have read", form)
            if form is None:
                print()
                return value
            
            startcount = pglobal.evalCount
            starttime = datetime.datetime.now()
            value = pglobal.Eval(form, expandMacros=True)
            endtime = datetime.datetime.now()
            n_evals = pglobal.evalCount - startcount
            
            print()
            print(repr(value))
            # I'm coming back for...
            seconds = (endtime - starttime).total_seconds()

            print(";; {} evals in {:5f} s ({:.2f} evals/s), max. eval level {}"
                  .format(n_evals, seconds, n_evals / seconds,
                          pglobal.maxEvalLevel))
        except KeyboardInterrupt as e:
            if raise_exceptions:
                raise e
        except Exception as e:
            print(e)
            if raise_exceptions:
                raise e
        finally:
            readline.write_history_file(histfname)


def read_loop(reader):
    value = Nil
    while True:
        expr = reader.read()
        if expr is None:
            return value
        value = pglobal.Eval(expr, expandMacros=True)


def load_string(s, name=None):
    """Evaluate expressions from a string. Return the value of the last.
    """
    reader = Reader(StringInputPort(s, name))
    return read_loop(reader)


def load_file(fname, missing_ok=False, silent=False):
    """Load and evaluate expressions from a file. Return T if all was fine.

    If missing_ok is true, return Nil if the file was not found. Otherwise (the
    default), raise a PyleLoadFileNotFound error in this case.

    """
    try:
        with open(fname) as f:
            utils.trace_if("load", "{}", fname)
            assert pglobal.CurrentLoadFile is not None, \
                "pglobal.CurrentLoadFile is " + pglobal.CurrentLoadFile
            with variableAs(pglobal.CurrentLoadFile, String(fname)):
                if not silent and pglobal.PrintLoadNotice.getValue():
                    y.notice("loading file", fname)
                reader = Reader(Port(f, fname))
                read_loop(reader)
                return T
    except OSError as e:
        if not missing_ok:
            raise PyleLoadFileNotFound(fname)
        return Nil


def load_from_path(fname, missing_ok=False, silent=False):
    load_path = pglobal.LoadPath.getValue()
    if "/" in fname or not load_path:
        return load_file(fname, missing_ok, silent)
    for dir in load_path:
        for suffix in ("", ".l"):
            fullpath = os.path.join(str(dir), fname + suffix)
            found = load_file(fullpath, missing_ok=True, silent=silent)
            if found:
                return T
    if not missing_ok:
        raise PyleLoadFileNotFound(fname, load_path)
    return Nil
        



