# Expression reader in the 2nd ython Lambda Experiment (Pyle2)

import io
import sys
import enum
import numbers
import readline
import jpylib as y

import pglobal
from objects import *
from utils import *


def closingOf(opening):
    """Return the corresponding closing delimiter for the specified opening.

    Some come in complementary pairs, and for the others (e.g. / or |) it the
    same as the opening one.

    """
    matching_bracket = {
        "{": "}",
        "[": "]",
        "(": ")",
        "<": ">",
    }
    return matching_bracket.get(opening) or opening


class Token():

    """A token read from the input stream.

    It may carry a value besides its type, as for symbols, strings, and numbers.

    """
    def __init__(self, reader, value=None):
        self.value = value
        self.location = reader.name
        self.line = reader.line
        self.column = reader.column

    def __str__(self):
        """Return a string representation for a Token.

        These need not be read in as Pyle objects, and should not be confused
        with those. So, different.

        """
        vstring = ""
        if self.value is not None:
            vstring = " " + repr(self.value)
        return "#<{} {}:{}:{}{}>".format(self.__class__.__name__, self.location,
                                         self.line, self.column, vstring)

    def __repr__(self):
        return self.__str__()


class OparenToken(Token): pass
class CparenToken(Token): pass
class PeriodToken(Token): pass
class QuoteToken(Token): pass
class FunctionToken(Token): pass
class UnquoteToken(Token): pass
class QuasiquoteToken(Token): pass
class UnquoteSplicingToken(Token): pass
class SymbolToken(Token): pass
class StringToken(Token): pass
class NumberToken(Token): pass
class TableToken(Token): pass
class VectorToken(Token): pass
class RegexpToken(Token): pass
class EOFToken(Token): pass

EOFSymbol = Symbol("EOF")
commentChar = ";"

class Reader:
    """A reader to read expressions from a stream."""

    def __init__(self, port=None):
        """Initialise a Reader object.

        The input port, if not specified, is pglobal.Stdin.
        """
        if port is None:
            port = pglobal.Stdin
        self.port = port
        # expect a #!interpreter line on file input
        self.may_have_hashbang = not port.is_interactive()
        self.lastread = None
        self.unread_token = None
        self.prompt = None              # set by self.read()
        self.show_prompt = True         # show prompt on next readline


    def name(self):
        return self.port.name()

    def line(self):
        return self.port.line()

    def column(self):
        return self.port.column()


    def getPrompt(self):
        """Callback for the port to determine the line prompt to show."""
        if self.show_prompt and self.prompt:
            return self.prompt
        return ""
        

    def __str__(self):
        """Return some string representation of the Reader."""
        result = ["<"]
        result.append(self.__class__.__name__)
        result.append(": ")
        for k, v in self.__dict__.items():
            result.append(k)
            result.append("=")
            result.append(repr(v))
            result.append(",")
        result.append(">")
        return "".join(result)
            

    def nextChar(self):
        """Return the next character.

        That may be one unread before, or actually a new one from the stream.
        The input port keeps track of column and row.

        """
        return self.port.readchar()


    def unreadChar(self, ch):
        """Unread a char so it is return as the next char by nextChar().

        Only one char may be unread before calling nextChar() again, but that is
        enough.

        """
        self.port.unreadchar(ch)
        

    def nextNonCommentChar(self):
        """Return the next char that is not in a comment.

        Actually, return a space for a comment, so we have a delimiter.
        """
        in_comment = False
        while True:
            ch = self.nextChar()
            if ch and in_comment:
                if ch == "\n":
                    in_comment = False
                    return " "
                continue
            if ch == commentChar:
                in_comment = True
                continue
            return ch

    def nextNonSpaceChar(self):
        """Return the next char that is not whitespace or in a comment."""
        while True:
            ch = self.nextNonCommentChar()
            if ch.isspace():
                continue
            return ch

    def nextToken(self):
        """Deliver the next token from the input stream.

        This may be a token that has been unread before.

        """
        if self.unread_token:
            token = self.unread_token
            self.unread_token = None
            y.trace("nextToken returns unread", token)
            return token
        
        one_char_token = {              # maps to the token *classes*
            "(": OparenToken,
            ")": CparenToken,
            ".": PeriodToken,
            "'": QuoteToken,
            "`": QuasiquoteToken,
            "": EOFToken,
        }
        result = None
        while True:
            ch = self.nextNonSpaceChar()
            token_type = one_char_token.get(ch)
            if token_type:
                # the token_type class is instantiated with the reader to use
                # position info in the reader object
                result = token_type(self)
                break
            if ch == "":
                return EOFToken()
            if ch == ",":
                ch2 = self.nextChar()
                if ch2 == "@":
                    result = UnquoteSplicingToken(self)
                    break
                self.unreadChar(ch2)
                result = UnquoteToken(self)
                break
            if ch == "#":
                result = self.octothorpeMacro()
                if result is None:      # hashbang in the first line
                    continue
                break
            if ch == '"':
                result = self.readStringToken()
                break
            self.unreadChar(ch)
            result = self.readAtomToken() # symbol or number
            break
        y.trace("nextToken returns", result)
        return result

        
    def unreadToken(self, token):
        """Unread a token so it is returned as the next token by nextToken().

        Only one token may be unread before calling nextToken() again, but that
        is enough.

        """
        assert self.unread_token is None, \
            "two unread tokens! old {}, new {}".format(self.unread_token, token)
        self.unread_token = token


    def octothorpeMacro(self):
        """Return an octothorpe token after having alread read the '#'.

        Return None if reading a '#!' in the first line.

        The type of the octothorpe token depends on the next char (*not* the
        next non-space char!). Currently, there is only the ' for a
        FunctionToken. In the future there may be vectors and tables.

        """
        ch = self.nextChar()
        if ch == "'":
            return FunctionToken(self)
        if ch == ":":
            return TableToken(self)
        if ch == "(":
            return VectorToken(self)
        if ch == "/":                   # regexp in #/.../ form
            return RegexpToken(self, self.readRegexp("/"))
        if ch == "r":                   # regexp in #r{...} form
            ch = self.nextChar()
            if ch == "":
                raise PyleReaderEOFError(self, "after '#r'")
            if ch.isspace() or ch == commentChar:
                raise PyleSyntaxError(
                    self,
                    "regexp delimiter may not be whitespace or comment sign")
            return RegexpToken(self, self.readRegexp(closingOf(ch)))
        if ch == "<":
            raise PyleSyntaxError(self, "unreadable #<...> object")
        if ch == "!":
            if self.line() == 1:
                self.port.skip_rest_of_line()
                return None
        if ch == "":
            raise PyleReaderEOFError(self, "after '#'")
        raise PyleSyntaxError(self, "unexpected after '#'", repr(ch))

    
    def readAtomToken(self):
        """Read an atom (symbol or number) and return it as a Token.

        Barred can stop or begin anywhere! Also, backslash escapes. Holy bovine.
        Sounds like it's time for a table-based approach again. Hah! Finally!

        """
        # Maybe pulling the constant setup out to the top level could make this
        # faster. See later if it is worth the price.
        
        class CC(enum.Enum):            # character class
            Bar = 0                     # vertical bar
            Backsl = 1                  # a backslash
            Delim = 2                   # whitespace or some other delimiter
            Member = 3                  # potential member of name or number

        def charclass(c):
            """Return the class of a character."""
            if ch == "|":
                return CC.Bar
            if ch == "\\":
                return CC.Backsl
            if ch == "" or ch.isspace() or ch in pglobal.delimiter_chars:
                return CC.Delim
            return CC.Member

        class St(enum.Enum):            # states
            initial = 0                 # normal, nothing remarkable about this
            normesc = 1                 # escaped from initial state
            barred = 2                  # after a Bar (vs. *in* a bar)
            barresc = 3                 # escaped from barred state
            final = 4                   # done with everything (crash otherwise)

        class Ac(enum.Enum):            # actions
            none = 0                    # do nothing except maybe change state
            collect = 1                 # collect character
            membar = 2                  # remember being barred
            finish = 3                  # unread character and return result

        # 'Tis but a small table setup, luckily.

        # state transition table, by state (vertical) and cclass
        newstate = [
            # Bar         Backsl      Delim       Member
            [ St.barred,  St.normesc, St.final,   St.initial ], # initial
            [ St.initial, St.initial, St.initial, St.initial ], # normesc
            [ St.initial, St.barresc, St.barred,  St.barred  ], # barred
            [ St.barred,  St.barred,  St.barred,  St.barred  ], # barresc
        ]
        
        # action table, by state (vertical) and cclass
        action = [
            # Bar         Backsl      Delim       Member
            [ Ac.membar,  Ac.none,    Ac.finish,  Ac.collect ], # initial
            [ Ac.collect, Ac.collect, Ac.collect, Ac.collect ], # normesc
            [ Ac.none,    Ac.none,    Ac.collect, Ac.collect ], # barred
            [ Ac.collect, Ac.collect, Ac.collect, Ac.collect ], # barresc
        ]
        
        was_barred = False             # was barred at some point => no number
        collected = []
        the_state = St.initial

        while True:
            ch = self.nextChar()
            cclass = charclass(ch)
            act = action[the_state.value][cclass.value]

            y.trace("ch {} cclass {} act {}".format(repr(ch), cclass, act))

            if act == Ac.none:
                pass
            elif act == Ac.collect:
                collected.append(ch)
            elif act == Ac.membar:
                was_barred = True
            elif act == Ac.finish:
                self.unreadChar(ch)
                break
            the_state = newstate[the_state.value][cclass.value]

        result = "".join(collected)
        if was_barred:
            return SymbolToken(self, result)

        num = None
        try:
            val = eval(result)
            if isinstance(val, numbers.Number):
                num = val
        except:
            pass
        if num is None:
            return SymbolToken(self, result)
        return NumberToken(self, num)


    def read_stringlike(self, is_regexp, end_char):
        """Read the contents of a regexp or string.

        whatami is the name of the thing being read, for messages; end_char is
        the char that stops the reading. Return the string contents.

        """
        whatami = ["string", "regexp"][bool(is_regexp)]

        octaldigits = "01234567"

        def parse_octaldigits(first):
            """Read octal digits and return the number value."""
            # Other than with hexdigits, the octal digits string is not fied in
            # length. There is at least one (the first, which we have read
            # already) and at most three, so we may now read up to two more.
            # Any character that is not an octal digit ends the sequence.
            digits = first
            for _ in range(2):
                digit = self.nextChar()
                if digit not in octaldigits:
                    self.unreadChar(digit)
                    break
                digits += digit
            return chr(int(digits, 8))

        def parse_hexdigits(ndigits):
            """Read a number of hex digits and return the number value."""
            digits = ""
            for _ in range(ndigits):
                digit = self.nextChar()
                if digit not in "0123456789abcdefABCDEF":
                    raise PyleSyntaxError(
                        self, "invalid hex digit in {} literal".format(whatami),
                        digit)
                digits += digit
            return chr(int(digits, 16))

        hexdigits = { "x": 2, "u": 4, "U": 8 }
        result = ""
        after_backslash = False
        while True:
            ch = self.nextChar()
            if after_backslash:
                # Now this is a bit tricky, as backslashes are for some part
                # used to escape regpexp specials chars, as * or +, and for
                # another to escape those that are special chars in strings.
                # Holy bovine, she is dumping!
                if ch in pglobal.escape2specialChar:
                    ch = pglobal.escape2specialChar[ch]
                elif ch in hexdigits:
                    ch = parse_hexdigits(hexdigits[ch])
                elif ch in octaldigits:
                    ch = parse_octaldigits(ch)
                elif ch == end_char:
                    pass                # insert this if it is backslash-escaped
                elif is_regexp:         # need the literal backslash after all
                    result += "\\"
                after_backslash = False
            else:
                if ch == "\\":
                    after_backslash = True
                    continue
                if ch == end_char:
                    break
            if ch == "":
                raise PyleReaderEOFError(
                    self, "in {} literal".format(whatami))
            result += ch
        return result
        

    def readRegexp(self, end_char):
        """Read a regexp from the input. '#/' has already been seen."""
        return Regexp(self.read_stringlike("regexp", end_char), self)
        

    def readStringToken(self):
        """Read a string from the input, return a StringToken."""
        return StringToken(self, self.read_stringlike("string", "\""))


    def skip_input_on_error(self):
        # Let's at least empty the current line. There may not be more to do.
        self.port.skip_rest_of_line()


    def read(self, prompt=""):
        """Read an expression from the input and return it."""
        try:
            self.prompt = prompt
            if prompt:
                self.port.setPrompt(self.getPrompt)
            #y.trace(self)
            # token type => function
            readerMacros = {
                QuoteToken: QuoteSymbol,
                FunctionToken: FunctionSymbol,
                UnquoteToken: UnquoteSymbol,
                QuasiquoteToken: QuasiquoteSymbol,
                UnquoteSplicingToken: UnquoteSplicingSymbol,
            }
            try:
                token = self.nextToken()
            except EOFError:
                return None
            self.show_prompt = False
            ttype = type(token)
            if ttype is EOFToken:
                return None
            if ttype is SymbolToken:
                return Symbol(token.value)
            if ttype is NumberToken:
                return Number(token.value)
            if ttype is StringToken:
                return String(token.value)
            if ttype is OparenToken:
                return self.readList()
            if ttype is TableToken:
                return self.readTable()
            if ttype is VectorToken:
                return self.readVector()
            if ttype is RegexpToken:
                return token.value
            if ttype is CparenToken:
                raise PyleSyntaxError(self, "unexpected closing parenthesis")
            if ttype is PeriodToken:
                raise PyleSyntaxError(self, "unexpected dot")
            macroFunction = readerMacros.get(ttype)
            if macroFunction:
                return ListCollector(macroFunction, self.read()).list()
            y.trace("unexpected token:", token)
            raise PyleSyntaxError(self, "unexpected token type", token)
        except Exception as e:
            self.skip_input_on_error()
            raise e
        finally:
            self.show_prompt = True
            

    def readTable(self):
        """Read the body of a table. '#:' has already been read."""
        lc = ListCollector()
        token = self.nextToken()
        if type(token) != OparenToken:
            raise PyleSyntaxError(
                self, "invalid token expecting '(' in a table", token)
        while True:
            token = self.nextToken()
            ttype = type(token)
            if ttype == OparenToken:
                l = self.readList()
                lc.append(l)
            elif ttype == CparenToken:
                break
            else:
                raise PyleSyntaxError(self, "invalid token expecting key/value"
                                      " pair in a table", token)
        return Table(lc.list())


    def readVector(self):
        """Read a vector from the input and return it."""
        lc = ListCollector()
        while True:
            token = self.nextToken()
            ttype = type(token)
            if ttype is PeriodToken:
                raise PyleSyntaxError(self, "unexpected period in vector")
            if ttype is CparenToken:
                return Vector(lc.list())
            if ttype is EOFToken:
                raise PyleReaderEOFError(self, "where ')' expected")
            self.unreadToken(token)
            lc.append(self.read())


    def readList(self):
        """Read a list from the input and return it."""
        lc = ListCollector()
        while True:
            token = self.nextToken()
            ttype = type(token)
            if ttype is PeriodToken:
                if lc.list() is Nil:
                    raise PyleSyntaxError(self, "unexpected dot at "
                                          "beginning of list", token)
                lc.lastcdr(self.read())
                next = self.nextToken()
                if type(next) == CparenToken:
                    return lc.list()
                else:
                    raise PyleSyntaxError(self, "unexpected token where ')' "
                                          "expected", next)
            if ttype is CparenToken:
                return lc.list()
            if ttype is EOFToken:
                raise PyleReaderEOFError(self, "where ')' expected")
            self.unreadToken(token)
            lc.append(self.read())

# EOF
