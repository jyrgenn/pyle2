
import unittest

from objects import *

class SymbolTestcase(unittest.TestCase):

    def test_whatisit(self):
        p0 = Symbol("hohoho")
        self.assertFalse(p0.isPair())
        self.assertTrue(p0.isSymbol())
        self.assertFalse(p0.isString())
        self.assertFalse(p0.isEnvironment())
        self.assertFalse(p0.isNumber())

    def test_str_sym_0(self):
        self.assertEqual(str(Nil), "nil")
        
    def test_str_sym_1(self):
        self.assertEqual(str(T), "t")

    def test_sym_0(self):
        self.assertIs(Symbol("t"), T)

    def test_sym_identity(self):
        name = "lala"
        self.assertIs(Symbol(name), Symbol(name))
        self.assertEqual(str(Symbol(name)), name)

    def test_sym_name(self):
        checks = {
            #:  check   want
            1: ("lala", "lala"),
            2: ("la la", "|la la|"),
            3: ("la\\la", r"|la\\la|"),
            4: ("la|la", r"|la\|la|"),
            5: ("`la\\|la.", r"|`la\\\|la.|"),
            8: ("bu x|\\,y zw", r"|bu x\|\\,y zw|"),
            9: ("bu b|u bu", "|bu b\|u bu|"),
            10: ("bu bu bu", "|bu bu bu|"),
            11: ("bu b|\\,u", r"|bu b\|\\,u|"),
            12: ("bu b\\,u", r"|bu b\\,u|"),
            13: ("|bu b|u", "|\\|bu b\\|u|"),
        }
        # print("Symbol names:")
        for k, v in checks.items():
            with self.subTest("{}: {}".format(k, v[0])):
                sym_str = repr(Symbol(v[0]))
                # print("{:3}:   in »{}«\n     want {}\n     have {}".format(
                #     k, v[0], v[1], sym_str))
                self.assertEqual(sym_str, v[1])
                self.assertEqual(Symbol(v[0]).name(), v[0])

