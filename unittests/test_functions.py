#!/usr/bin/env python3

import unittest
import jpylib as y

import pio
import functions
import pglobal
from helpers import *


class FunctionTestcase(unittest.TestCase):

    def test_func1(self):
        fundef = "(lambda (n) (* 2 n))"
        self.assertEqual(read_from("lambda"),
                         pglobal.Eval(read_from(fundef),
                                      expandMacros=True).type())

    def test_call(self):
        # y.alert_level(y.L_TRACE)
        calldef = "((lambda (n) (* 2 n)) 3)"
        result = pglobal.Eval(read_from(calldef), expandMacros=True)
        self.assertEqual(result.value(), 6)

    def test_func_rest(self):
        fun = "(lambda (head . tail) (cons head tail))"
        self.assertEqual(read_from("lambda"),
                         pglobal.Eval(read_from(fun), expandMacros=True).type())

    def test_true(self):
        self.assertTrue(False is False)

    def test_raises(self):
        with self.assertRaises(NameError):
            self.dodo = gipsnich

    def test_zero_parameters(self):
        expr = read_from("""(progn (defun foo () "foo!") (foo))""")
        value = pglobal.Eval(expr, expandMacros=True)
        self.assertEqual(value.value(), "foo!")
        
