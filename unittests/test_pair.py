# tests for Pair class

import unittest

from objects import *
from pio import Reader
from helpers import *


class PairTestcase(unittest.TestCase):

    def test_str_0(self):
        p0 = Pair(Nil, Nil)
        self.assertEqual(str(p0), "(nil)")

    def test_pair_1(self):
        syma = Symbol("sym a")
        p0 = Pair(syma, Nil)
        self.assertEqual(p0.car(), syma)
        self.assertEqual(p0.cdr(), Nil)
        
    def test_pair_2(self):
        syma = Symbol("sym a")
        symb = Symbol("sym b")
        p0 = Pair(syma, symb)
        self.assertEqual(p0.car(), syma)
        self.assertEqual(p0.cdr(), symb)
        
    def test_pair_3(self):
        syma = Symbol("sym a")
        symb = Symbol("sym b")
        p0 = Pair(syma, symb)
        p1 = Pair(T, p0)
        self.assertEqual(p1.car(), T)
        self.assertEqual(p1.cdr(), p0)
        self.assertEqual(p1.cdr().car(), syma)
        
    def test_list_1(self):
        p0 = Pair(Symbol("hohoho"), Nil)
        self.assertEqual(str(p0), "(hohoho)")
        
    def test_list_1a(self):
        p0 = Pair(Symbol("hohoho"), Nil)
        p0.rplaca(Symbol("hihihi"))
        self.assertEqual(str(p0), "(hihihi)")
        
    def test_list_2(self):
        p0 = Pair(Symbol("hohoho"), Nil)
        p0.rplaca(Symbol("hihihi"))
        p0.rplacd(Pair(Symbol("hahaha"), Nil))
        self.assertEqual(str(p0), "(hihihi hahaha)")

    def test_list_3(self):
        p0 = Pair(Symbol("hohoho"), Nil)
        p0.rplaca(Symbol("hihihi"))
        p0.rplacd(Pair(Symbol("hahaha"), Nil))
        p1 = Pair(Symbol("hohoho"), p0)
        self.assertEqual(str(p1), "(hohoho hihihi hahaha)")

    def test_improper_list(self):
        p0 = Pair(Symbol("hohoho"), Nil)
        p0.rplaca(Symbol("hihihi"))
        p0.rplacd(Pair(Symbol("hahaha"), Nil))
        p1 = Pair(Symbol("hohoho"), p0)
        p1.cdr().cdr().rplacd(Symbol("im! proper!"))
        p1.rplaca(p0)
        self.assertEqual(str(p1),
                         "((hihihi hahaha . |im! proper!|)"
                         " hihihi hahaha . |im! proper!|)")
        
    def test_whatisit(self):
        p0 = Pair(Symbol("hohoho"), Nil)
        self.assertTrue(p0.isPair())
        self.assertFalse(p0.isSymbol())
        self.assertFalse(p0.isString())
        self.assertFalse(p0.isEnvironment())
        self.assertFalse(p0.isNumber())

    def test_length(self):
        cases = {
            "nil": 0,
            "(a)": 1,
            "(1 2 3 4 5)": 5,
        }
        for k, v in cases.items():
            with self.subTest(k):
                form = read_from(k)
                self.assertEqual(form.length(), v)
                
    def test_length_improper(self):
        cases = {
            "(1 2 3 4 . 5)": 4,
        }
        for k, v in cases.items():
            with self.subTest(k):
                form = read_from(k)
                self.assertEqual(form.length(), v)
                
