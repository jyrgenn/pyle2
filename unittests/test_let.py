# tests

import unittest
import jpylib as y

import pglobal
from objects import *
import builtin
import pio
import functions
import lcore
from helpers import *


class LetTestcase(unittest.TestCase):

    def setUp(self):
        pass

    def test_lexical(self):
        expr = """\
(let ((a 'lexical))
  (let ((f (lambda () a)))
    (let ((a 'dynamic))
      (funcall f))))
        """
        value = eval_from(expr)
        self.assertEqual(str(value), "lexical")

    def test_deconst_1(self):
        expr = "(let (((a b c) '(1 2 3 4))) (list c b a))"
        self.assertEqual(repr(eval_from(expr)), "(3 2 1)")

    def test_deconst_2(self):
        expr = "(let (((a b . c) '(1 2 3 4))) (list c b a))"
        self.assertEqual(repr(eval_from(expr)), "((3 4) 2 1)")

    def test_deconst_3(self):
        expr = "(let (((a . b) '(1 2 3 4))) (list b a))"
        self.assertEqual(repr(eval_from(expr)), "((2 3 4) 1)")

    def test_letrec_0(self):
        expr = "(let* ((a 1234)) a)"
        self.assertEqual(repr(eval_from(expr)), "1234")
        
    def test_letrec_1(self):
        expr = "(let* ((a '(1 2 3 4))) a)"
        self.assertEqual(repr(eval_from(expr)), "(1 2 3 4)")
        
    def test_letrec_2(self):
        expr = """(let* ((a '(1 2 3 4))
                         (b (cons a a)))
                  b)"""
        self.assertEqual(repr(eval_from(expr)), "((1 2 3 4) 1 2 3 4)")
        
    def test_letrec_3(self):
        expr = """(let* ((a (* 3 4))
                         (b (cons a a)))
                  b)"""
        self.assertEqual(repr(eval_from(expr)), "(12 . 12)")
        
    def test_letrec_deconst_1(self):
        expr = "(let* (((a b c) '(1 2 3 4))) (list c b a))"
        self.assertEqual(repr(eval_from(expr)), "(3 2 1)")

    def test_letrec_deconst_2(self):
        expr = "(let* (((a b . c) '(1 2 3 4))) (list c b a))"
        self.assertEqual(repr(eval_from(expr)), "((3 4) 2 1)")

    def test_letrec_deconst_3(self):
        expr = "(let* (((a . b) '(1 2 3 4))) (list b a))"
        self.assertEqual(repr(eval_from(expr)), "((2 3 4) 1)")

