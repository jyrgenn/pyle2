#!/usr/bin/env python3

import unittest
import jpylib as y

import lcore
from objects import *
from pio import Reader, load_string, load_file


class IOTestcase(unittest.TestCase):

    def test_load_string(self):
        # y.alert_level(11)
        s = "(setq a 4) (setq b 5) (+ a b)"
        self.assertEqual(str(load_string(s)), "9")

    def test_load_file_nonexistent(self):
        with self.assertRaises(PyleLoadFileNotFound):
            s = load_file("unittests/assets/nonexistent/file", silent=True)
        s = load_file("assets/nonexistent/file", missing_ok=True)
        self.assertEqual(s, Nil)

    def test_load_file_empty(self):
        s = load_file("/dev/null", silent=True)
        self.assertEqual(s, T)

    def test_load_file_symsym(self):
        s = load_file("unittests/assets/quoted_symsym", silent=True)
        self.assertEqual(s, T)

    def test_load_string2(self):
        with open("unittests/assets/quoted_symsym") as f:
            contents = f.read()
        expr = load_string(contents)
        self.assertEqual(str(expr), "symsym")

