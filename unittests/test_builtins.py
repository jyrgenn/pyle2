#!/usr/bin/env python3

import unittest
import jpylib as y

from objects import *
import pio
import functions
import pglobal
from builtins import *
from helpers import *


class BuiltinTestcase(unittest.TestCase):

    def test_equal(self):
        result = eval_from("""\
(flet ((boo (n) (> n 3)))
  (filter #'boo '(0 1 2 3 4 5 6 7 8)))""")
        self.assertEqual(str(result), "(4 5 6 7 8)")

    def test_let(self):
        # y.alert_level(11)
        value = eval_from("(let ((n 4)) (* n n))")
        self.assertEqual(value.value(), 16)

    def test_and(self):
        cases = {
            "(and (* 3 1) (* 3 2) (* 3 3) (* 3 4))": "12",
            "(and nil (error 1) 3)": "nil",
            "(and 1 2 nil (error 2))": "nil",
            "(and 1 2 3 nil)": "nil",
        }
        for k, v in cases.items():
            with self.subTest(k):
                value = eval_from(k)
                self.assertEqual(str(value), v)

    def test_error(self):
        with self.assertRaises(PyleProgramError) as ctx:
            with y.outputCaptured():
                eval_from('(error "fo {} ba {} da {}" 3 4 5)')
        self.assertEqual(str(ctx.exception.args), "(3, 4, 5)")

    def test_raises(self):
        with self.assertRaises(NameError):
            self.dodo = gipsnich

