import unittest

from objects import *

class StringTestcase(unittest.TestCase):

    def test_whatisit(self):
        p0 = String("hohoho")
        self.assertFalse(p0.isPair())
        self.assertFalse(p0.isSymbol())
        self.assertTrue(p0.isString())
        self.assertFalse(p0.isEnvironment())
        self.assertFalse(p0.isNumber())

    def test_str(self):
        the_string = "hihihi hahaha hohoho"
        s = String(the_string)
        self.assertEqual(str(s), "hihihi hahaha hohoho")
        self.assertEqual(repr(s), "\"hihihi hahaha hohoho\"")

    def test_repr(self):
        cases = {
            "": '""',
            "lalala": '"lalala"',
            "hal\"al": '"hal\\"al"',
            "hal\\al": '"hal\\\\al"',
        }
        for k, v in cases.items():
            with self.subTest(k):
                self.assertEqual(repr(String(k)), v)

    def test_substr_0(self):
        s = String("Jimi Hendrix died 50 years ago today.")
        self.assertEqual(str(s.substr()), str(s))

    def test_substr_1(self):
        s = String("Jimi Hendrix died 50 years ago today.")
        self.assertEqual(str(s.substr(13)),
                         str(String("died 50 years ago today.")))

    def test_substr_2(self):
        s = String("Jimi Hendrix died 50 years ago today.")
        self.assertEqual(str(s.substr(13, 30)),
                         str(String("died 50 years ago")))


        
