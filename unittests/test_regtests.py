
import unittest
import jpylib as y

import lcore
import pio
import pglobal
import pyle2
from objects import *


def eval_from(s):
    return pglobal.Eval(pio.Reader(StringInputPort(s)).read(),
                        expandMacros=True)


class RegTestcase(unittest.TestCase):

    def setUp(self):
        print()
        pyle2.init_all()
        y.alert_level(0)

    def test_runAll(self):
        pio.load_file("l/regtests.l", silent=True)
        result = eval_from("(run-tests)")
        self.assertEqual(result.value(), 0)

