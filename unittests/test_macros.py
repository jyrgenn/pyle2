#!/usr/bin/env python3

import unittest
import jpylib as y

from objects import *
import pio
import functions
import pglobal
import builtin
import lcore
from helpers import *

class MacroTestcase(unittest.TestCase):

    def setUp(self):
        pass

    def test_macro0(self):
        # y.alert_level(y.L_DEBUG)
        macrodef = """\
(defmacro if (condition if-clause . else-clauses)
  `(cond (,condition ,if-clause)
         (t ,@else-clauses)))"""
        macroexp = "(macroexpand '(if a 3 4))"
        pglobal.Eval(read_from(macrodef))
        expanded = pglobal.Eval(read_from(macroexp))
        self.assertEqual(str(expanded), "(cond (a 3) (t 4))")

    def test_macro1(self):
        # we already have the core functions, so defun is there
        pglobal.Eval(read_from(
            "(defun fac (n) (if (eq n 0) 1 (* n (fac (- n 1)))))"),
                     expandMacros=True)
        self.assertEqual(str(pglobal.Eval(read_from("(fac 10)"),
                                          expandMacros=True)),
                         str(read_from("3628800")))

# (let ((n 4)) (print n))
# (macroexpand '(let ((n 4)) (while (> n 0) (print n))))
