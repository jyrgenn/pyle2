# reader test cases

import io
import unittest

import jpylib as y

import lcore
from pio import *
from objects import *
from helpers import *

class ReaderTestCase(unittest.TestCase):

    def setUp(self):
        y.alert_level(y.L_NOTICE)

    def test_sym(self):
        cases = {
            1: ("lala", "lala"),
            2: ("~+-$#./@", "~+-$#./@"),
            3: ("abcdefghijklmnopqrstuvxyz0123456789!#$%&*+.-/:<=>?@^_{}~",
                "abcdefghijklmnopqrstuvxyz0123456789!#$%&*+.-/:<=>?@^_{}~"),
            8: ("|bu x\\|\\\\,y zw|", "|bu x\\|\\\\,y zw|"),
            9: ("|bu b\\|u bu|", "|bu b\|u bu|"),
            10: ("|bu bu bu|", "|bu bu bu|"),
            11: ("|bu b|\\,u bu|", "|bu b,u|"),
            12: ("|bu b|\\,u bu|", "|bu b,u|"),
            13: ("|bu b|u bu|", "|bu bu|"),
        }
        # print("Reader symbol names:")
        for k, v in cases.items():
            with self.subTest("{}: {}".format(k, v[0])):
                result = read_to_string(v[0])
                # print("{:3}: want {}\n     have {}".format(k, v[1], result))
                self.assertEqual(result, v[1])

    def test_exprs(self):
        cases = {
            1: ("#'car", "(function car)"),
            2: ("(a ,@b \"hu la la\" . ())", "(a (unquote-splicing b) \"hu la la\")"),
            3: ("(a ,@b \"hu la la\" . boy!)", "(a (unquote-splicing b) \"hu la la\" . boy!)"),
            4: ("(a b \"hu la la\" . nil)", "(a b \"hu la la\")"),
            5: ("(a b \"hu la la\")", "(a b \"hu la la\")"),
            6: (",@bla", "(unquote-splicing bla)"),
            7: ("`bla", "(quasiquote bla)"),
            8: ("'bla", "(quote bla)"),
            9: ("123", "123"),
            10: ("'123", "(quote 123)"),
            11: ("(+ 123e4 12)", "(+ 1230000 12)"),
        }
        for k, v in cases.items():
            with self.subTest("{}: {}".format(k, v[0])):
                result = read_to_string(v[0])
                #print("{:3}: want {}\n     have {}".format(k, v[1], result))
                self.assertEqual(result, v[1])
        
    def test_defmacro(self):
        # y.alert_level(y.L_TRACE)
        expr = """\
(defmacro if (condition if-clause . else-clauses)
  `(cond (,condition ,if-clause)
         (t ,@else-clauses)))"""
        as_read = """\
(defmacro if (condition if-clause . else-clauses) (quasiquote (cond ((unquote condition) (unquote if-clause)) (t (unquote-splicing else-clauses)))))"""
        self.assertEqual(str(read_to_string(expr)), as_read)

    def test_improper_list(self):
        # y.alert_level(y.L_TRACE)
        expr = "(a (1 2 . 3) b c)"
        as_read = "(a (1 2 . 3) b c)"
        self.assertEqual(read_to_string(expr), as_read)

    def test_when_clause(self):
        expr = "if-clause"
        as_read = "if-clause"
        ob = read_from(expr)
        self.assertEqual(str(ob), as_read)
        
    def test_eof(self):
        with self.assertRaises(PyleReaderEOFError):
            #with y.temp_alert_level(11):
            expr = "(and 1 2 nil (error 2)"
            ob = read_from(expr)

    def test_eof_string_atom(self):
        expr = "gaga"
        ob = read_from(expr)
        self.assertEqual(str(expr), str(ob))

    def test_eof_octothorpe(self):
        expr = "#"
        with self.assertRaises(PyleReaderEOFError) as ctx:
            ob = read_from(expr)
        self.assertEqual(str(ctx.exception),
                         "ReaderEOFError: unexpected EOF at *read-test*:1:1 after '#'")


    def test_re_backslash(self):
        expr = r"#/\.py$/"
        ob = read_from(expr)
        self.assertEqual(str(ob), r"#/\.py$/")
