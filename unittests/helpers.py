
from pio import *
from objects import *

def eval_from(s):
    return pglobal.Eval(read_from(s), expandMacros=True)

def read_from(s):
    """Read on object from the string, return the object."""
    return Reader(StringInputPort(s, "*read-test*")).read()

def read_to_string(s):
    """Read on object from the string, return the object as string."""
    return repr(read_from(s))

