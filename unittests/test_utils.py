
import unittest
import jpylib as y

import pio
from utils import *
from objects import *
from helpers import *


class UtilsTestcase(unittest.TestCase):

    def setUp(self):
        pass

    def test_lc_0(self):
        lc = ListCollector()
        self.assertEqual(lc.list(), read_from("()"))

    def test_lc_1(self):
        lc = ListCollector()
        lc.append(read_from("a"))
        self.assertEqual(str(lc.list()), str(read_from("(a)")))

    def test_lc_n(self):
        lc = ListCollector()
        lc.append(read_from("a"))
        lc.append(read_from("b"))
        lc.append(read_from("((113 4) bla)"))
        lc.append(read_from("12"))
        lc.append(read_from("boohoo"))
        self.assertEqual(str(lc.list()),
                         str(read_from("(a b ((113 4) bla) 12 boohoo)")))

    def test_lc_improper(self):
        lc = ListCollector()
        lc.append(read_from("a"))
        lc.append(read_from("b"))
        lc.append(read_from("((113 4) bla)"))
        lc.append(read_from("12"))
        lc.lastcdr(read_from("boohoo"))
        self.assertEqual(str(lc.list()),
                         str(read_from("(a b ((113 4) bla) 12 . boohoo)")))


