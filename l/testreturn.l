;;; test the 'return' builtin function

(defun fac-up (n)
  (let ((i 1)
        (result 1))
    (loop
      (incf i)
      (when (> i n)
        (return result))
      (setf result (* i result)))))

        
