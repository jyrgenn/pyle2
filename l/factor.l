
(defvar the-primes '(2 3))
(defvar last-pair (lastpair the-primes))

(defun append-prime (new-prime)
  (let ((newpair (list new-prime)))
    (rplacd last-pair newpair)
    (setf last-pair newpair))
  new-prime)

(defun next-prime ()
  (let ((candidate (+ 2 (car last-pair))))
    (while t
      (catch 'divisible
        (let ((limit (sqrt candidate)))
          (dolist (prime the-primes)
            (cond ((> prime limit)
                   (print "next prime is " candidate)
                   (return (append-prime candidate)))
                  ((zerop (% candidate prime))
                   (throw 'divisible))))))
      (incf candidate 2))))
        

(defun all-primes ()
  (let ((pos the-primes))
    (lambda ()
      (if (null pos)
          (next-prime)
        (pop pos)))))

(defun prime-factors (n)
  (let ((factors (list-collector))
        (get-next (all-primes)))
    (while t
      (let ((p (get-next)))
        (while (and (> n 1)
                    (zerop (% n p)))
          (factors p)
          (setf n (/ n p)))
        (when (eq n 1)
          (break))))
    (when (> n 1)
      (factors n))
    (factors)))
