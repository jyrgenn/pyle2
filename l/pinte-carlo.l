#! pyle2
;;; calculate pi using a Monte Carlo method as suggested by
;;; https://www.heise.de/news/Happy-Pi-Day-Wir-feiern-mit-Kuchen-und-Raspberry-Pi-co-5075899.html
;;;
;;; * locate a random point in the square [0,1),[0,1)
;;; * count total and those inside the unit circle
;;; * calculate the ratio of the counts

;;; Area of the square: 1
;;; Area of first quadrant of the unit circle: pi * r^2/4 = pi / 4
;;; the ratio of those inside is pi / 4, so ratio * 4 = pi


(defun inside (x y)
  (if (< (+ (* x x) (* y y)) 1)
      1.0
    0.0))
  

(let ((printerval 10000)
      (overall 0)
      (count-inside 0)
      (end 1000000))
  (while (< overall end)
    (let ((is-in (inside (random) (random))))
      (incf count-inside is-in)
      (incf overall)
      (if (zerop (% overall printerval))
          (format t "{}: pi = {}\n"
                  overall
                  (* 4 (/ count-inside overall)))))))
