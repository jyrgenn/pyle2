#!./pyle2.py

(provide 'regtests)

(setf *print-stacktraces* nil)
(setf *print-load-notice* nil)
(defvar testdir "regtests" "directory containing the test cases")
(defvar protocol-file-name "testrun.log")
(defvar out (open protocol-file-name :output) "test log file")
(defvar fails nil "list of failed test cases")
(defvar warnings 0 "number of warnings issued")
(defvar ntests 0 "number of tests done")
(defvar verbose nil)

(fset 'string-concat #'string)

(defvar test-name-table #:()
  "Ensure that all tests have a unique name")

(defun presult args
  (when verbose
    (apply #'format t args))
  (apply #'format out args))

(defmacro test (name expr)
  "Run test NAME; success if EXPR evaluates to a true value"
  `(test-internal ,name ',expr t 'true))

(defmacro test-is (name expr expect)
  "Run test NAME; success if EXPR evaluates to EXPECT"
  `(test-internal ,name ',expr ,expect 'cmp))

 (defmacro test-match (name expr expect-re)
   "Run test NAME; success if EXPR evaluates to something matching EXPECT-RE"
   `(test-internal ,name ',expr ,expect-re 'match))

(defmacro test-not (name expr)
  "Run test NAME; success if EXPR evaluates to false"
  `(test-internal ,name ',expr nil 'false))

(defmacro test-num (name expr expect)
  "Run test NAME; success if EXPR evaluates to a number close to EXPECT"
  `(test-internal ,name ',expr ,expect 'num))

(defmacro test-form (name form expect)
  "Run test NAME; success if calling FORM results in EXPECT"
  `(test-internal ,name ,form ,expect 'cmp))

(defmacro test-err (name expr expect-re)
  "Run test NAME; success if evaluating EXPR raises an error matching EXPECT-RE"
  `(test-internal ,name ',expr ,expect-re 'error))

(defun pad (item len)
  (let* ((item-s (string item))
         (ilen (length item-s)))
    (string item (make-string (- len ilen) " "))))


(defun test-internal (name expr expect type)
  "Run the test called NAME and print the result.
The test is considered successful if the printed representations of
the evaluation of EXPR and EXPECT are equal.
TYPE can be 'true, 'cmp, 'false, 'match, 'num, or 'error."
  ;; (format t "test '{}' expr '{}' expect '{}' type {}\n"
  ;;         name expr expect type)
  (trace-if 'trace-test "{}" (list name expr expect type))
  (let ((already-have-name? (test-name-table name)))
    (if already-have-name?
        (let ((where-before already-have-name?))
          (when (eq where-before *current-load-file*)
            (setf where-before "the same file"))
          (format t "WARN {}: \"{}\" already seen in {}\n"
                  *current-load-file* name where-before)
          (incf warnings))
      (test-name-table name *current-load-file*)))
  (let* ((result (errset (if (functionp expr)
                             (expr)     ;allow closures
                           (eval expr))
                         nil))
         (pass (lambda ()
                 (presult "pass: {} »{}«\n" (pad name 23) result)))
         (fail (lambda args
                 (unless args
                   (setf args
                         (list
                          "FAIL: {}\n calculated: »{}«\n   expected: »{}«\n"
                          name (princs result) (princs expect))))
                 (apply #'presult args)
                 (push (cons *current-load-file* name) fails))))
    ;; (format t "test '{}' result is: {}\n" name result)
    (incf ntests)
    (if (atom result)
        (progn (setf result *last-error*)
               (if (eq type 'error)
                   (if (regexpp expect)
                       (if (expect result)
                           (pass)
                         (fail))
                     (fail "FAIL: {} wrong, not a regexp: {}\n"
                           name expect))
                 (fail "FAIL: {} RAISED ERROR: {}\n"
                       (pad name 23) *last-error*)))
      ;; no error
      (setf result (car result))
      (cond ((eq type 'true) (if result (pass) (fail)))
            ((eq type 'false) (if result (fail) (pass)))
            ((eq type 'cmp)
             (setf result (princs result))
             (setf expect (princs expect))
             (if (eq result expect) (pass) (fail)))
            ((eq type 'match)
             (setf result (princs result))
             (if (expect result) (pass) (fail)))
            ((eq type 'num)
             (setf result (princs (round-deep result)))
             (setf expect (princs (round-deep expect)))
             (if (= result expect) (pass) (fail)))
            ((eq type 'error) (fail "FAIL: {} NO ERROR\n" name))
            (t (error "invalid test type {}" type))))))

(test-internal 'test-is ''lala "lala" 'cmp) ;check if the *testing* works
(decf ntests)                               ;but don't count this one

(defmacro roundto5 (n)
  "Round to 5 places."
  `(let ((factor 1e5))
     (/ (round (* ,n factor)) factor)))

(defun round-deep (ob)
  "Round all numbers in the object OB for numeric comparison.
Traverse conses and vectors to find numbers."
  (cond ((numberp ob) (roundto5 ob))
        ((consp ob) (cons (round-deep (car ob))
                          (round-deep (cdr ob))))
        ((vectorp ob) (dotimes (i (length ob) ob)
                        (ob i (round-deep (ob i)))))
        (t ob)))

(defvar testing-done nil
  "A test file is not completed until this is true.")

(defun done-testing ()
  "Mark a test file as done."
  (presult "testing done in {}\n" *current-load-file*)
  (setf testing-done t))

;;;;;;;;;;;;;;;;;;; now let the games begin

(defun run-tests ()
  (setf fails nil)
  (setf warnings 0)
  (when (eq (car *args*) "-v")
    (pop *args*)
    (setf verbose t))

  (let ((files (or *args*
                   (directory (string-concat testdir "/" "[0-9]*.l")))))
    (when verbose
      (format t "load files: {}\n" files))
    (dolist (f files)
      (let ((number (car (#/^[0-9][0-9][0-9]$/ f))))
        ;; (format t "number: {}\n" number)
        (when number
          (let ((fname (car (directory (format nil "{}/{}*.l"
                                                    testdir number)))))
            (when fname 
              (setf f fname)))))
      (presult "\nloading {}\n" f)
      (setf testing-done nil)
      (unless verbose
        (let ((number (cadr (#r{/([0-9]+)} f))))
          (princ (string " " number ""))))
      (let ((result (errset (load f))))
        (if (atom result)
            (progn (presult "load FAIL: {} {}\n" f *last-error*)
                   (push (cons f "load") fails))
          (unless testing-done
            (presult "file FAIL: {} not completed\n" f)
            (push (cons f " not completed, (done-testing) not called")
                  fails))))))

  (let ((nfails (length fails)))
    (format t "\n{} tests, {} FAILS, {} warning{}\n"
            ntests nfails warnings (plural-s warnings))
    (if (zerop nfails)
        (progn (princ "\ntests ALL PASSED")
               (when (not (zerop warnings))
                 (princ "\n(but non-zero warnings)"))
               (terpri))
      (dolist (err (reverse fails))
        (let ((file (car err))
              (name (cdr err)))
          (format t "   {}:{}\n" file name)))))
  (terpri)
  (format t "test output written to {}\n" protocol-file-name)
  
  ;; return non-zero if there were test fails or warnings
  (+ (length fails) warnings))

;; EOF
