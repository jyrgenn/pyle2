
(let ((zeroes 0))
  (while t
    (let* ((num-s (string "1" (make-string zeroes "0") "1"))
           (number (read num-s)))
      (format t "{:3d}: {}\n" zeroes (factor number))
      (incf zeroes))))
