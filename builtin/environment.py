# functions dealing with environments

import pglobal
from objects import *
from utils import *
from . import *

def envArg(arg, name=""):
    """Trigger an error if arg is not an environment.

    Return the argument, otherwise.
    """
    if arg.isEnvironment():
        return arg
    argError(arg, name, "environment")


@builtin()
def the_environment():
    """Return the current environment."""
    return pglobal.theEnvironment


@builtin()
def env_table(env=None, noparents=False):
    """Return the key/value entries of ENV (or the current env) as a table."""
    env = env or pglobal.theEnvironment
    the_dict = envArg(env).dict(noparents)
    return Table([]).update(the_dict)


@builtin()
def env_vars(env=None, noparents=False):
    """Return the variable defined in ENV (or the current env) as a list."""
    env = env or pglobal.theEnvironment
    return ListCollector(*envArg(env).dict(noparents).keys()).list()


@builtin()
def environmentp(ob):
    """Return true iff OB is an environment."""
    return boolOb(ob.isEnvironment())


@builtin()
def new_environment(parent_env=None, value_table=None):
    """Return a new environment. Optional PARENT-ENVIRONMENT is the parent,
otherwise the current environment. If PARENT-ENVIRONMENT is nil, there
is no parent environment, i.e. a top-level environment is created; if
PARENT-ENVIRONMENT is t (the default), the parent is the current
environment. If VALUE-TABLE is non-nil, it is a table with symbol/value
pairs to populate the new environment."""
    if parent_env in (T, None):
        parent_env = pglobal.theEnvironment
    elif parent_env is Nil:
        parent_env = None
    the_env = Environment(parent_env)
    if value_table is not None:
        the_env.update(tableArg(value_table).data)
    return the_env


@special()
def with_environment(env, *bodyforms):
    """Evaluate BODYFORMS in environment ENV and return the last value."""
    savedEnvironment = pglobal.theEnvironment
    try:
        pglobal.theEnvironment = envArg(pglobal.Eval(env))
        return pglobal.EvalProgn(bodyforms)
    finally:
        pglobal.theEnvironment = savedEnvironment
