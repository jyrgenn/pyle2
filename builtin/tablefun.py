# table builtin functions

from objects import *
from . import *

def tableArg(arg, name=""):
    """Trigger an error if arg is not a table.

    Return the argument.
    """
    if arg.isTable():
        return arg
    argError(arg, name, "table")


@builtin()
def table(*args):
    """Return a new table, optionally filled with pairs.
    Every argument that is not a pair will be used as (arg . nil).
    """
    return Table(args)


@builtin()
def table_count(table):
    return Number(tableArg(table).length())


@builtin()
def table_exists(table, key):
    """Return t if KEY exists in TABLE, nil else."""
    return boolOb(tableArg(table).exists(key))


@builtin()
def table_get(table, key, default=Nil):
    """Get the value for KEY from TABLE.
    If KEY is not present, return DEFAULT (which defaults to nil).
    TODO really, never an error?
    """
    try:
        return tableArg(table).get(key, default)
    except:
        raise PyleKeyError(table, key)


@builtin()
def table_inc(table, key, delta=None, start=None):
    """Increment the value of KEY in TABLE with DELTA (default 1).

    Return the new value. If the key already exists, its value must be a number.
    If not, it will be created and initialised with START (default 0) before
    incrementing.

    """
    table = tableArg(table)
    if start is None:
        start = Number(0)
    value = table.get(key, start)
    if not value.isNumber():
        raise PyleDataTypeError("table {} value of key {} is not a number: {}",
                                table, key, value)
    if delta is None:
        delta = Number(1)
    newvalue = Number(value.value() + delta.value())
    table.put(key, newvalue)
    return newvalue


@builtin()
def table_keys(table):
    """Return a list of the keys in TABLE."""
    return tableArg(table).keys()


@builtin()
def table_pairs(table):
    """Return a list of the key/value pairs in TABLE."""
    return tableArg(table).pairs()


@builtin()
def table_put(table, key, value):
    """In TABLE, set the value of KEY to VALUE and return it."""
    return tableArg(table).put(key, value)


@builtin()
def table_remove(table, key):
    """Remove KEY from TABLE and return its value (or nil, if it didn't exist).
    """
    return tableArg(table).delete(key)


@builtin()
def table_values(table):
    """Return a list of the values in TABLE."""
    return tableArg(table).values()

# EOF
