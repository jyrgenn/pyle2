
import math

the_primes = [2, 3]


def nextPrime():
    """Return the next bigger prime and store it."""
    candidate = the_primes[-1] + 2
    while True:
        limit = math.sqrt(candidate)
        for prime in the_primes:
            if prime > limit:
                the_primes.append(candidate)
                yield candidate
                break
            if candidate % prime == 0:
                break
        candidate += 2


def allPrimes():
    """Return a generator to deliver all primes."""
    for p in the_primes:
        yield p
    while True:
        yield from nextPrime()


def isPrime(candidate):
    """Return True iff candidate is a prime number, else False."""
    limit = math.sqrt(candidate)
    for prime in allPrimes():
        if prime > limit:
            return True
        if candidate % prime == 0:
            return False


def prime_factors(n):
    """Return the list of prime factors of n."""
    prime_factors = []

    limit = math.sqrt(n)
    for p in allPrimes():
        while n > 1 and n % p == 0:
            prime_factors.append(p)
            n /= p
        if n == 1:
            break
    if n > 1:
        prime_factors.append(n)
    return prime_factors
        
