# assorted builtin functions that don't really have a place elsewhere

from utils import *
from . import *


@builtin()
def random_():
    """Return a random float number n with 0 <= n < 1."""
    return Number(random.random())


@builtin()
def random_int(stop):
    """Return a random integer n with 0 <= n < STOP."""
    return Number(int(random.uniform(0, numberArg(stop))))


@builtin()
def type_of(object):
    """Return the type of OBJECT as a symbol."""
    return object.type()


@builtin()
def plist(symbol):
    """Return the property list of SYMBOL."""
    lc = ListCollector()
    for k, v in symbolArg(symbol).props():
        lc.append(Pair(k, v))
    return lc.list()


@builtin()
def length(seq):
    """Return the length of SEQ, which may be a string or a list."""
    return Number(seq.length())


@builtin()
def symbol_function(symbol):
    func = symbolArg(symbol).function()
    if func:
        return func
    raise PyleUndefFunctionError(symbol)


@builtin()
def id_(ob):
    """Return a unique id for the argument OB."""
    return Number(id(ob))

# EOF
