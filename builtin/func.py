# builtin functions dealing with functions and macros

import pglobal
from objects import *
import functions
import utils
from . import *

@builtin()
def docstring(item):
    """Return the docstring of FUNCTION (a function symbol or object)."""
    function = None
    if item.isCallable():
        return String(item.docstring())
    elif item.isSymbol():
        function = item.function()
        if not function:
            return Nil
        if function.isFunction():
            return String(function.docstring())
        raise PyleDataTypeError("non-function in symbol {}'s function cell: {}",
                                function)
    else:
        raise PyleDataTypeError("argument not a function or a symbol: {}", item)


def _get_docstring(bodyforms):
    """Return the docstring of given function body, and the rest of the body.

    If the docstring is the only part of the body, return the docstring and the
    body. In this case, the docstring is kinda the function's constant return
    value. That may or may not be what its author intended.

    """
    if bodyforms:
        maybedoc, rest = bodyforms.cxr()
        if maybedoc.isString():
            return maybedoc, rest or bodyforms
    return "", bodyforms


@special()
def defmacro(name, params, *bodyforms):
    """Create macro NAME (a symbol) with PARAMS and BODYFORMS.
    The first of the bodyforms can be a docstring.
    PARAMS may be a proper list, an improper list, or a symbol. In case of a
    proper list, the number of possible arguments is fixed, and each will be
    bound to one of the parameter symbols. In case of an improper list, the
    first arguments will be bound to the normal parameter symbols, and the list
    of remaining arguments to the symbol ending the list. In case of a symbol,
    the list of all arguments will be bound to this parameter symbol.

    """
    utils.trace_if("defs", "defmacro {}", str(name))
    if not (params.isSymbol() or params.isPair()):
        raise PyleDataTypeError("params must be symbol or list: {}", params)
    bodyforms = Seq2List(bodyforms)
    docstring, bodyforms = _get_docstring(bodyforms)
    macro = Macro(name.name(), params, bodyforms, docstring)
    name.setFunction(macro)
    return name

@builtin()
def macroexpand(form):
    """Expand macros in FORM."""
    return functions.macroexpandForm(form)

@builtin()
def macroexpand1(form):
    """Expand one layer of macros in FORM."""
    return expandFormRecurse(form)[0]
    

@special()
def lambda_(params_or_name, *moreargs): # "lambda" is a reserved word
    """Return a function object.
    (lambda ["name"] (params) bodyforms...)
    PARAMS may be a proper list, an improper list, or a symbol. In case of a
    proper list, the number of possible arguments is fixed, and each will be
    bound to one of the parameter symbols. In case of an improper list, the
    first arguments will be bound to the normal parameter symbols, and the list
    of remaining arguments to the symbol ending the list. In case of a symbol,
    the list of all argument will be bound to this parameter symbol.

    """
    name = None
    if params_or_name.isString():
        name = params_or_name
        if not moreargs:
            raise PyleInvalidCallError("lambda needs at least a `params` arg")
        params, *bodyforms = moreargs
    else:
        params = params_or_name
        bodyforms = moreargs

    if not (params.isSymbol() or params.isPair()):
        raise PyleDataTypeError("params must be symbol or list: {}", params)
    bodyforms = Seq2List(bodyforms)
    docstring, bodyforms = _get_docstring(bodyforms)
    return Lambda(params, bodyforms, docstring, name)

@builtin()
def fset(symbol, function, set_name=False):
    """Bind FUNCTION to SYMBOL.
    If SET-NAME is true, also set the name of the function.
    """
    sym = symbolArg(symbol, "symbol")
    if sym.function():
        y.noticef("redefine {} `{}' with new {}", type_of(sym.function()), sym,
                  type_of(function))
    sym.setFunction(functionArg(function, "function"))
    if set_name:
        function.setName(symbol.name())
    return function

@special()
def unquote(form):
    """Mark FORM as not quoted inside a quasiquote form.
    This cannot be meaningfully used outside of a quasiquote.
    """
    raise PyleInvalidCallError(
        "unquote cannot be called outside of a quasiquote")


@special()
def unquote_splicing(form):
    """Unquote FORM a quasiquote form and splice it into the surrounding list.
    This cannot be meaningfully used outside of a quasiquote.
    """
    raise PyleInvalidCallError(
        "unquote-splicing cannot be called outside of a quasiquote")

@y.tracefn
@special()
def quasiquote(form):
    """Quasiquote-expand form.
    The form is considered quoted, except for elements unquoted by the unquote
    or unquote-splicing forms. These will be evaluated and expanded.

    This is mainly meant for implementing macros, but can used anywhere,
    actually.

    """
    if not form.isPair():
        return form
    head, tail = form.cxr()
    if head is UnquoteSymbol:
        taillen = tail.length()
        if taillen == 1:
            return pglobal.Eval(tail.car())
        else:
            raise PyleArgCountError(UnquoteSymbol.function(),
                                    taillen, taillen > 1)
    tailResult = quasiquote(tail)
    if not head.isPair():
        return Pair(head, tailResult)
    headhead = head.car()
    if headhead is UnquoteSplicingSymbol:
        headlen = head.length()
        y.debug("headlen =", headlen, "head is", head)
        if headlen == 2:
            unqSArg = pglobal.Eval(head.cdr().car())
            # append rest of list nconc-style
            if unqSArg is Nil:
                return tailResult
            if not unqSArg.isPair():
                raise PyleDataTypeError("unquote-splice argument must be list, "
                                        "but is `{}'", unqSArg)
            
            newlist, lastpair = utils.copyListLastpair(unqSArg)
            if lastpair is Nil:
                raise PyleArgTypeError(UnquoteSplicingSymbol.function(),
                                       "form", unqSArg, "proper list")
            lastpair.rplacd(tailResult)
            return newlist
        else:
            raise PyleArgCountError(UnquoteSplicingSymbol.function(),
                                    headlen - 1, headlen > 2)
    else:
        return Pair(quasiquote(head), tailResult)
    

@builtin()
def fboundp(symbol):
    """Return t if SYMBOL is bound to a function, nil otherwise."""
    return boolOb(symbolArg(symbol).function() is not None)


@special()
def flet(bindings, *bodyforms):
    """Bind one or more functions to symbols and evaluate BODY.
    The BINDINGS are (symbol params bodyforms) lists.
    """
    restore_bindings = []               # tuples (sym, saved_func)
    for binding in bindings:
        if len(binding) < 2:
            PyleDataTypeError("flet: malformed binding form `{}'", binding)
            
        symbol = binding.car()
        params = binding.cdr().car()
        f_body = binding.cdr().cdr()
        docstring = ""
        if len(f_body) > 1 and f_body.car().isString():
            doc, f_body = f_body.cxr()
            docstring = str(doc)

        if not symbol.isSymbol():
            PyleDataTypeError("flet: function binding needs symbol, not `{}'",
                              sym)
        func = Lambda(params, f_body, docstring, name=symbol.name())
        restore_bindings.append((symbol, symbol.function()))
        symbol.setFunction(func)
        func.setName(symbol.name())
    try:
        return pglobal.EvalProgn(bodyforms)
    finally:
        for symbol, saved_func in restore_bindings:
            symbol.setFunction(saved_func)


@builtin()
def function_name(function):
    """Return the name under which FUNCTION has been defined as a string.

    This works for functions defined in flet. The name may be `*anon*' for
    anonymous lambdas, if they haven't been defined without a name. (If they
    have been defined with a name, they are still anonymous as the name is for
    purely decorative purposes only.)

    """
    return String(functionArg(function).name())


@builtin()
def function_body(function):
    """Return the body forms of FUNCTION, which must be a lambda or a macro.
    """
    return functionArg(function).body()


@builtin()
def function_params(function):
    """Return the parameters of FUNCTION.
    """
    return functionArg(function).params()

