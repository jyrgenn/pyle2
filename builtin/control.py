# control flow builtin functions

import pglobal
from objects import *
from . import *


@special()
def cond(*clauses):
    """For the first of the CLAUSES, which are (test-form value-forms...),
    where test-form evaluates to non-nil, evaluate the value-forms and
    return the last of their values.
    """
    for clause in clauses:
        carval = pglobal.Eval(listArg(clause, "clause").car())
        if carval:
            resultclauses = clause.cdr()
            if resultclauses:
                return pglobal.EvalProgn(resultclauses)
            return carval
    return Nil


@special()
def catch(tag, *bodyforms):
    """Eval BODYFORMS as implicit progn. If TAG is thrown, return its value."""
    try:
        return pglobal.EvalProgn(bodyforms)
    except PyleThrow as throw:
        if throw.tag is pglobal.Eval(tag):
            return throw.value
        else:
            raise throw


@builtin()
def throw(tag, value=Nil):
    """Throw TAG with VALUE into the innermost catch for TAG."""
    raise PyleThrow(tag, value)


@special()
def unwind_protect(bodyform, *cleanupforms):
    """Eval BODYFORM, and even in case of an error or throw, eval CLEANUPFORMS.
    If BODYFORM completes normally, return its value after executing the
    CLEANUPFORMS.

    """
    try:
        return pglobal.Eval(bodyform)
    finally:
        pglobal.EvalProgn(cleanupforms)


@builtin()
def return_(value=Nil):
    """Return VALUE from the function in which this is called."""
    raise PyleReturn(value)


@special()
def _while(condition, *bodyforms):
    """Execute BODYFORMS while CONDITION is true.
    There is an implicit catch around the loop that catches the symbol
    `*break-while*'.

    """
    try:
        while pglobal.Eval(condition):
            pglobal.EvalProgn(bodyforms)
    except PyleThrow as throw:
        if throw.tag is Symbol("*break-while*"):
            return throw.value
        else:
            raise throw
    return Nil


@special()
def and_(*objects):
    """Return the last of OBJECTS if all evaluate to non-nil.
    Otherwise, return the first nil value and don't evaluate the rest.
    """
    value = Nil
    for ob in objects:
        value = pglobal.Eval(ob)
        if value is Nil:
            return Nil
    return value

@special()
def or_(*objects):
    """Return the first of OBJECTS that evaluates to non-nil.
    Do not evaluate the following objects.
    """
    for ob in objects:
        value = pglobal.Eval(ob)
        if value:
            return value
    return Nil


# EOF
