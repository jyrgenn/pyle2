# load Pyle2's builtin functions, and some helpers

import inspect
import jpylib as y

y.debug("loading", __name__)

from utils import *
from objects import *
import functions

# some helper functions to check/extract arguments

def argError(arg, name, typ):
    function = inspect.stack()[2].function
    if name:
        name = function + " " + name
    else:
        name = function
    raise PyleDataTypeError("{} argument is not {}: {}", name, typ, repr(arg))
    

def numberArg(arg, name=""):
    """Trigger an error if arg is not a number.

    Return the value as a python number.
    """
    if arg.isNumber():
        return arg.value()
    argError(arg, name, "number")

    
def pairArg(arg, name=""):
    """Trigger an error if arg is not a pair.

    Return the argument.
    """
    if arg.isPair():
        return arg
    argError(arg, name, "pair")

    
def listArg(arg, name=""):
    """Trigger an error if arg is not a list.

    Return the argument.
    """
    if arg.isList():
        return arg
    argError(arg, name, "list")


def symbolArg(arg, name=""):
    """Trigger an error if arg is not a symbol.

    Return the argument.
    """
    if arg.isSymbol():
        return arg
    argError(arg, name, "symbol")


def functionArg(arg, name=""):
    """Trigger an error if arg is not a function.

    Return the argument.
    """
    if arg.isSymbol():
        func = arg.function()
        if not func:
            func = arg.getValue()
        arg = func
    if arg.isCallable():
        return arg
    argError(arg, name, "callable")


# the function decorators for the builtin functions (and special forms, mind)

def special(fname=None):
    """Decorator for special function builtins, for convenience."""
    return builtin(fname, special=True)


def builtin(func_name=None, special=False):
    """Decorator for builtin functions.

    Takes or determines the function's name, creates a Builtin function object
    with a docstring and min/max args values, and binds it to its function
    symbol.

    """

    def the_decorator(func):
        """This is the actual function returned.

        It will never be called except once to do the registration.

        """
        min_args = 0                        # minimum number of arguments
        max_args = 0                        # maximum number of arguments
        std_params = []
        key_params = []
        rest_params = None
        nonlocal func_name

        if func_name is None:
            # Function names not only get the underscores replaced by dashes,
            # but also underscores stripped from the beginning and the end, so I
            # can avoid any name conflicts with words reserved in Python.
            func_name = dashify(func.__name__.strip("_"))

        for name, param in inspect.signature(func).parameters.items():
            name = dashify(name)
            if param.default != inspect.Parameter.empty:
                # parameter with a default -> optional
                max_args += 1
                key_params.append(name)
            elif param.kind == inspect.Parameter.VAR_POSITIONAL:
                # variable number of optional parameters
                max_args = None                # must be last arg to not go boom
                rest_params = name
            else:
                # no default and not VAR_POSITIONAL => standard mandatory
                # parameter
                min_args += 1
                max_args += 1
                std_params.append(name)

        docstring = "("
        docstring += " ".join([func_name, *std_params])
        if key_params:
            docstring += " &optional "
            docstring += " ".join(key_params)
        if rest_params:
            docstring += " &rest " + rest_params
        docstring += ")"
        if func.__doc__:
            docstring += "\n\n"
            docstring += func.__doc__

        trace_if("builtin", "define Builtin({}, {}, {}, special={})",
                 func_name, str(func), docstring.split("\n")[0], special)

        def theCaller(*args, **kwargs):
            nonlocal func, func_name
            trace_if("builtin", "calling {}({}, {})", func_name,
                     lambda: ", ".join(map(repr, args)),
                     lambda: ", ".join(["{}={}".format(k, repr(v))
                                        for k, v in kwargs]))
            try:
                result = func(*args, **kwargs)
            except PyleException as pe:
                raise pe
            except Exception as e:
                raise PyleBuiltinError(e, func_name, args, kwargs)
            assert isinstance(result, Object) ,\
                "return value of {} not an Object: {}".format(func_name,
                                                              repr(result))
            return result
        
        builtin_function = Builtin(func_name, theCaller, min_args, max_args,
                                   docstring, special)
        Symbol(func_name).setFunction(builtin_function)
        return func
    return the_decorator


def spread_arglist(which, args):
    """Collect a spreadable args list for function WHICH.

    The last argument will be the cdr of the last cons. The result is
    (arg1 arg2 [...] . argn)
    """
    lc = ListCollector()
    lastarg_index = len(args) - 1
    for index, elem in enumerate(args):
        if index == lastarg_index:
            lc.lastcdr(elem)
        else:
            lc.append(elem)
    return lc.list()    


from .arithmetic import *
from .assorted import *
from .basic import *
from .control import *
from .func import *
from .iofunc import *
from .string import *
from .system import *
from .vars import *
from .tablefun import *
from .vectorfun import *
from .alerts import *
from .types import *
from .compare import *
from .environment import *
from .process import *

# EOF
