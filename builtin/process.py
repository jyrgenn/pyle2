# functions dealing with processes

import os
import subprocess

import pglobal
from objects import *
from utils import *
from . import *

shellmeta = "\"'`|&;[(<>)]*?$:"
fullSymbol = Symbol("full")


@builtin()
def getenv(var=None):
    """Get the process environment as a table, or a particular variable,
    if specified.
    """
    if var is None:
        table = Table()
        for key, value in os.environ:
            table.put(String(key), String(value))
        return table
    value = os.environ.get(str(var))
    if value is None:
        return Nil
    return String(value)


@builtin()
def putenv(var, value):
    """Put VAR with VALUE into the environment and return VALUE.
    If VALUE is nil, delete VAR from the environment and return VAR's old value.
    """
    var = str(var)
    if value is Nil:
        oldvalue = os.environ.pop(var, None)
        if oldvalue is None:
            return Nil
        return String(oldvalue)
    os.environ[var] = str(value)
    return value


@builtin()
def shell_command(command, capture=None, silent=None, shell=None):
    """Run command (string or sequence) in a Unix shell.

    If CAPTURE is true, capture the standard output of the command and return it
    as a string. In this case, an error is raised if the exit status of the
    command is non-zero or stderr is not empty. This can be suppressed by
    setting SILENT to true.

    If CAPTURE is 'full, return a list of (stdout stderr exit status). No error
    for exit status or stderr is raised, regardless of the value of silent.

    If CAPTURE is nil, do not capture the output. An error is raised if the exit
    status of the command is non-zero or stderr is not empty. This can be
    suppressed by setting SILENT to true.

    If command is a vector or a list, run it directly. Otherwise, take it as a
    string and:
    
        If shell is t, run command as shell command line with "/bin/sh".

        If shell is otherwise true, use it as the shell and run command in it.
    
        If shell is unspecified, run command with "/bin/sh" if it contains shell
        meta characters. Otherwise, split the string into a list and run it
        directly.

        If shell is nil, split the string into a list and run it directly.

    """
    if command.isSequence():
        command = list(map(str, Seq2Array(command)))
    else:
        command = str(command)
        if shell:
            if shell is T:
                shell = "/bin/sh"
            command = [str(shell), "-c", command]
        elif shell is None:
            if any([ ch in shellmeta for ch in command ]):
                command = ["/bin/sh", "-c", command]
            else:
                command = command.split()
        else:                           # must be Nil
            command = command.split()

    if capture:
        full_result = capture is fullSymbol
        result = y.backquote(command, full_result=full_result,
                             silent=silent is Nil)
        if full_result:
            return Seq2List((String(result[0]), String(result[1]),
                             Number(result[2])))
        return String(result)
    cpl = subprocess.run(command)
    if silent:
        return cpl.returncode
    cpl.check_returncode()
    return T


