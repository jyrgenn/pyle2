# type assertion

import jpylib as y

import pglobal
from objects import *
from utils import *
from . import *

@special()
def declare(*assertions):
    """Declare and assert the type of variables.

    Arguments are (TYPE1 VAR1a ...) assertions. Example:

      (defun substring (s start end)
        (declare (string s) (number start end))
        ...)

    If any of the VARn is not of TYPEn, raise an error; otherwise, return t.

    All arguments are symbols (unevaluated); the TYPEn are the names of types,
    the VARn are the names of variables whose bindings are checked.

    If this form occurs as the first form of a lambda definition (after a
    potential docstring), it is not called as a function, but rather treated as
    syntax that encodes type checks.

    """
    for assertion in assertions:
        typesym, *vars = assertion
        if not typesym.isSymbol():
            raise PyleArgTypeError("declare", "type", typesym, "symbol")

        for var in vars:
            if not var.isSymbol():
                raise PyleArgTypeError("declare", "var", var, "symbol")
            value = var.getValue()
            checkname = "is" + str(typesym).title()
            checkfunc = getattr(value, checkname, None)
            if not checkfunc:
                raise PyleTypeAssertionError(
                    "declared type for variable {} is unknown: {}",
                    var, typesym)
            if not checkfunc():
                raise PyleTypeAssertionError(
                    "variable {} is declared type {}, but value is {:r} ({})",
                    var, typesym, value, value.type())
    return T


# EOF
