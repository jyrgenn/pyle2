# system-related builtin functions

import os
import platform

import pglobal
from objects import *
from utils import *
from . import *

@builtin()
def exit(arg=None):
    """Exit Pyle.
    If an argument is present, it may be a number, in which case this is the
    exit status of the program (which defaults to zero); otherwise, it will be
    printed to stderr, and the exit status is 1.

    """
    if arg is None:
        status = 0
    elif arg.isNumber() or arg.isString():
        status = arg.value()
    else:
        status = str(arg)
    sys.exit(status)


@builtin()
def error(message, *args):
    """Raise an error; format Message with ARGS."""
    raise PyleProgramError(str(message), *args)


@special()
def errset(expr, print_error=True):
    """Return the value of EXPR as a singleton list, or nil in case of an error.
    In the latter case, the error object is in *last-error*, and, if
    optional PRINT-ERROR is non-nil or omitted, it is printed as well.
    """
    savedInErrset = pglobal.inErrset
    pglobal.inErrset = True
    try:
        return Pair(pglobal.Eval(expr), Nil)
    except PyleException as pe:
        if print_error:
            print(pe)
        return Nil
    finally:
        pglobal.inErrset = savedInErrset


@builtin()
def gensym(prefix="G"):
    """Return a new, (not really) uninterned symbol with optional PREFIX."""
    return Symbol.gensym(prefix)


@builtin()
def type(object):
    """Return the type of OBJECT as a symbol.
    This serves as basis for all the listp, symbolp etc. functions.
    """
    return object.type()


# TODO
@builtin()
def describe(arg):
    """Return a list describing the specifed object."""
    lc = ListCollector(arg.type())
    if arg.isSymbol():
        lc.append(arg)
        val = arg.getValue(must_exist=False) or Nil
        if val:
            val = cons(val, Nil)
        lc.append(cons(pglobal.Symbol("val"), val))
        fun = arg.function() or Nil
        if fun:
            fun = cons(fun, Nil)
        lc.append(cons(pglobal.Symbol("fun"), fun))
        props = arg.props() or Nil
        if props:
            props = Seq2List([ cons(k, v) for k, v in props ])
        lc.append(cons(pglobal.Symbol("props"), props))
    elif arg.isString() or arg.isList() or arg.isTable():
        lc.append(Number(arg.length()))
    else:
        lc.append(arg)
    return lc.list()
                       

# TODO
@builtin()
def debug(*args):
    """"""
    print("DEBUG:", *args)
    return Nil


@builtin()
def python_eval(string):
    """Evaluate a string as a python expression; resturn result as string."""
    return String(eval(str(string)))


@builtin()
def python_exec(string):
    """Execute a string as a python statement."""
    exec(str(string))
    return T


@builtin()
def symbols(*args):
    """Return a list of all symbols."""
    return Seq2List(symbolTable.values())

@builtin()
def abort_if(symbol, formatstr, *args):
    trap_if(symbolArg(symbol), str(formatstr), *args, tag="ABORT")
    return Nil

@builtin()
def _trace_if(symbol, formatstr, *args):
    trap_if(symbolArg(symbol), str(formatstr), *args, tag="DEBUG", abort=False)
    return Nil


# System things

@builtin()
def pyle_version():
    return Symbol(pglobal.pyle_version)

@builtin()
def python_implementation(arg=None):
    return Symbol(platform.python_implementation().lower())

@builtin()
def python_platform():
    return Seq2List(map(String,
                        (platform.python_implementation(),
                         platform.python_version(),
                         platform.node(),
                         platform.platform())))


# EOF
