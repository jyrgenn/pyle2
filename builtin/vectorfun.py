# table builtin functions

from objects import *
from . import *

def vectorArg(arg, name=""):
    """Trigger an error if arg is not a vector.

    Return the argument.
    """
    if arg.isVector():
        return arg
    argError(arg, name, "vector")


@builtin()
def vector(*args):
    """Return a new vector filled with the arguments.
    """
    return Vector(args)


@builtin()
def vector_get(vector, index, default=None):
    """Get the value at INDEX from VECTOR.

    If the index does not exist in the vector, return DEFAULT, if
    specified; otherwise, raise an error. If the index is negative,
    count from the end; -1 is the last element.

    """
    return vectorArg(vector).get(numberArg(index), default)


@builtin()
def vector_inc(vector, index, delta=None):
    """Increment the value at INDEX in VECTOR with DELTA (default 1).

    Return the new value. The value at index must be a number, or nil, which is
    taken as zero. If the index does not exist in VECTOR, raise an error.

    """
    vector = vectorArg(vector)
    i = numberArg(index)
    value = vector.get(i)
    if value is Nil:
        value = 0
    elif value.isNumber():
        value = value.value()
    else:
        raise PyleDataTypeError("vector {} value at index {} not a number: {}",
                                vector, index, value)
    if delta is None:
        delta = 1
    else:
        delta = numberArg(delta)
    newvalue = Number(value + delta)
    vector.put(key, newvalue)
    return newvalue


@builtin()
def vector_set(vector, index, value):
    """In VECTOR, set the value at INDEX to VALUE and return it."""
    return vectorArg(vector).set(numberArg(index), value)


@builtin()
def vector_elems(vector):
    """Return the elements of VECTOR as a list."""
    return Seq2List(vectorArg(vector))

# EOF
