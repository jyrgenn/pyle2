# builtin functions dealing with variables

import pglobal
from . import *

@builtin()
def _set(symbol, value):
    """Set SYMBOL to VALUE and return VALUE."""
    symbolArg(symbol, "symbol").setValue(value)
    return value

@builtin()
def makunbound(symbol):
    """Make SYMBOL's value undefined, return SYMBOL."""
    sym = symbolArg(symbol)
    sym.setValue(None)
    return sym

@builtin()
def fmakunbound(symbol):
    """Make SYMBOL's function definition to be undefined, return SYMBOL."""
    sym = symbolArg(symbol)
    sym.setFunction(None)
    return sym

@special()
def defvar(symbol, initial_value=Nil, docstring=None):
    """Define variable SYMBOL with optional INITIAL-VALUE and DOCSTRING.
    If the variable is already bound, its value is not changed.
    """
    sym = symbolArg(symbol)
    if sym not in pglobal.rootEnvironment:
        pglobal.rootEnvironment.bindValue(symbol, pglobal.Eval(initial_value))
    return sym
    
@special()
def defparameter(symbol, initial_value=Nil, docstring=None):
    """Define variable SYMBOL with optional INITIAL-VALUE and DOCSTRING.
    If the variable is already bound, its value is changed (which is not the
    case with defvar.

    """
    sym = symbolArg(symbol)
    pglobal.rootEnvironment.bindValue(symbol, pglobal.Eval(initial_value))
    return sym
    

@builtin()
def boundp(symbol):
    """Return t if SYMBOL is bound to a value, nil otherwise."""
    return boolOb(symbolArg(symbol).getValue(must_exist=False) is not None)


