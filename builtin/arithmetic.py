# arithmetic builtin functions

import math
import random

from . import *
from .factors import prime_factors, allPrimes, isPrime

@builtin("*")
def multiply(*args):
    """Multiply all given arguments (numbers)."""
    result = 1
    for arg in args:
        result *= numberArg(arg)
    return Number(result)


@builtin("+")
def add(*args):
    """Add all given arguments (numbers)."""
    result = 0
    for arg in args:
        result += numberArg(arg)
    return Number(result)


@builtin("/")
def divide(arg1, *args):
    """Divide the first argument by all following arguments."""
    result = numberArg(arg1)
    if args:
        for arg in args:
            result /= numberArg(arg)
    else:
        result = 1 / result
    return Number(result)


@builtin("-")
def subtract(arg1, *args):
    """From the first argument, subtract all following arguments."""
    result = numberArg(arg1)
    if args:
        for arg in args:
            result -= numberArg(arg)
    else:
        result = -result
    return Number(result)


@builtin()
def factor(n):
    """Return the list of prime factors of N."""
    return Seq2List(map(Number, prime_factors(numberArg(n))))


@builtin()
def prime_number_p(num):
    """Return t iff NUM is prime."""
    num = numberArg(num)
    if isinstance(num, int):
        return boolOb(isPrime(num))
    return Nil


@builtin()
def prime_after(n):
    """Return the next prime bigger than N."""
    n = numberArg(n)
    for p in allPrimes():
        if p > n:
            return Number(p)


@builtin()
def _int(num):
    """Return the integer part of number NUM."""
    return Number(int(numberArg(num)))


@builtin()
def sqrt(num):
    """Return the square root of NUM."""
    return Number(math.sqrt(numberArg(num)))


@builtin()
def isqrt(num):
    """Return the integer square root of NUM."""
    return Number(int(math.sqrt(numberArg(num))))


@builtin()
def integer_length(integer):
    """Returns the number of bits needed to represent integer
    in binary two's-complement format.
    """
    return Number(numberArg(integer).bit_length())


@builtin()
def ash(integer, count):
    """Perform the arithmetic shift operation on the binary representation
    of integer, which is treated as if it were binary.

    Shifts integer arithmetically left by count bit positions if count is
    positive, or right count bit positions if count is negative. The shifted
    value of the same sign as integer is returned.

    """
    i = numberArg(integer)
    count = numberArg(count)
    if count > 0:
        return Number(i << count)
    elif count < 0:
        return Number(i >> -count)
    else:
        return integer


@builtin()
def expt(base, power):
    """Return BASE raised to the power POWER."""
    return Number(numberArg(base) ** numberArg(power))


@builtin()
def ceiling(number):
    """Return NUMBER truncated to an integer towards positive infinity."""
    return Number(math.ceil(numberArg(number)))


@builtin()
def round(number):
    """Return NUMBER rounded to the nearest integer."""
    num = numberArg(number)
    return Number(math.trunc(num + math.copysign(0.5, num)))


@builtin()
def floor(number):
    """Return NUMBER truncated to an integer towards negative infinity."""
    return Number(math.floor(numberArg(number)))

@builtin()
def log(number, base=None):
    """Return the logarithm of NUMBER in base BASE.
    If BASE is not supplied, its value is e.
    """
    if base is None:
        base = math.e
    else:
        base = numberArg(base)
    return Number(math.log(numberArg(number), base))


@builtin()
def atanh(number):
    """Return the arc hyperbolic tangent of NUMBER."""
    return Number(math.atanh(numberArg(number)))


@builtin()
def acosh(number):
    """Return the arc hyperbolic cosine of NUMBER."""
    return Number(math.acosh(numberArg(number)))


@builtin()
def asinh(number):
    """Return the arc hyperbolic sine of NUMBER."""
    return Number(math.asinh(numberArg(number)))


@builtin()
def tanh(number):
    """Return the hyperbolic tangent of NUMBER."""
    return Number(math.tanh(numberArg(number)))


@builtin()
def cosh(number):
    """Return the hyperbolic cosine of NUMBER."""
    return Number(math.cosh(numberArg(number)))


@builtin()
def sinh(number):
    """Return the hyperbolic sine of NUMBER."""
    return Number(math.sinh(numberArg(number)))


@builtin()
def atan(number):
    """Return the arc tangent of NUMBER."""
    return Number(math.atan(numberArg(number)))


@builtin()
def acos(number):
    """Return the arc cosine of NUMBER."""
    return Number(math.acos(numberArg(number)))


@builtin()
def asin(number):
    """Return the arc sine of NUMBER."""
    return Number(math.asin(numberArg(number)))


@builtin()
def tan(number):
    """Return the tangent of NUMBER."""
    return Number(math.tan(numberArg(number)))


@builtin()
def cos(number):
    """Return the arc cosine of NUMBER."""
    return Number(math.cos(numberArg(number)))


@builtin()
def sin(number):
    """Return the sine of NUMBER."""
    return Number(math.sin(numberArg(number)))


@builtin()
def integerp(ob):
    """Return t if OB is an integer number."""
    if ob.isNumber() and isinstance(ob.value(), int):
        return T
    return Nil


@builtin()
def truncate(num):
    """Truncate NUM towards zero."""
    value = numberArg(num)
    if isinstance(value, int):
        return num
    return Number(int(value))


@builtin()
def _random(limit=None, integer=None):
    if limit is None:
        the_limit = 1
    else:
        the_limit = numberArg(limit)
    if integer:
        result = random.randrange(0, the_limit)
    elif limit:
        result = random.uniform(0, the_limit)
    else:
        result = random.random()
    return Number(result)


_digitWeights = [
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7,
    8, 9, -1, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18,
    19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1,
    -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
    23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1,
]


@builtin()
def digit_char_p(char, radix=None):
    char = str(char)
    if radix is None:
        radix = 10
    else:
        radix = numberArg(radix)
    if radix < 2 or radix > 36:
        raise PyleValueError("radix must be in the range 2..36 (is {})",
                             radix)
    if len(char) != 1:
        raise PyleValueError("char must be string of length 1 (is {:r})",
                             char)
    if char > 'z' or char < "0":
        return Nil
    pval = _digitWeights[ord(char)]
    if pval >= radix or pval < 0:
        return Nil
    return Number(pval)

@builtin()
def _rem(num1, num2):
    """Return remainder of NUM1 divided by NUM2."""
    return Number(math.remainder(numberArg(num1), numberArg(num2)))

@builtin()
def _mod(num1, num2):
    """Return modulus of NUM1 divided by NUM2."""
    num1 = numberArg(num1)
    num2 = numberArg(num2)
    if isinstance(num1, int) and isinstance(num2, int):
        return Number(num1 % num2)
    return Number(math.fmod(num1, num2))

# EOF
