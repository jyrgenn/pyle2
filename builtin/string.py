# builtin functions dealing with strings (more to come)

from objects import *
from . import *
from utils import *

start_symbol = Symbol("start")
end_symbol = Symbol("end")

@builtin()
def string(*objects):
    """Make a string from all arguments and return it."""
    if len(objects) == 1 and objects[0].isString():
        # is this premature? anyway, seems to make sense
        return objects[0]
    return String(Seq2String(objects))


@builtin()
def search_string(str1, str2):
    """Search STR1 in STR2 and return its position, or nil if it wasn't found.
    """
    s1 = str(str1)
    s2 = str(str2)
    result = s2.find(s1)
    if result >= 0:
        return Number(result)
    return Nil


@builtin()
def regexp(string):
    """Make a regular expression from STRING."""
    if string.isRegexp():
        return string
    return Regexp(str(string))


@builtin()
def regexp_match(regexp, string, all=None):
    """Match regular expression REGEXP the STRING and return the result.

    If there is no match, return Nil. Otherwise, return a list containing the
    full match and, if present, the matches of the groups. If ALL is non-nil,
    return all matches.

    """
    if not regexp.isRegexp():
        regexp = Regexp(str(regexp))
    if all:
        return regexp.findall(string)
    return regexp.match(string)


@builtin()
def regexp_replace(regexp, string, newtxt, count=None):
    """Replace matches of REGEXP in STRING with NEWTXT.
    COUNT is the maximum number of replacements (default 0 for all).
    """
    if count is None:
        count = 0
    else:
        count = int(numberArg(count))
    if not regexp.isRegexp():
        regexp = Regexp(str(regexp))
    return String(regexp.replace(str(string), str(newtxt), count))
    
@builtin()
def split_string(string, separator=None, limit=None):
    """Split a string and return the list of parts.

    If separator is not specified or nil, the string is split at
    whitespace; runs of consecutive whitespace are regarded as a
    single separator, and the result will contain no empty strings
    at the start or end if the string has leading or trailing
    whitespace.

    If separator is t, the string is split into single characters.
    If separator is a string, the string is split at each occurrence
    of the string, with possibly empty parts in the result, e.g.

    > (split-string "Erdbeerenernte" "e")
    => ("Erdb" "" "r" "n" "rnt" "")

    If separator is a regexp, the string is split at matches of the
    regexp. Groups in the patters are return as parts of the list.

    If limit is specified as a positive integer, it limits the
    number of parts returned. The last part contains the rest of the
    string, except if the separator is nil -- in this case, a list
    of the first limit characters of the string is returned.

    """
    s = str(string)
    if limit is None:
        # No, I am *not* kidding. Python is, to keep it polite, really weird in
        # places. Very, very weird.
        if separator is None:
            limit = -1
        elif separator.isRegexp():
            limit = 0
        else:
            limit = -1
    else:
        limit = int(numberArg(limit)) - 1
    if separator in (None, Nil):
        result = s.split(None, limit)
    elif separator is T:
        l = list(s)
        if limit > 0:
            l1 = l[0:limit]
            l2 = l[limit:]
            if l2:
                l1.append("".join(l2))
            l = l1            
        return Seq2List(map(String, l))
    elif separator.isRegexp():
        result = separator.split(s, limit)
    else:
        result = s.split(str(separator), limit)
    return Seq2List(map(String, result))
    

@builtin()
def strip_string(s, chars=None, where=None):
    """Strip characters off the end(s) of string S and return the result.

    If CHARS is not specified or nil, all whitespace characters are stripped.
    Otherwise, the characters in CHARS are stripped.

    If WHERE is the symbol `start', characters are stripped from the start of
    the string; if it is the symbol `end', characters are stripped from the end
    of the string; otherwise, they are stripped from the start and the end of
    the string.

    """
    if chars:
        if chars.isList():
            charbag = Seq2String(chars)
        else:
            charbag = str(chars)
    else:
        # Doesn't matter if it's an empty List or String
        charbag = None
    the_string = str(s)
    if where is start_symbol:
        method = the_string.lstrip
    elif where is end_symbol:
        method = the_string.rstrip
    else:
        method = the_string.strip
    return String(method(charbag))
    
@builtin()
def string_upcase(s, start=None, end=None):
    """Return STRING with all lowercase chars replaced by uppercase chars.
    START and END may specify the region of the string to be treated;
    defaults are 0 and the end of the string.
    """
    the_string = str(s)
    if start is None and end is None:
        return String(the_string.upper())
    if start is not None:
        start = int(numberArg(start))
    if end is not None:
        end = int(numberArg(end))
    else:
        end = len(the_string)
    if end < start:
        end = start
    return String(the_string[:start] + the_string[start:end].upper()
                  + the_string[end:])


@builtin()
def string_downcase(s, start=None, end=None):
    """Return STRING with all uppercase chars replaced by lowercase chars.
    START and END may specify the region of the string to be treated;
    defaults are 0 and the end of the string.
    """
    the_string = str(s)
    if start is None and end is None:
        return String(the_string.lower())
    if start is not None:
        start = int(numberArg(start))
    if end is not None:
        end = int(numberArg(end))
    else:
        end = len(the_string)
    if end < start:
        end = start
    return String(the_string[0:start] + the_string[start:end].lower()
                  + the_string[end:])

    
@builtin()
def string_capitalize(s, start=None, end=None):
    """Return STRING with all uppercase chars replaced by lowercase chars.
    START and END may specify the region of the string to be treated;
    defaults are 0 and the end of the string.
    """
    the_string = str(s)
    if start is None and end is None:
        return String(the_string.title())
    if start is not None:
        start = int(int(numberArg(start)))
    if end is not None:
        end = int(int(numberArg(end)))
    else:
        end = len(the_string)
    if end < start:
        end = start
    return String(the_string[0:start] + the_string[start:end].title()
                  + the_string[end:])


@builtin()
def substring(s, start, end=None):
    """Return a substring of STRING, bounded by indices START and END
    (or the end of the original string).
    """
    if end is Nil:
        end = None
    if end is not None:
        end = int(numberArg(end, "end"))
    return String(str(s)[int(numberArg(start, "start")):end])


# EOF
