# builtin functions for I/O

import io
import sys
import glob

import pio
from objects import *
from . import *


def portArg(arg, name=""):
    """Trigger an error if arg is not a port or None.

    Return the argument, or the stdout port if arg is None.
    """

    if arg is None:
        return pglobal.Stdout
    if arg.isPort():
        return arg
    argError(arg, name, "port")


@builtin()
def open_interactive_stream(port, prompt=None, history_file=None):
    """Open an interactive input port for the specified port.
    Return a new port from which can be read using the interactive line
    editor using PROMPT. If PROMPT is a function, it will be called to
    determine the line editor prompt. If optional HISTORY-FILE is non-nil,
    read and write a history of the session to that file name (a string).

    Implementation note: This can be done only for the standard input, as
    Ptyhon's readline API seems to be limited to that. So, the port argument is
    effectively ignored.

    """
    port = Port(port._file, "*interactive-input*", can_read=True,
                is_interactive=True, prompt=prompt, history_file=history_file)
    port.readHistory()
    return port


@builtin()
def read(input=None, eof_error_p=None, eof_value=None):
    """Read an Object from INPUT (a string or port) or stdin.
    """
    if not input:
        port = pglobal.Stdin
    elif input.isString():
        port = StringInputPort(input)
    else:
        port = portArg(input)

    if eof_error_p is None:
        eof_error_p = T
    if eof_value is None:
        eof_value = Nil

    result = pio.Reader(port).read()
    if result is None:
        if eof_error_p:
            raise PyleEOFError(port)
        return eof_value
    return result


@builtin()
def princ_(arg, port=None):
    """Print ARGs to stdout (or the specified port), without quoting.
    Return the string printed.
    """
    port = portArg(port)
    port.write(arg)
    port.flush()
    return String(arg)


@builtin()
def prin1_to_string(arg):
    """Print ARG to a string quoted to be read by (read); return the string."""
    return String(repr(arg))


@builtin()
def prin1(arg, port=None):
    """Print ARG to PORT (or standard output) quoted to be read by (read)."""
    port = portArg(port)
    value = repr(arg)
    port.write(value)
    return String(value)


@builtin()
def load(filename, missing_ok=False, silent=False):
    """Read and evaluate expressions from the file named FILENAME.

    If there is no "/" in the file name, search for the file in the directories
    in *load-path*, and try to find it without and with a ".l" suffix.
    
    Return t on success. If the file cannot be found, return nil if missing_ok
    is true; otherwise, throw an error.

    """
    return pio.load_from_path(str(filename), missing_ok, silent)


@builtin()
def format(port, format_string, *args):
    """Format a FORMAT-STRING with ARGS and return or print the result.
    The formatting is done via Python's {} formatting specifications, but
    only with positional arguments.
    If PORT is nil, return the resulting string; if it is t, print it;
    otherwise, do neither and return nil.
    """
    # TODO: why doesn't this work with 'pass: {:23} »{}«\n' ?
    s = str(format_string).format(*args)
    if port is T:
        port = pglobal.Stdout
    elif port is Nil:
        return String(s)

    if port.isPort():
        port.write(s)
    else:
        PyleArgTypeError("format", "port", port, "t or nil or a port")
    return Nil


@builtin()
def open_(fname, direction=None, if_exists=None, if_not_exists=None):
    """
    Open a file (or reopen a port) and return the connected port.
    Options: direction can be :input or :output or :io (default :input)
             if-exists can be :append or :overwrite or :error or nil
                       (:output default :overwrite, else nil)
             if-does-not-exist can be :error or :create or nil
    """
    do_read = True
    do_write = False
    if not direction or direction is portDirReadSym:
        pass
    elif direction is portDirWriteSym:
        do_read = False
        do_write = True
    elif direction is portDirRWSym:
        do_write = True
    else:
        raise PyleArgTypeError("open", "direction", direction,
                               "one of :input or :output or :io")
    # now determine the open() mode
    mode = ""
    if do_read and do_write:
        mode = "w+"
        if if_exists is portAppendSym:
            mode = "a"
        if if_exists is portOverwriteSym:
            mode = "w+"
        if if_exists is portErrorSym:
            mode = "x+"
    elif do_read:
        mode = "r"
        if if_not_exists is portCreateSym:
            mode = "r+"
    elif do_write:
        mode = "w"
        if if_exists is portAppendSym:
            mode = "a"
        elif if_exists is portOverwriteSym:
            mode = "w"
        elif if_exists is portErrorSym:
            mode = "x"
    filename = str(fname)
    try:
        file = open(filename, mode=mode)
        return Port(file, filename, do_read, do_write)
    except Exception as e:
        raise PyleOpenError("cannot open file '{}': {}", fname, e)


def _input_args(port, eof_error_p, eof_value):
    if port is None:
        port = pglobal.Stdin
    if eof_error_p is None:
        eof_error_p = T
    if eof_value is None:
        eof_value = Nil
    return port, eof_error_p, eof_value


@builtin()
def read_line(port=None, eof_error_p=None, eof_value=None, strip_newline=False):
    """Read a line from PORT (default *stdin*).

    If EOF-ERROR-P is non-nil (that is the default), return error on EOF.
    Otherwise, return EOF-VALUE (or nil).

    """
    port, eof_error_p, eof_value = _input_args(port, eof_error_p, eof_value)
    if port is T:
        port = pglobal.Stdin
    line = port.readline()
    if line == "":
        if eof_error_p:
            raise PyleEOFError(port)
        return eof_value
    if strip_newline:
        line = line.rstrip("\n")
    return String(line)


@builtin()
def read_char(port=None, eof_error_p=None, eof_value=None):
    """Read a character from PORT (default *stdin*).

    If EOF-ERROR-P is non-nil (that is the default), return error on EOF.
    Otherwise, return EOF-VALUE (or nil).

    """
    port, eof_error_p, eof_value = _input_args(port, eof_error_p, eof_value)
    char = port.readchar()
    if char == "":
        if eof_error_p:
            raise PyleEOFError(port)
        return eof_value
    return String(char)


@builtin()
def close_(port):
    portArg(port).close()
    return Nil


@builtin()
def directory(pathspec):
    """Return a list of path names matching PATHSPEC.
    PATHSPEC may contain wildcards. If not, the resulting list has
    either one or zero elements.
    """
    return Seq2List(map(String, sorted(glob.glob(str(pathspec)))))


@builtin()
def make_string_output_stream():
    """Return an output stream that will accumulate all output given it
    for the benefit of the function `get-output-stream-string'.
    """
    stream = io.StringIO()
    return Port(stream, "*output-string*", can_read=False, can_write=True,
                is_stringio=True)


@builtin()
def make_string_input_stream(string):
    """Return an input stream that will supply the characters in string.
    """
    stream = io.StringIO(str(string))
    return Port(stream, "*input-string*", can_read=True, can_write=False,
                is_stringio=True)


@builtin()
def get_output_stream_string(stream):
    return String(portArg(stream).value())


# EOF
