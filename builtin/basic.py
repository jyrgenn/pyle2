# very basic builtin functions, core of the language

import pglobal
from . import *


@builtin()
def car(list):
    """Return the car of LIST."""
    return listArg(list).car()

@builtin()
def cdr(list):
    """Return the cdr of LIST."""
    return listArg(list).cdr()

@builtin()
def cons(arg1, arg2):
    """Return a pair of (ARG1 . ARG2)."""
    return Pair(arg1, arg2)

@builtin()
def apply(function, *args):
    """Apply FUNCTION to ARGS (a spreadable args list).

    """
    return functionArg(function).call(spread_arglist("apply", args))

@special()
def quote(arg):
    """Return the (un-evaluated) ARG."""
    return arg
    
@builtin()
def intern(arg):
    """Return the symbol with the name ARG."""
    sym = Symbol(str(arg))
    y.debug("(intern {}) => {}".format(arg, sym.dump()))
    return sym

@special()
def function(arg):
    """Return the function bound to symbol ARG (or itself, if it is a function).
    """
    if arg.isFunction():
        return arg
    if arg.isSymbol():
        funcval = arg.function()
        if funcval:
            return funcval
        raise PyleUndefFunctionError(arg)
    raise PyleDataTypeError("not a function or function symbol: {}", arg)


@builtin()
def eq(arg1, arg2):
    """Return t if ARG1 and ARG2 are the same object.

    As numbers and strings, like symbols, are interned, equal numbers or strings
    are also the same object.

    """
    return boolOb(arg1 is arg2)


@builtin()
def eval_(expr):                        # avoid name clash with Python builtin
    """Evaluate EXPR and return the result."""
    return pglobal.Eval(expr)


@special()
def let(bindings, *bodyforms):
    """(let ((symbol1 [value1]) ...) bodyforms...)
    Evaluate BODYFORMS with local BINDINGS, return value of last bodyform.
    Each of BINDINGS is of the form Symbol or (Symbol) or (Symbol Value), where
    the first two bind Symbol to nil. All Values are evaluated before any
    variable bindings are done.

    Remark: letrec can be defined as a macro constructing a series
    of nested let bindings.

    """
    with newEnvironment():
        params = []
        values = []
        for binding in bindings:
            if binding.isSymbol():
                params.append(binding)
                values.append(Nil)
            else:
                param, rest = binding.cxr()
                params.append(param)
                values.append(pglobal.Eval(rest.car() or Nil))
        for param, value in zip(params, values):
            y.trace("let binds", param, "=>", value)
            bindValues(param, value)

        return pglobal.EvalProgn(bodyforms)


@special("let*")
def letrec(bindings, *bodyforms):
    """(let* ((symbol1 [value1]) ...) bodyforms...)
    Evaluate BODYFORMS with local BINDINGS, return value of last bodyform. Each
    of BINDINGS is of the form Symbol or (Symbol) or (Symbol Value), where the
    first two bind Symbol to nil. Values are evaluated with bindings of earlier
    variables in the same let* already in place.

    """
    with newEnvironment():
        for binding in bindings:
            if binding.isSymbol():
                bindValues(binding, Nil)
            else:
                param, rest = binding.cxr()
                bindValues(param, pglobal.Eval(rest.car()))
        return pglobal.EvalProgn(bodyforms)


@builtin()
def put(symbol, value, prop):
    """Set SYMBOL's property PROP to VALUE and return VALUE."""
    symbolArg(symbol).putprop(symbolArg(prop), value)
    return value


@builtin()
def get(symbol, prop):
    """Return the value of SYMBOL's property PROP."""
    return symbolArg(symbol).getprop(symbolArg(prop))


@builtin()
def rplaca(pair, new_car):
    """Set PAIR's car to NEW-CAR and return PAIR.
    This function only sets a new car. If you want to get a new car, make sure
    you have the funds and visit a car dealership.

    """
    pairArg(pair).rplaca(new_car)
    return pair
                                 
                                 
@builtin()
def rplacd(pair, new_cdr):
    """Set PAIR's car to NEW-CDR and return PAIR.
    To my regret, the wording "new cdr" does not present an opportunity to make
    cheesy jokes about new cdrs.

    """
    pairArg(pair).rplacd(new_cdr)
    return pair


@builtin("list*")
def list_star(*args):
    """Return a list with ARGS as members, with the last as the end of the list.
    list* is like list except that the last argument to list becomes the car
    of the last cons constructed, while the last argument to list* becomes
    the cdr of the last cons constructed.
    """
    return spread_arglist("list*", args)


