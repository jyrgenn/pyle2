# alerts from jpylib

import jpylib as y

import pglobal
from objects import *
from utils import *
from . import *


_names = []                             # level => namesym
_levels = {}                            # namesym => level

for level in range(y.alerts.cfg.max_level + 1):
    _namesym = pglobal.Symbol(y.alert_level_name(level))
    _names.append(_namesym)
    _levels[_namesym] = level


@builtin()
def alert_level(level=None):
    """Optionally set alert level to LEVEL; return current alert level.
    The LEVEL argument must be numeric or one of the L_* symbols.
    """
    if level is not None:
        if level.isNumber():
            y.alert_level(level.value)
        elif level in _names:
            y.alert_level(_levels[level])
        else:
            raise PyleArgTypeError("level arg must be level symbol or number")
    return _names[y.alert_level()]
        

@builtin()
def alert_level_name(level):
    """Return the name symbol for the specified alert level LEVEL."""
    num_level = min(numberArg(level), len(_names) - 1)
    return _names[num_level]


@builtin()
def alert_level_up():
    """Raise the alert level by one, if not already at maximum.
    Return the new alert level.
    """
    y.alert_level_up()
    return _names[y.alert_level()]
    

@builtin()
def alert_level_zero():
    """Set the alert level to zero. Return the new alert level.
    """
    y.alert_level_zero()
    return _names[y.alert_level()]


@builtin()
def alert_notice_p():
    """Return true iff the alert level is at L_NOTICE."""
    return boolOb(y.is_notice())


@builtin()
def alert_info_p():
    """Return true iff the alert level is at L_INFO."""
    return boolOb(y.is_info())


@builtin()
def alert_debug_p():
    """Return true iff the alert level is at L_DEBUG."""
    return boolOb(y.is_debug())


@builtin()
def alert_trace_p():
    """Return true iff the alert level is at L_TRACE."""
    return boolOb(y.is_trace())


@builtin()
def alert_fatal(*args):
    """Print a fatal alert message and end the program."""
    return boolOb(y.fatal(*map(str, args)))


@builtin()
def alert_err(*args):
    """Print an error alert message."""
    return boolOb(y.err(*map(str, args)))


@builtin()
def alert_notice(*args):
    """Print a notice message if alert level is at L_NOTICE or greater."""
    return boolOb(y.notice(*map(str, args)))


@builtin()
def alert_info(*args):
    """Print an info message if alert level is at L_INFO or greater."""
    return boolOb(y.info(*map(str, args)))


@builtin()
def alert_debug(*args):
    """Print a debug message if alert level is at L_DEBUG or greater."""
    return boolOb(y.debug(*map(str, args)))


@builtin()
def alert_trace(*args):
    """Print a trace message if alert level is at L_TRACE."""
    return boolOb(y.trace(*map(str, args)))


@builtin()
def alert_tracef(formatstring, *args):
    """Print a formatted trace message if alert level is at L_TRACE.

    The first argument is a Python format string, the following ones arguments
    to be formatted using that format string.

    """
    return boolOb(y.tracef(*map(str, args)))


@builtin()
def alert_debugf(*args):
    """Print a formatted debug message if alert level is at L_DEBUG or greater.

    The first argument is a Python format string, the following ones arguments
    to be formatted using that format string.

    """
    return boolOb(y.debugf(*map(str, args)))


@builtin()
def alert_infof(*args):
    """Print a formatted info message if alert level is at L_INFO or greater.

    The first argument is a Python format string, the following ones arguments
    to be formatted using that format string.

    """
    return boolOb(y.infof(*map(str, args)))


@builtin()
def alert_noticef(*args):
    """Print a formatted debug message if alert level is at L_NOTICE or greater.

    The first argument is a Python format string, the following ones arguments
    to be formatted using that format string.

    """
    return boolOb(y.noticef(*map(str, args)))


@builtin()
def alert_errorf(*args):
    """Print a formatted error message.

    The first argument is a Python format string, the following ones arguments
    to be formatted using that format string.

    """
    return boolOb(y.errorf(*map(str, args)))


@builtin()
def alert_fatalf(*args):
    """Print a formatted error message and end the program.

    The first argument is a Python format string, the following ones arguments
    to be formatted using that format string.

    """
    return boolOb(y.fatalf(*map(str, args)))

