# comparisons using the generic operators

from objects import *
from . import *

_cmpname = {}
def _lt(ob1, ob2): return ob1 <  ob2
_cmpname[_lt] = "`<'"
def _le(ob1, ob2): return ob1 <= ob2
_cmpname[_le] = "`<='"
def _eq(ob1, ob2): return ob1 == ob2
_cmpname[_eq] = "`=='"
def _ne(ob1, ob2): return ob1 != ob2
_cmpname[_ne] = "`!='"
def _gt(ob1, ob2): return ob1 >  ob2
_cmpname[_gt] = "`>'"
def _ge(ob1, ob2): return ob1 >= ob2
_cmpname[_ge] = "`>='"

def compArg(arg, name=""):
    """Trigger an error if arg is not comparable.

    Return the argument.
    """
    if arg.isComparable():
        return arg
    argError(arg, name, "comparable")


def comparison(op, ob1, *args):
    the_type = type(compArg(ob1, _cmpname[op]))
    for arg in args:
        if type(arg) != the_type:
            raise PyleInvalidCallError(
                "cannot compare `{:r}' ({}) with `{:r}' ({})",
                ob1, type_of(ob1), arg, type_of(arg))
    for arg in args:
        if op(ob1, arg):
            ob1 = arg
            continue
        return Nil
    return T


@builtin("<")
def lessthan(ob1, *args):
    return comparison(_lt, ob1, *args)


@builtin("<=")
def lessorequal(ob1, *args):
    return comparison(_le, ob1, *args)


@builtin("=")
def equal(ob1, *args):
    return comparison(_eq, ob1, *args)


@builtin("!=")
def notequal(ob1, *args):
    the_type = type(compArg(ob1))
    seen = { ob1 }
    for arg in args:
        if type(arg) != the_type:
            raise PyleInvalidCallError(
                "cannot compare `{:r}' ({}) with `{:r}' ({})",
                ob1, type_of(ob1), arg, type_of(arg))
    for arg in args:
        value = arg
        if value in seen:
            return Nil
        seen.add(value)
    return T


@builtin(">")
def greaterthan(ob1, *args):
    return comparison(_gt, ob1, *args)


@builtin(">=")
def greaterorequal(ob1, *args):
    return comparison(_ge, ob1, *args)


@builtin("<=>")
def compare(item1, item2):
    """Return -1, 0, or 1 if ITEM1 is less than, equal to, or greater than
    ITEM2, respectively. Comparable are symbols, numbers, and strings.
    """
    if compArg(item1) < compArg(item2):
        return Number(-1)
    if item1 == item2:
        return Number(0)
    return Number(1)
    



# EOF
