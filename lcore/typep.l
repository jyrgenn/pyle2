;; type predicates

(defun consp (ob)
  "Return t iff OB is a pair, otherwise nil."
  (eq (type-of ob) 'pair))

(defun listp (ob)
  "Return t iff OB is a list, otherwise nil."
  (or (eq ob nil)
      (eq (type-of ob) 'pair)))

(defun symbolp (ob)
  "Return t iff OB is a symbol, otherwise nil."
  (eq (type-of ob) 'symbol))

(defun stringp (ob)
  "Return t iff OB is a string, otherwise nil."
  (eq (type-of ob) 'string))

(defun functionp (ob)
  "Return t iff OB is a function, otherwise nil."
  (let ((type (type-of ob)))
    (or (eq type 'builtin)
        (eq type 'macro)
        (eq type 'lambda))))

(defun macrop (ob)
  "Return non-nil iff OB is a macro."
  (eq (type-of ob) 'macro))

(defun numberp (ob)
  "Return t iff OB is a number, otherwise nil."
  (eq (type-of ob) 'number))

(defun tablep (ob)
  "Return t iff OB is a table, otherwise nil."
  (eq (type-of ob) 'table))

(defun vectorp (ob)
  "Return t iff OB is a vector, otherwise nil."
  (eq (type-of ob) 'vector))

(defun sequencep (ob)
  "Return t iff OB is a sequence, otherwise nil."
  (or (listp ob)
      (vectorp ob)))

(defun regexpp (ob)
  "Return t iff OB is a regular expression, otherwise nil"
  (eq (type-of ob) 'regexp))

(defun atom (ob)
  "Return t iff OB is an atom, otherwise nil."
  (let ((type (type-of ob)))
    (or (eq type 'symbol)
        (eq type 'string)
        (eq type 'number))))




