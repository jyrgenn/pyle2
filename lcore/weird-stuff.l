;;; some weird stuff

(defun easter (year)
  "Calculate the easter date for YEAR. Return a list (year month mday).
The formula to calculate the date is according to Lichtenberg as cited by
Wikipedia in
https://de.wikipedia.org/wiki/Gau%C3%9Fsche_Osterformel#Eine_erg.C3.A4nzte_Osterformel"
  (declare (number year))
  (let* ((x (truncate year))
         (k (div x 100))
         (m (- (+ 15
                  (div (+ (* k 3) 3)
                     4))
               (div (+ (* k 8) 13)
                    25)))
         (s (- 2 (div (+ (* k 3) 3) 4)))
         (a (mod x 19))
         (d (mod (+ (* a 19) m) 30))
         (r (div (+ d (div a 11)) 29))
         (og (- (+ 21 d) r))
         (sz (- 7 (mod (+ x (div x 4) s) 7)))
         (oe (- 7 (mod (- og sz) 7)))
         (os (+ og oe))
         (month 3)
         (mday os))
    (when (> os 31)
      (setf month 4)
      (setf mday (- mday 31)))
    (list x month mday))) 
