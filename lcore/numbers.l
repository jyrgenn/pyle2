;; number ops

(defun 1- (n)
  "Return a number one less than the argument N."
  (- n 1))

(defun 1+ (n)
  "Return a number one bigger than the argument N."
  (+ n 1))

(defun zerop (ob)
  "Return t if the argument is zero; nil otherwise."
  (eq ob 0))

(defun abs (num)
  "Return the absolute value of the number."
  (if (numberp num)
      (if (< num 0)
          (- num)
        num)
    (error "`{}' is not a number" num)))
  
(defun signum (num)
  "Return the sign of the number."
  (cond ((> num 0) 1)
        ((< num 0) -1)
        (t 0)))

(defun evenp (num)
  "Return true iff num is an even number."
  (zerop (% num 2)))

(defun oddp (num)
  "Return true iff num is an odd number."
  (= 1 (% num 2)))

(defun minusp (num)
  "Return true iff num is less than zero."
  (< num 0))

(defun plusp (num)
  "Return true iff num is greater than zero."
  (> num 0))

(defun range (limit-or-start . options)
  "(range limit) or (range start limit [step])
Return a list of numbers similar to the corresponding Python function.
Invoked as (range <limit>), it starts at 0 and ends before limit.
Invoked as (range <start> <limit>), it starts at start and ends before limit.
Optional STEP is the step width between the list members, defaulting to 1.
If any of the values is not an integer, this may behave unexectedly due to
roundoff errors."
  (declare (number limit-or-start))
  (let* (((limit-or-not step) options)
         (step (or step 1))
         (start (if limit-or-not limit-or-start 0))
         (limit (if limit-or-not limit-or-not limit-or-start))
         (lc (list-collector)))
    (if (< start limit)
        (progn (when (not (plusp step))
                 (error "range: step must be positive to terminate, but is {}"
                        stop))
               (while (< start limit)
                 (funcall lc start)
                 (setq start (+ start step))))
      (when (not (minusp step))
        (error "range: step must be negative to terminate, but is {}"
               step))
      (while (> start limit)
        (funcall lc start)
        (setq start (+ start step))))
    (funcall lc)))

(defun seq (start end . options)
  "Return a list of numbers from START to END.
Optional STEP (default 1 or -1) specifies the step-width."
  (declare (number start end))
  (let* (((step) options)
         (step (or step (if (< start end) 1 -1))))
    (range start (+ end (signum step)) step)))

(defun lcm args
  "Return the least common multiple of all arguments."
  (flet ((f-merge
          (f1 f2)
          (cond ((null f1) f2)
                ((null f2) f1)
                (t (let ((c1 (car f1))
                         (c2 (car f2)))
                     (cond ((< c1 c2) (cons c1 (f-merge (cdr f1) f2)))
                           ((< c2 c1) (cons c2 (f-merge f1 (cdr f2))))
                           (t (cons c1 (f-merge (cdr f1) (cdr f2))))))))))
    (let ((flists (mapcar #'factor args))
          merged)
      (while flists
        (setq merged (f-merge merged (pop flists))))
      (apply #'* merged))))

(defun gcd (n1 n2)
  "Return the greatest common divisor of the arguments."""
  (let ((gcd 1)
        (factors1 (factor n1))
        (factors2 (factor n2)))
    (while (and factors1 factors2)
      (let ((f1 (car factors1))
            (f2 (car factors2)))
        (if (= f1 f2)
            (progn (setq gcd (* gcd f1))
                   (pop factors1)
                   (pop factors2))
          (if (< f1 f2)
              (pop factors1)
            (pop factors2)))))
    gcd))

(defun function-not-implemented args
  "This function is not implemented and will throw an error."
  (error "function is not implemented"))

(let ((syms '(conjugate cis rational rationalize)))
  (while syms
    (fset (pop syms) #'function-not-implemented)))

(defun numerator (number)
  "Return the numerator of NUMBER.
As only real numbers are supported, the numerator of a number is
always the number itself."
  (declare (number number))
  number)

(defun denominator (number)
  "Return the denominator of NUMBER.
As only real numbers are supported, the denominator of a number is
always 1."
  (declare (number number))
  1)

(defun realpart (number)
  "Return the real part of NUMBER.
As only real numbers are supported, the real part of a number is
always the number itself."
  (declare (number number))
  number)

(defun imagpart (number)
  "Return the imaginary part of NUMBER.
As only real numbers are supported, the imaginary part of a
number is always zero."
  (declare (number number))
  0)

(defun phase (number)
  "Return the angle part of NUMBER's polar representation.
As only real numbers are supported, the return value is only -Pi for
negative numbers, Pi for positive numbers, and zero for zero."
  (declare (number number))
  (cond ((minusp number) (- (pi)))
        ((plusp number) (pi))
        (t 0)))

(defun pi ()
  "Return the value of the constant Pi."
  3.141592653589793)

(defun e ()
  "Return the value of the Euler constant."
  2.718281828459045)

(defun exp (power)
  "Return e raised to the power of POWER."
  (expt 2.718281828459045 power))

