;; I/O functions

(defun print (arg . options)
  "Print ARG to optional PORT (default *stdout*) without quoting,
framed by a newline and a blank."
  (let* (((port) options)
         (port (or port *stdout*))
         (s (string "\n" (prin1-to-string arg) " ")))
    (princ s port)
    arg))

(defun println args
  "Print ARGS to stdout without quoting, plus a newline."
  (princ (string (join " " args) "\n")))

(defun terpri args
  "Terminate a print line by printing a newline character
to optional port (default *stdout*)."
  (let* (((port) args)
         (port (or port *stdout*)))
    (princ "\n" port)
    nil))
  
(defun pprint args
  "Print args as in Python: separated with blanks, terminated by newline."
  (println (join " " args)))

(defmacro pvars (tag . symbols)
  (let ((syms symbols)
        (formatstring (string tag ": ")))
    (while syms
      (let ((sym (pop syms)))
        (setq formatstring (format nil "{}{} = {{:r}}, " formatstring sym))))
    `(format t ,formatstring ,@symbols)))
    
