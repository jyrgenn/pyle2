;;; core macros with no more dependencies than the core functions

(defmacro progn bodyforms
  "Eval BODYFORMS and return the value of the last."
  `(let () ,@bodyforms))

(defmacro prog1 (form1 . bodyforms)
  "Eval all forms and return the value of the first."
  `(let ((value ,form1))
     ,@bodyforms
     value))

(defmacro prog2 (form1 form2 . bodyforms)
  "Eval all forms and return the value of the second."
  `(let ((value ,form1)
         (value ,form2))
     ,@bodyforms
     value))

(defmacro when (condition . if-clauses)
  "If CONDITION yields true, eval IF-CLAUSES and return the last value."
  `(cond (,condition ,@if-clauses)))

(defmacro unless (condition . else-clauses)
  "Unless CONDITION yields true, eval ELSE-CLAUSES and return the last value."
  `(cond (,condition nil)
         (t ,@else-clauses)))  

(defmacro loop bodyforms
  `(while t . ,bodyforms))

(defmacro break args
  "Break out of a while loop."
  (let ((arg (car args)))
    `(throw '*break-while* ,arg)))

(defmacro setq (symbol value)
  "Set SYMBOL to VALUE."
  `(set ',symbol ,value))

(defmacro if (condition if-clause . else-clauses)
  "If CONDITION is true, evaluate IF-CLAUSE, otherwise the ELSE-CLAUSES."
  `(cond (,condition ,if-clause)
         (t ,@else-clauses)))

(defmacro defun (name params . bodyforms)
  "Define function NAME (a symbol) with parameters PARAMS and BODYFORMS."
  `(progn (trace-if 'trace-defs "defun {}" ',name)
          (fset ',name
                (lambda ,params ,@bodyforms) t)
          ',name))

(defmacro push (item variable)
  "Prepend ITEM to the list in VARIABLE and store the result in VARIABLE.
Return the new value of VARIABLE."
  `(setq ,variable (cons ,item ,variable)))

(defmacro pop (variable)
  "Return the car of the value of VARIABLE;
store the cdr of the value into VARIABLE."
  ;; cannot use with-gensyms here due to circular dependency with pop
  (let ((valsym (gensym))
        (tailsym (gensym)))
    `(progn (declare (list ,variable))
            (let (((,valsym . ,tailsym) ,variable))
              (setq ,variable ,tailsym)
              ,valsym))))

