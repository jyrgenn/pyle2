;; generic sequence ops


(defun elt (seq n)
  "Return element N (zero-based) from sequence SEQ.
It is an error if N is greater than the length of the sequence."
  (declare (sequence seq) (number n))
  (when (< n 0)
    (error "elt: negative index {}" n))
  (if (listp seq)
      (progn
        (dotimes (_ n)
          (pop seq))
        (unless seq
          (error "elt: index {} out of bounds for list" n))
        (car seq))
    (if (vectorp seq)
        (vector-get seq n)
      (error "elt: encountered unknown seq type {}" (type-of seq)))))

(defun setelt (seq n val)
  "Set element N (zero-based) of sequence SEQ to VAL and return VAL.
It is an error if N is greater than the length of the sequence."
  (declare (sequence seq) (number n))
  (if (vectorp seq)
      (vector-set seq n val)
    (if (listp seq)
        (let ((pair (nthcdr n seq)))
          (if (null pair)
              (error "setelt: index {} out of bounds for list" n)
            (rplaca pair val)
            val))
      (error "setelt: encountered unknown seq type {}" (type-of seq)))))

(defun copy-seq (seq)
  "Return a copy of SEQUENCE.
The elements of the new sequence are the same as the corresponding
elements of the given sequence."
  (declare (sequence seq))
  (if (vectorp seq)
      (apply #'vector (vector-elems seq))
    (if (listp seq)
        (let ((lc (list-collector)))
          (while seq
            (if (consp seq)
                (funcall lc (pop seq))
              (error "copy-seq: argument not sequence: {}" seq)))
          (funcall lc))
      (error "copy-seq: encountered unknown seq type {}" (type-of seq)))))

(defun sequence-elements (seq)
  "Return the elements of sequence SEQ as a list."
  (declare (sequence seq))
  (if (vectorp seq)
      (vector-elems seq)
    (if (listp seq)
        seq
      (error "sequence-elements: unknown type of sequence: {}" seq))))

(defmacro doseq (control . bodyforms)
  "With CONTROL as (var seqform &optional resultform):
Iterate over SEQFORM, binding symbol VAR to each element in turn.
Return the value of RESULTFORM (if specified) or nil."
  (with-gensyms (var elems seqform resultform)
    (let (((var seqform resultform) control))
      `(let ((,elems (sequence-elements ,seqform)))
         (while ,elems
           (let ((,var (pop ,elems)))
             ,@bodyforms))
         ,resultform))))

;; (defun subseq (seq from to)
;;   "Return a subsequence of SEQ from index FROM (included) to TO (not included).
;; Both indexes need not be in the bounds of SEQ.
;; Negative indexes count from the end, -1 indicating the last element."
;;   (declare (sequence seq) (number from to))
;;   (let* ((len (length seq))
;;          (start ())
;;          (end (min len to))
;;          (lc (list-collector)))
;;     (if (vectorp seq)
;;         (progn (for (i start end)
;;                  (funcall lc (svref seq i)))
;;                (apply #'vector (funcall lc)))
;;       (if (listp seq)
;;           (progn (setq seq (nthcdr start seq))
;;                  (for (i start end)
;;                    (funcall lc (pop seq)))
;;                  (funcall lc))
;;         (error "sequence-elements: unknown type of sequence: {}" seq)))))
  
(defun subseq (seq from . options)
  "Return a subsequence of SEQ from index FROM to optional TO (not included).
Both indexes need not be in the bounds of SEQ."
  (declare (sequence seq) (number from))
  (let ((to (or (car options) (length seq))))
    (declare (number to))
    (cond ((vectorp seq)
           (apply #'vector (subseq (vector-elems seq) from to)))
          ((listp seq)
           (let ((lc (list-collector))
                 (counter 0))
             (while (and seq (< counter from))
               (pop seq)
               (incf counter))
             (while (and seq (< counter to))
               (funcall lc (pop seq))
               (incf counter))
             (funcall lc)))
          (t (error "sequence-elements: unknown type of sequence: {}" seq)))))
  
(defun find (item seq . options)
  "Find ITEM in sequence SEQ and return it, or nil it it wasn't found.
Options: TEST binary function to compare ITEM with an element (default equal);
         FROM-END to return the leftmost found element;
         KEY unary function to extract the value before test."
  (declare (sequence seq))
  (let* (((from-end test key) options)
         (test (or test #'equal))
         (key (or key #'identity)))
    (cond ((vectorp seq)
           (let (var from to step looptest)
             (if from-end
                 (progn (setf from (1- (length seq)))
                        (setf to 0)
                        (setf step -1)
                        (setf looptest #'>=))
               (progn (setf from 0 )
                      (setf to (length seq))
                      (setf step 1)
                      (setf looptest #'<)))
             (for (i from to step looptest)
               (let ((elem (vector-get seq i)))
                 (when (funcall test item (funcall key elem))
                   (return elem))))
             nil))
          ((listp seq)
           (when from-end
             (setf seq (reverse seq)))
           (dolist (elem seq)
             (when (funcall test item (funcall key elem))
               (return elem))))
          (t (error "find: unknown type of sequence: {}" seq)))))

(defun find-if (pred seq . options)
  "Find element in sequence SEQ for which PRED is true and return it,
or nil it it wasn't found.
Options: FROM-END to return the leftmost found element;
         KEY unary function to extract the value before test."
  (declare (sequence seq))
  (let* (((from-end key) options)
         (key (or key #'identity)))
    (cond ((vectorp seq)
           (let (var from to step looptest)
             (if from-end
                 (progn (setf from (1- (length seq)))
                        (setf to 0)
                        (setf step -1)
                        (setf looptest #'>=))
               (progn (setf from 0 )
                      (setf to (length seq))
                      (setf step 1)
                      (setf looptest #'<)))
             (for (i from to step looptest)
               (let ((elem (vector-get seq i)))
                 (when (funcall pred (funcall key elem))
                   (return elem))))
             nil))
          ((listp seq)
           (when from-end
             (setf seq (reverse seq)))
           (dolist (elem seq)
             (when (funcall pred (funcall key elem))
               (return elem))))
          (t (error "find-if: unknown type of sequence: {}" seq)))))

(defun filter (pred seq)
  "Use predicate PRED to filter sequence SEQ."
  (declare (callable pred) (sequence seq))
  (cond ((listp seq)
         (let ((lc (list-collector)))
           (dolist (el seq)
             (when (funcall pred el)
               (funcall lc el)))
           (funcall lc)))
        ((vectorp seq)
         (apply #'vector (filter pred (vector-elems seq))))
        (t (error "filter: unknown type of sequence: {}" seq))))

(defun elements (seq)
  "Return the elemens of the sequence as a list.
Beware: If the sequence is a list, the original list is returned."
  (declare (sequence seq))
  (cond ((listp seq) seq)
        ((vectorp seq) (vector-elems seq))
        (t (error "elements: unknown type of sequence: {}" seq))))

(defun sequence-combine-function (seq)
  (declare (sequence seq))
  (cond ((listp seq) #'list)
        ((vectorp seq) #'vector)
        (t (error "sort: unknown type of sequence: {}" seq))))

(defun sort (seq pred)
  "Sort sequence SEQ with predicate PRED and return the result."
  (declare (sequence seq) (function pred))
  (let ((qsort (lambda (l)
                 (if (< (length l) 2)
                     l
                   (let (((pivot . rest)  l)
                         l1 l2)
                     (dolist (el rest)
                       (if (funcall pred el pivot)
                           (push el l1)
                         (push el l2)))
                     (nconc (funcall qsort l1)
                            (list pivot)
                            (funcall qsort l2)))))))
      (apply (sequence-combine-function seq) (qsort (elements seq)))))


(defun delete (item seq . options)
  "Return a sequence from which the occurences of ITEM have been removed.
Options: FROM-END: if non-nil, start at the end of the seqence
         TEST:     binary function to test sequence elements against ITEM
         COUNT:    if present, remove at most COUNT elements
         KEY:      uniary function to extract a comparison key from an element."
  (declare (sequence seq))
  (let* (((from-end test count key) options)
         (test (or test #'equal))
         (key (or key #'identity)))
    (delete-if (lambda (el) (funcall test item el)) seq from-end count key)))
              
(defun delete-if (test seq . options)
  "Return a sequence from which the elements for which TEST is true are removed.
Options: FROM-END: if non-nil, start at the end of the seqence
         COUNT:    if present, remove at most COUNT elements
         KEY:      uniary function to extract a comparison key from an element."
  (declare (sequence seq))
  (let* (((from-end count key) options)
         (key (or key #'identity))
         (lc (list-collector))
         (elems (elements seq))
         elem)
    (when from-end
      (setf elems (reverse elems)))
    (dolist (el elems)
      (if (funcall test (funcall key el))
          (cond ((null count))
                ((> count 0) (decf count))
                (t (funcall lc el)))
        (funcall lc el)))
    (apply (sequence-combine-function seq) (if from-end
                                               (reverse (funcall lc))
                                             (funcall lc)))))
              
 
(defun concatenate (result-type . sequences)
  "Return a sequence of type RESULT-TYPE with the elements of all sequences
in the order in which they are supplied."
  (declare (symbol result-type))
  (let ((lc (list-collector)))
    (dolist (seq sequences)
      (doseq (elem seq)
        (lc elem)))
    (cond ((eq result-type 'list) (funcall lc))
          ((eq result-type 'vector) (apply #'vector (lc)))
          (t (error "concatenate: unknown type argument `{}'" result-type)))))

