;; more list functions

(defun make-list (n el)
  "Return a list of length N with elements EL (which may be a function)."
  (let ((lc (list-collector)))
    (if (functionp el)
        (while (< 0 n)
          (setq n (1- n))
          (funcall lc (funcall el)))
      (while (< 0 n)
        (setq n (1- n))
        (funcall lc el)))
    (funcall lc)))

(defun append lists
  "Return a new list that is the concatenation of LISTS.
The list structure of all but the last list is copied."
  (if (null lists)
      nil
    (let (((list1 . others) lists))
      (if list1
          (let (((head1 . tail1) list1))
            (cons head1 (apply #'append (cons tail1 . others))))
        (apply #'append others)))))

(defun member (item list . rest)
  "Find first ITEM in LIST and return the tail of the list beginning with item.
Options: KEY to provide the key for comparison
         TEST to compare ITEM with the elements; defaults to equal."
  (declare (list list))
  (let* (((key test) rest)
         (key (or key #'identity))
         (test (or test #'equal)))
    (while list
      (if (funcall test item (key (car list)))
          (return list)
        (pop list)))))

(defun member-if (pred list . rest)
  "Find first element in LIST that satisfies predicate PRED and return
the tail of the list beginning with item.
Options: KEY to provide the key for comparison."
  (declare (function pred) (list list))
  (let* (((key) rest)
         (key (or key #'identity)))
    (while list
      (if (funcall pred (key (car list)))
          (return list)
        (pop list)))))

(defun map (f l)
  "Apply function F to all elements in L and return the resulting list."
  (declare (list l))
  (cond ((null l) nil)
        (t (cons (funcall f (car l))
                 (map f (cdr l))))))

(defun reverse (l)
  "Reverse the order of the list L and return the resulting list.
L may also be a vector, symbol, or string; it may be anything else,
for that matter, but the result may make not much sense."
  (cond ((listp l)
         (let (result)
           (while l
             (push (pop l) result))
           result))
        ((vectorp l)
         (apply #'vector (reverse (vector-elems l))))
        ((symbolp l)
         (intern (string-reverse l)))
        (t (string-reverse l))))

(defun lastpair (l)
  "Return the last pair of list L."
  (declare (list l))
  (while (cdr l)
    (pop l))
  l)

(defun last (l)
  "Return the last element of the list L."
  (car (lastpair l)))
    

(defun nconc lists
  "Append the lists and return the result. The argument lists may be modified."
  (if (null lists)
      nil
    (let (((list1 . rest) lists))
      (if (null list1)
          (apply #'nconc rest)
        (let ((lastp (lastpair list1)))
          (while rest
            (rplacd lastp (pop rest))
            (setq lastp (lastpair lastp)))
          list1)))))


(defun nthcdr (n l)
  "Returns the tail of list obtained by calling cdr N times in succession.
If N is greater than the length of the list, return nil."
  (declare (number n) (list l))
  (when (< n 0)
    (error "nth/nthcdr: index must be at least zero"))
  (while (> n 0)
    (setq n (1- n))
    (pop l))
  l)

(defun nth (n l)
  "Return the nth element of the list L (zero-based).
If N is greater than the length of the list, return nil."
  (car (nthcdr n l)))

(defun nreverse (l)
  "Reverse a list by modifying the pairs and return the list."
  (let (res)
    (while l
      (let ((p l))
        (setq l (cdr l))
        (rplacd p res)
        (setq res p)))
    res))

