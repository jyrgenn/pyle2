# load core definitions in the target language

import os
import sys
import jpylib as y

y.debug("loading", __name__)

import pglobal
import objects
import pio
import builtin
import utils

load_files = (
    "base-macros",
    "typep",
    "base-funcs",
    "ca-dr",
    "io",
    "numbers",
    "assoc",
    "lists",
    "more-macros",
    "setf",
    "sequences",
    "string",
    "more-funcs",
    "aliases",
    "weird-stuff",
)

def print_loading(*args, **kwargs):
    """Print loading indicator if explicitly instructed or we would, anyway."""
    if (pglobal.interactive and y.is_notice()) or pglobal.print_loading:
        print(*args, **kwargs, flush=True)

print_loading("loading", end="")
for fname in load_files:
    try:
        print_loading(".", end="")
        path = os.path.join(pglobal.basedir, "lcore", fname + ".l")
        pio.load_file(path, silent=True)
    except objects.PyleException as e:
        y.err(type(e).__name__, e, "while loading", path)
        if pglobal.raise_exceptions:
            raise e
        sys.exit(12)
print_loading()

