;;; string functions

(defun string-reverse (s)
  "Reverse the string S and return the result.
S will be converted to a string if it is not one."
  (join "" (nreverse (split-string (string s) t))))
