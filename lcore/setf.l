;;; the setf stuff... will I get it to work?

(defmacro incf (place . rest)
  "Increment PLACE by optional DELTA (or 1) and return the new value."
  (let ((delta (or (car rest) 1)))
    `(setf ,place (+ ,place ,delta))))

(defmacro decf (place . rest)
  "Decrement place by optional DELTA (or 1) and return the new value."
  (let ((delta (car rest)))
    (if delta
        `(setf ,place (- ,place ,delta))
      `(setf ,place (1- ,place)))))


;; ;; first get rid of previous definition of push, then replace it with
;; ;; the more flexible setf-based one
;; ;(fmakunbound 'push)
;; (defmacro push (element place)
;;   "Prepend ITEM to the list stored in PLACE and store the result in PLACE.
;; Return the new value of PLACE."
;;   (with-gensyms (elsym newsym)
;;     `(let* ((,elsym ,element)
;;             (,newsym (cons ,elsym ,place)))
;;        (setf ,place ,newsym))))
       
;; ;; first get rid of previous definition of pop, then replace it with
;; ;; the more flexible setf-based one
;; ;(fmakunbound 'pop)
;; (defmacro pop (place)
;;   "Return the car of the value of PLACE; store the cdr of the value into PLACE."
;;   (with-gensyms (placesym)
;;     `(let* ((,placesym ,place))
;;        (setf ,place (cdr ,placesym))
;;        (car ,placesym))))

(defmacro rplaca-ret-value (pair new-car)
  `(car (rplaca ,pair ,new-car)))

(defmacro rplacd-ret-value (pair new-cdr)
  `(cdr (rplacd ,pair ,new-cdr)))

(defmacro propput (sym prop value)
  `(put ,sym ,value, prop))

(defvar *setf-update-table* #:())

(defmacro update-setf-table (access update)
  `(table-put *setf-update-table* ,access ,update))

(defmacro defsetf (access-fn update-fn-or-lambda-list . forms)
  "Define an update-function for an access-function, to be used by setf.
Short form:
    (defsetf access-fn update-fn)
where both ACCESS-FN and UPDATE-FN are function symbols.

Long form:
    (defsetf access-fn (lambda-list) forms ...)
defines an update function with the parameters specified in the
LAMBDA-LIST, and the forms executed for update in a body where
these parameters are bound accordingly.

In both cases the update function is called with the arguments of the access
function plus the value argument."
  `(progn
     ,(if (symbolp update-fn-or-lambda-list)
          (if forms
              (error "defsetf: two-symbol form called with forms argument(s)")
            `(update-setf-table ',access-fn ',update-fn-or-lambda-list))
        `(update-setf-table ',access-fn
                            (lambda ,update-fn-or-lambda-list ,@forms)))
      ',access-fn))

(defsetf car rplaca-ret-value)
(defsetf cdr rplacd-ret-value)
(defsetf caar (l value) (rplaca-ret-value (car l) value))
(defsetf cadr (l value) (rplaca-ret-value (cdr l) value))
(defsetf cdar (l value) (rplacd-ret-value (car l) value))
(defsetf cddr (l value) (rplacd-ret-value (cdr l) value))
(defsetf caaar (l value) (rplaca-ret-value (caar l) value))
(defsetf caadr (l value) (rplaca-ret-value (cadr l) value))
(defsetf cadar (l value) (rplaca-ret-value (cdar l) value))
(defsetf caddr (l value) (rplaca-ret-value (cddr l) value))
(defsetf cdaar (l value) (rplacd-ret-value (caar l) value))
(defsetf cdadr (l value) (rplacd-ret-value (cadr l) value))
(defsetf cddar (l value) (rplacd-ret-value (cdar l) value))
(defsetf cdddr (l value) (rplacd-ret-value (cddr l) value))
(defsetf caaaar (l value) (rplaca-ret-value (caaar l) value))
(defsetf caaadr (l value) (rplaca-ret-value (caadr l) value))
(defsetf caadar (l value) (rplaca-ret-value (cadar l) value))
(defsetf caaddr (l value) (rplaca-ret-value (caddr l) value))
(defsetf cadaar (l value) (rplaca-ret-value (cdaar l) value))
(defsetf cadadr (l value) (rplaca-ret-value (cdadr l) value))
(defsetf caddar (l value) (rplaca-ret-value (cddar l) value))
(defsetf cadddr (l value) (rplaca-ret-value (cdddr l) value))
(defsetf cdaaar (l value) (rplacd-ret-value (caaar l) value))
(defsetf cdaadr (l value) (rplacd-ret-value (caadr l) value))
(defsetf cdadar (l value) (rplacd-ret-value (cadar l) value))
(defsetf cdaddr (l value) (rplacd-ret-value (caddr l) value))
(defsetf cddaar (l value) (rplacd-ret-value (cdaar l) value))
(defsetf cddadr (l value) (rplacd-ret-value (cdadr l) value))
(defsetf cdddar (l value) (rplacd-ret-value (cddar l) value))
(defsetf cddddr (l value) (rplacd-ret-value (cdddr l) value))
(defsetf elt setelt)
(defsetf table-get table-put)
(defsetf vector-get vector-set)
(defsetf aref vector-set)
(defsetf symbol-function fset)
(defsetf symbol-value set)
(defsetf get propput)
(defsetf nth (n l value) (rplaca-ret-value (nthcdr n l) value))

(defmacro setf (place value)
  "Set field at PLACE to VALUE."
  (trace-if 'trace-setf "(setf {} {})" place value)
  (cond ((symbolp place)
         `(setq ,place ,value))
        ((consp place)
         (let* ((access-fn (car place))
                (args (cdr place))
                (update-fn (table-get *setf-update-table* access-fn)))
           (if update-fn
               `(,update-fn ,@args ,value)
             (error "no setf expansion found for place {}" place))))
        (t (error "no setf expansion for place {}" place))))

