;;; macros depending on some functions, perhaps

(defmacro with-gensyms (syms . body)
  "Run the BODY with the symbols in SYMS (a list) bound to gensyms.
This is meant to simplify macro definitions that would otherwise
use a
  (let ((param1 (gensym))
        (param2 (gensym)))
        ... )
    ,@body)
symbol definition chain explicitly."
  (let (decls)
    (while syms
      (let ((sym (pop syms)))
        (push (list sym '(gensym)) decls)))
    `(let ,decls
       ,@body)))

(defmacro dolist (control . bodyforms)
  "With CONTROL as (var listform &optional resultform):
Iterate over LISTFORM, binding symbol VAR to each element in turn.
Return the value of RESULTFORM (if specified) or nil."
  (with-gensyms (var list listform resultform)
    (let (((var listform resultform) control))
      `(let ((,list ,listform))
         (while ,list
           (let ((,var (pop ,list)))
             ,@bodyforms))
         ,resultform))))
    
(defmacro dotimes (control . bodyforms)
  "With CONTROL as (var countform resultform):
dotimes evaluates COUNTFORM, which should produce an integer. If
COUNTFORM is zero or negative, the body is not executed. dotimes then
executes the body once for each integer from 0 up to but not including
the value of COUNTFORM, in the order in which the statements occur,
with VAR bound to each integer. Then RESULTFORM is evaluated. At the
time RESULTFORM is processed, VAR is bound to the number of times the
body was executed. [CLHS]"
  (with-gensyms (var countform resultform end)
    (let (((var countform resultform) control))
      `(let ((,var 0)
             (,end ,countform))
         (while (< ,var ,end)
           ,@bodyforms
           (incf ,var))
         ,resultform))))


(defmacro for (control . bodyforms)
  "With control as (var from to step test):
The for loop uses VAR as the counter variable, starting with FROM,
adding STEP to VAR after each run, ending when (TEST VAR TO) no longer
is true. The default STEP is 1; the default TEST is #'<. The arguments
FROM, TO, STEP, and TEST are evaluated just once."
  (with-gensyms (var from to step test stepval testfunc end)
    (let (((var from to step test) control))
      `(let ((,stepval (or (eval ,step) 1))
             (,testfunc (or (eval ,test) #'<))
             (,var (eval ,from))
             (,end (eval ,to)))
         (while (funcall ,testfunc ,var ,end)
           ,@bodyforms
           (incf ,var ,stepval))))))


(defmacro assert (test-form . message-args)
  "Eval TEST-FORM; if non-nil raise an error.
The error message contains TEST-FORM; if MESSAGE-ARGS (a format string
and arguments as appropriate) are present, append the formatted
message to the error message."
  (let ((messageform (when message-args
                       (let (((format-string . args) message-args))
                         `(format nil ,format-string ,@args)))))
    `(unless (eval ,test-form)
       (error (string "assertion failed: `" ',test-form "'"
                      (if ',messageform
                          (string "; " (eval ,messageform))
                        ""))))))

(defmacro with-open-file (file-declaration . bodyforms)
  "(with-open-file (stream-symbol pathname . options) BODY...)
Evaluate BODYFORMS with STREAM-SYMBOL bound to a port open to PATHNAME.
Options: [ direction [ if-exists [ if-not-exists ]]] as with `open'."  
  (let (((stream-symbol pathname . options) file-declaration)
        (bodyform (if (= (length bodyforms) 1)
                      (car bodyforms)
                    (cons 'progn bodyforms))))
    `(let ((,stream-symbol (open ,pathname ,@options)))
       (unwind-protect
           ,bodyform
         (close ,stream-symbol)))))

(defmacro with-output-to-string (stream-declaration . bodyforms)
  "(with-output-to-string (string-var) BODY...)
Evaluate BODYFORMS with STRING-VAR bound to a string output port.
Return the string."
  (let (((stream) stream-declaration)
        (bodyform (if (= (length bodyforms) 1)
                      (car bodyforms)
                    (cons 'progn bodyforms))))
    `(let ((,stream (make-string-output-stream)))
       (unwind-protect
           ,bodyform
         (close ,stream))
       (get-output-stream-string ,stream))))

(defmacro with-input-from-string (stream-declaration . bodyforms)
  "(with-output-to-string (string-var contents) BODY...)
Evaluate BODYFORMS with STRING-VAR bound to a string input port with
the contents of string. Return the value of the last bodyform."
  (let (((stream contents) stream-declaration)
        (bodyform (if (= (length bodyforms) 1)
                      (car bodyforms)
                    (cons 'progn bodyforms))))
    `(let ((,stream (make-string-input-stream ,contents)))
       (unwind-protect
           ,bodyform
         (close ,stream)))))

  
