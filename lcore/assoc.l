;;; assoc and friends -- would love to have that in lists.l, but must
;;; come before setf.l to get around a cyclic dependency.

(defun assoc (item alist)
  "Look up ITEM in ALIST and return the pair whose car is equal to ITEM.
Return nil if ITEM is not found as a car in one of the pairs in ALIST."
  (assoc-if (lambda (elem) (equal elem item)) alist))

(defun assq (item alist)
  "Look up ITEM in ALIST and return the pair whose car is eq to ITEM.
Return nil if ITEM is not found as a car in one of the pairs in ALIST."
  (assoc-if (lambda (elem) (eq elem item)) alist))

(defun assoc-if (predicate alist)
  "Return the pair from ALIST for which PREDICATE is true for its car."
  (while alist
    (let ((head (pop alist)))
      (when (funcall predicate (car head))
        (return head)))))



