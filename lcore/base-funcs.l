;;; core functions without dependencies of macros

(defun null (ob)
  "Return t if the argument is an empty list; nil otherwise."
  (eq ob nil))

(defun list args
  "Return the arguments as a list."
  args)

(defun symbol-name (symbol)
  "Return the name of SYMBOL as a string."
  (string symbol))

(defun symbol-value (symbol)
  "Return the value of SYMBOL."
  (declare (symbol symbol))
  (eval symbol))

(defun funcall (func . args)
  "Call function FUNC with arguments ARGS."
  (apply func args))

(defun bool (ob)
  "Return t iff ob is true, else nil."
  (cond (ob t)
        (t nil)))

(defun equal (ob1 ob2)
  "Returns true if OB1 and OB2 are structurally similar (isomorphic) objects.
For atoms this is the same as eq. For structured types, this is true
if all their elements are equal."
  (cond ((eq ob1 ob2) t)
        ((and (eq (type-of ob1) 'table)
              (eq (type-of ob2) 'table))
         (equal (table-pairs ob1) (table-pairs ob2)))
        ((and (eq (type-of ob1) 'pair)
              (eq (type-of ob2) 'pair))
         (and (equal (car ob1) (car ob2))
              (equal (cdr ob1) (cdr ob2))))
        ((and (eq (type-of ob1) 'vector)
              (eq (type-of ob2) 'vector))
         (and (equal (vector-elems ob1) (vector-elems ob2))))
        (t nil)))

(defun search (subseq seq)
  "Currently for strings only."
  (search-string subseq seq))

(defun list-collector ()
  "Return a list collector closure."
  (let (the-list lastpair)
    (lambda "*collector*" newargs
      "Add NEWARGS to the list being collected and return the list."
      (while newargs
        (let ((newpair (list (pop newargs))))
          (if the-list
              (rplacd lastpair newpair)
            (setq the-list newpair))
          (setq lastpair newpair)))
      the-list)))
