;;; more functions

(defun doc (func . options)
  "Print the documentation of function FUNC.
With option BRIEF non-nil, print only first line;
with option NOPRINT, do not print, but return string."
  (let (((brief noprint) options)
        (the-doc (strip-string (docstring func))))
    (when the-doc
      (when brief
        (setf the-doc (car (split-string the-doc "\n" 2))))
      (if noprint
          the-doc
        (format t "{}\n" the-doc)))))

(defun max (arg1 . moreargs)
  "Return the largest of the arguments."
  (dolist (item moreargs)
    (when (> item arg1)
      (setf arg1 item)))
  arg1)

(defun min (arg1 . moreargs)
  "Return the smallest of the arguments."
  (dolist (item moreargs)
    (when (< item arg1)
      (setf arg1 item)))
  arg1)

(defun iota-func count-start-and-step
  "(iota-func [count [start [step]]]) => a number seqence generator.
Count is the maximum length of the sequence (unbounded if count is nil);
start defaults to 0, step to 1. Each call returns the next number, or
 nil if the specified length is reached."
  (let (((count start step) count-start-and-step))
    (setf count (or count -1))
    (setf start (or start 0))
    (setf step (or step 1))
    (lambda ()
      "*iota-function*"
      (if (zerop count)
          nil
        (let ((result start))
          (incf start step)
          (decf count)
          result)))))

(defun iota (count . start-and-step) 
  "(iota-func count [start [step]]) => a number seqence.
Count is the length of the sequence; start defaults to 0, step to 1."
  (let ((lc (list-collector))
        (iof (apply #'iota-func (cons count . start-and-step))))
    (dotimes (_ count)
      (lc (iof)))
    (lc)))


(defun make-string (len . optional)
  "Return a string of length LEN, contents taken from optional INITIAL."
  (declare (number len))
  (let* (((initial) optional)
         (initial (or initial " "))
         (s "")
         (index 0))
    (if (functionp initial)
        (dotimes (i len)
          (let ((piece (string (funcall initial))))
            (unless (> (length piece) 0)
              (error "make-string: initializer function {} returns empty string"
                     initial))
            (setf s (string s (substring piece 0 1)))))
      (let ((ilen (length initial)))
        (assert (> ilen 0)
                "make-string: initializer is empty string")
        (dotimes (i len)
          (let* ((pos (% i ilen)))
            (setf s (string s (substring initial pos (1+ pos))))))))
    s))

    
(defun apropos (arg . rest)
  "Print the symbols matching ARG and if they are functions and/or variables.

With optional argument list-only, just return the list of symbols."
  (let* ((pred (if (regexpp arg)
                   (lambda (item) (arg item))
                 (lambda (item) (search arg item))))
         ((list-only) rest)
         (matching (filter pred (symbols)))
         (width (or 20 (apply #'max (map #'length (map #'string matching)))))
         (pad (lambda (item len)
                (let* ((item-s (string item))
                       (ilen (length item-s)))
                  (string item (make-string (- len ilen) " "))))))
      (if list-only
          matching
        (while matching
          (let ((item (pop matching))
                syms)
            (when (boundp item)
              (push 'variable syms))
            (when (fboundp item)
              (push (type-of (symbol-function item)) syms))
            (format t "{} {}" (pad item width) (join ", " syms))
            (terpri))))))

(defun join (separator . args)
  "Append all args into a single string separated by SEPARATOR.
ARGS is a spreadable args list."
  (let ((lc (list-collector))
        (args (apply #'list* args))
        arg)
    (trace-if 'trace-join "spread as {}" args)
    (while args
      (if (consp args)
          (setf arg (pop args))
        (setf arg args)
        (setf args nil))
      (trace-if 'trace-djoin "dealing with {}" arg)
      (lc arg)
      (when args
        (lc separator)))
    (apply #'string (lc))))
            

(defun identity (arg)
  "Return ARG as it is."
  arg)

(defun reduce (func l)
  "Reduce a list L to a single value using FUNC and return the value.
This is done by continually replacing the first two elements of L by the
result of applying FUNC to both, until there is only one element left."
  (if (cdr l)
      (reduce func (cons (func (car l) (cadr l))
                         (cddr l)))
    (car l)))

(defun arg-number-is-one (arg)
  "Iff ARG (or the number of its elements) is 1, return true."
  (let ((n (cond ((numberp arg) arg)
                 ((sequencep arg) (length arg)))))
    (= n 1)))
  
(defun plural-s (arg)
  "Return a plural-s or an empty string as appropriate for ARG.
If ARG is a number, return \"s\" if it is zero or greater than 1, but
an empty string if it is 1.
If ARG is a sequence, do the same for the number of it elements."
  (if (arg-number-is-one arg)
      ""
    "s"))

(defun plural-ies (arg)
  "Return a plural \"ies\" or singular \"y\" as appropriate for ARG.
If ARG is a number, return \"ies\" if it is zero or greater than 1, but
\"y\" if it is 1.
If ARG is a sequence, do the same according to the number of it elements."
  (if (arg-number-is-one arg)
      "y"
    "ies"))

(defvar *features* #:() "Table of all provided features.")
(put '*features* t '*system*)

(defun have-feature (feature)
  "Return non-nil if FEATURE has been provided."
  (if (table-get *features* feature nil)
      feature
    nil))

(defun require (feature)
  "Load FEATURE from file or fail."
  (if (stringp feature)
      (setq feature (intern feature)))
  (unless (have-feature feature)
    (load feature)
    (unless (have-feature feature)
      (error "feature {} is not provided" feature)))
  feature)

(defun provide (feature)
  "Let FEATURE be provided."
  (if (stringp feature)
      (setq feature (intern feature)))
  (table-put *features* feature t))

(defun ignore args
  nil)

(defun make-vector (n el)
  "Return a vector of length N with elements EL (which may be a function)."
  (apply #'vector (make-list n el)))

(defun string-right-trim (char-bag string)
  "Return a substring of STRING, with characters in CHAR-BAG stripped off
the beginning. CHAR-BAG may be t, in which case all whitespace
characters will be stripped, or a sequence of characters."
  (when (eq char-bag t)
    (setf char-bag nil))
  (strip-string string char-bag 'end))

(defun string-left-trim (char-bag string)
  "Return a substring of STRING, with characters in CHAR-BAG stripped off
the end. CHAR-BAG may be t, in which case all whitespace
characters will be stripped, or a sequence of characters."
  (when (eq char-bag t)
    (setf char-bag nil))
  (strip-string string char-bag 'start))

(defun string-trim (char-bag string)
  "Return a substring of STRING, with characters in CHAR-BAG stripped off
the beginning and the end. CHAR-BAG may be t, in which case all whitespace
characters will be stripped, or a sequence of characters."
  (when (eq char-bag t)
    (setf char-bag nil))
  (strip-string string char-bag))


(defun all (arglist)
  """Return true iff all elements of arglist are true."""
  (dolist (el arglist)
    (unless el
      (return nil)))
  t)

(defun mapcar (func . list-args)
  "Successively apply FUNC to the cars of LIST-ARGS, return result list.
i.e. (list (func (car list1) (car list2) ...)
           (func (cadr list1) (cadr list2) ...)
           ...)
Stop when one of the lists is empty and return the list of results.
"
  (let ((lc (list-collector)))
    (while (all list-args)
      (let ((the-cars (map #'car list-args)))
        (setf list-args (map #'cdr list-args))
        (lc (apply func the-cars))))
    (lc)))

(defun div (num1 num2)
  "Return the result of the integer division of NUM1 by NUM2."
  (int (/ num1 num2)))

(defun function-symbols ()
  "Return a list of all function symbols."
  (flet ((has-function (sym)
                       (errset (symbol-function sym) nil)))
    (sort (filter #'has-function (symbols)) #'<)))
