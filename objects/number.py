# Pyle Number class

import jpylib as y

from .object import *


class Number(ComparableInterned):
    """A number object."""
    __slots__ = tuple()

    def __init__(self, num):
        """Argument should really be a number."""
        if isinstance(num, (int, float)):
            inum = int(num)
            if inum == num:
                num = inum
        else:
            num = y.maybe_num(num)
        assert num is not None, "num is not a number: {}".format(
            repr(num))
        self._data = num

    def isNumber(self):
        return True

    def __str__(self, seen=False):
        return self.__repr__()

    def __repr__(self, seen=False):
        """For now; repr of unprintable chars will follow."""
        return str(self._data)

    def __format__(self, format_spec):
        if format_spec == "r":
            return repr(self)
        return self._data.__format__(format_spec)

    def value(self):
        return self._data

    def mult(self, other):
        if not isinstance(other, Number):
            PyleDataTypeError("argument should be a number, but isn't: {}",
                              other)
