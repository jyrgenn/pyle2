# Pyle2 symbols

import jpylib as y

import pglobal
from . import *


# the symbol table
symbolTable = {}

_gensym_counter = 64927


def _sym_needs_quoting(name):
    """True iff name needs to be |quoted| as a symbol.
    """
    if name[0] in ("#", ","):
        return True
    if name in ("."):
        return True
    for ch in name:
        if ch in "|()\"';\\`":          # separators
            return True
        if ch.isspace():
            return True
        if not ch.isprintable():
            return True
    return y.is_num(name)


def _sym_print_name(name):
    """Generate the print name of a symbol.

    This may mean symbol-style |name| quoting.
    """
    result = []
    if _sym_needs_quoting(name):
        result.append("|")
        for ch in name:
            if ch in r"\|":
                result.append("\\")
            result.append(ch)
        result.append("|")
        return "".join(result)
    return name


class Symbol(ComparableInterned):
    __slots__ = "_data", "_immutable", "_props", "_function", "_print_name"

    @staticmethod
    def gensym(prefix):
        global _gensym_counter
        while True:
            _gensym_counter += 1
            name = str(prefix) + "#" + str(_gensym_counter)
            if name not in symbolTable:
                return Symbol(name, register=False)
 

    def __init__(self, name, self_immutable=False, register=True):
        """Return a new, interned symbol without properties."""
        self._data = str(name)
        if name.startswith(":"):
            self_immutable = True
        self._immutable = self_immutable
        self._props = {}
        self._function = None
        if self_immutable:
            pglobal.rootEnvironment.bindValue(self, self)
        if register:
            symbolTable[name] = self
            
    def __len__(self):
        """Return the length of the list -- which it is only if Nil."""
        if self is Nil:
            return 0
        raise PyleInvalidCallError("len() called on non-nil symbol: {}", self)

    def __str__(self):
        return self._data

    def __repr__(self, seen=False):
        if not hasattr(self, "print_name"):
            self._print_name = _sym_print_name(self._data)
        return self._print_name

    def __bool__(self):
        return self is not Nil

    def __iter__(self):
        if self is Nil:
            while False:
                yield self
        else:
            raise PyleDataTypeError("cannot iterate over non-list: {}", self)

    def __format__(self, format_spec):
        if format_spec == "r":
            return self.__repr__()
        return self._data.__format__(format_spec)

    def length(self):
        if self is Nil:
            return 0
        return super().length()

    def isSymbol(self):
        return True

    def isList(self):
        return self is Nil

    def isSequence(self):
        return self.isList()

    def setFunction(self, func):
        self._function = func

    def function(self):
        return self._function

    def name(self):
        return self._data

    def getprop(self, name):
        return self._props.get(name, Nil)

    def putprop(self, name, value):
        self._props[name] = value

    def props(self):
        """Return the property list of the symbol as a list of tuples."""
        return self._props.items()

    def setValue(self, value):
        if self._immutable:
            raise PyleImmutableError("symbol {} is immutable", self)
        pglobal.theEnvironment.setValue(self, value)

    def bindValue(self, value):
        if self._immutable:
            raise PyleImmutableError("symbol {} is immutable", self)
        pglobal.theEnvironment.bindValue(self, value)

    def getValue(self, must_exist=True):
        return pglobal.theEnvironment.getValue(self, must_exist)

    def car(self):
        if self is Nil:
            return Nil
        raise PyleDataTypeError("cannot take car from non-list: {}", self)
        
    def cdr(self):
        if self is Nil:
            return Nil
        raise PyleDataTypeError("cannot take cdr from non-list: {}", self)
        

# init
Nil = Symbol("nil", True, True)

# EOF
