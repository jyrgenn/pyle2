# Pyle2 tables

import re
import collections
import jpylib as y

import pglobal
from . import *
from utils import *

def _stringify_match(seq):
    for elem in seq:
        if elem is None:
            yield String("")
        else:
            assert isinstance(elem, str), "elem is not None or str: "+repr(elem)
            yield String(elem)

class Regexp(Callable):
    __slots__ = "re",

    def __init__(self, pattern, reader=None):
        """Return a new Regex built from pattern."""
        self.re = None
        self._minargs = 1
        self._maxargs = 2
        self._name = "*regexp*"
        try:
            self.re = re.compile(pattern)
        except Exception as e:
            raise PyleSyntaxError(reader, "Regexp " + str(e), pattern)
            
    def __len__(self):
        """Return the length of the pattern string."""
        return len(len(self.re.pattern))

    def __str__(self, seen=False):
        return self.__repr__(seen)

    def __repr__(self, seen=False):
        # Now this is a bit tricky, as backslashes are for some part used to
        # escape regpexp specials chars, as * or +, and for another to escape
        # those that are special chars in strings. Holy bovine, she is dumping!
        if self.re is None:
            return "#<regexp uninitialised>"
        result = ["#/"]
        for ch in self.re.pattern:
            if ch == "/":
                result.append("\\/")
            elif ch == "\\":
                result.append("\\")
            elif ch in pglobal.specialChar2escaped:
                result.append("\\")
                result.append(pglobal.specialChar2escaped[ch])
            else:
                result.append(json.dumps(ch).strip('"'))
        result.append("/")
        return "".join(result)

    def __iter__(self):
        raise PyleDataTypeError("cannot iterate over non-list: {}", self)

    def __format__(self, format_spec):
        return self.__repr__()

    def length(self):
        return self.__len__()

    def isRegexp(self):
        return True

    def docstring(self):
        return """regexp (REGEXP STRING &optional ALL) => matches
If STRING matches REGEXP, return list of match and sub-matches, else
nil. With optional third argument ALL non-nil, a list of match lists for
(potentially) multiple matches is returned.

Regular expression syntax is that of the Python regexp package, which is
largely similar to that of the Perl and Go languages. A "(?flags)"
specification in the regexp can modify the behaviour of the match in the
current group. Possible flags are, among others:

i  case-insensitive (default false)
m  multi-line mode: ^ and $ match begin/end line in addition to begin/end
   text (default false)
s  let . match
   (default false)"""

    def match(self, string):
        match = self.re.search(str(string))
        if match:
            return Pair(String(match.group(0)),
                        Seq2List(_stringify_match(match.groups())))
        return Nil

    def findall(self, string):
        findings = self.re.findall(str(string))
        if findings:
            lc = ListCollector()
            for elem in findings:
                if not isinstance(elem, str) and isinstance(
                        elem, collections.abc.Iterable):
                    lc.append(Seq2List(map(String, elem)))
                else:
                    lc.append(String(elem))
            return lc.list()
        return Nil

    def replace(self, string, newtxt, count):
        return self.re.sub(newtxt, string, count)

    def split(self, string, maxsplit):
        return self.re.split(string, maxsplit)

    def call(self, arglist):
        l = self.checkargs(arglist)
        if l == 1:
            return self.match(arglist.car())
        if l == 2:
            return self.findall(arglist.car())
        raise PyleArgCountError(self, l, too_many=l > 1)
            


# EOF
