# Pyle Pair class a.k.a. as cons cell

from . import *

class Pair(Object):
    """A pair, a cons cell, with car and cdr attributes."""
    __slots__ = "_car", "_cdr"

    def __init__(self, car, cdr):
        # The car and cdr attributes should not be accessed directly.
        self._car = car
        self._cdr = cdr

    def __iter__(self):
        the_list = self
        while the_list.isPair():
            head, the_list = the_list.cxr()
            yield head
        if the_list:                    # not Nil => improper list
            yield the_list

    def isList(self):
        return True

    def isSequence(self):
        return True

    def isPair(self):
        return True

    def car(self):
        return self._car

    def cdr(self):
        return self._cdr

    def cxr(self):
        return self._car, self._cdr

    def __len__(self):
        """Return the number of pairs in the list.

        This will give an improper result for an improper list.
        """
        this = self
        l = 0
        while this.isPair():
            l += 1
            this = this.cdr()
        return l

    def length(self):
        """Return the number of pairs in the list.

        This will give an improper result for an improper list.
        """
        return self.__len__()

    def rplaca(self, newcar):
        self._car = newcar

    def rplacd(self, newcdr):
        self._cdr = newcdr

    def lastpair(self):
        """Return the last pair of a list.

        If the list is not a proper list, return Nil.
        """
        this = self
        while this.isPair():
            the_cdr = this.cdr()
            if the_cdr is Nil:
                return this
            this = the_cdr
        return Nil
        

    def __str__(self, seen=set()):
        return self.__repr__(seen)

    def __repr__(self, seen=set()):
        with utils.printContext(self, seen) as yes:
            if yes: return yes
            result = ["("]
            elem = self
            while elem.isPair():
                if elem is not self and elem in seen:
                    result.append(elem.pid())
                else:
                    result.append(elem.car().__repr__(seen))
                    if elem.cdr():
                        result.append(" ")
                elem = elem.cdr()
            if elem is not Nil:
                result.append(". ")
                result.append(elem.__repr__(seen))
            result.append(")")
            return "".join(result)


# EOF
