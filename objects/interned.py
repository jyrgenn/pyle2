# an Interned metaclass, cargo-culted after
# https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python

import weakref


class Interned(type):
    _instances_per_class = {}                     # class => classdict


    def __call__(cls, value, *args, **kwargs):
        """The value to initialise the Interned object must `be hashable."""
        if cls in Interned._instances_per_class:
            instances = Interned._instances_per_class[cls]
        else:
            instances = weakref.WeakValueDictionary()
            Interned._instances_per_class[cls] = instances
        if value in instances:
            the_thing = instances[value]
        else:
            # be sure we keep a reference to the thing firmly in hand, so it
            # isn't collected in between (which has happened)
            the_thing = super(Interned, cls).__call__(value, *args, **kwargs)
            instances[value] = the_thing
        return the_thing
        
