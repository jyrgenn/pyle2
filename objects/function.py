# functions for Pyle2

import json
import jpylib as y

import pglobal
from .object import *
from .symbol import *
from utils import *

declareSymbol = Symbol("declare")


class Function(Callable):
    """A class of callable functions. These may be bultins, lambdas, and macros.

    """
    __slots__ = "_docstring", "_special",

    def isFunction(self):
        return True

    def isSpecial(self):
        return False

    def name(self):
        return self._name

    def setName(self, name):
        self._name = name

    def body(self):
        raise PyleInvalidCallError("function {} of type {} has no body",
                                   self._name, self.type())
    
    def params(self):
        raise PyleInvalidCallError("function {} of type {} has no params list",
                                   self._name, self.type())
    
    def call(self, *_):
        # This will never happen. No, indeed, it is quite unlikely.
        raise NotImplementedError("generic abstract function cannot be called")

    def __call__(self, *args, **kwargs):
        return self.call(Seq2List(args))

    def docstring(self):
        return self._docstring


class Builtin(Function):
    """Class of builtin functions."""
    __slots__ = "_function",

    def __init__(self, name, function, minargs, maxargs,
                 docstring, special=False):
        """Initialise a Builtin function."""
        self._name = name
        self._function = function
        self._minargs = minargs
        self._maxargs = maxargs
        self._special = special

        # The doc string may require some massaging.
        doclines = []
        for line in docstring.split("\n"):
            doclines.append(line.strip())
        self._docstring = "\n".join(doclines).rstrip()

    def __str__(self, seen=False):
        return self.__repr__()

    def __repr__(self, seen=False):
        if self._special:
            typ = "special form"
        else:
            typ = "builtin function"
        return "#<{} {}>".format(typ, self._docstring.split("\n")[0])

    def isSpecial(self):
        return self._special

    def isBuiltin(self):
        return True

    def call(self, arglist):
        """Call the builtin function with specified args (a List).

        So the builtin function gets passed a Python array of Objects and
        reaturns an Object.

        """
        args = Seq2Array(arglist)
        self.checkargs(args)
        return self._function(*args)


class Form(Function):
    """Factor out commonalities between Lambdas and Macros."""
    __slots__ = "_bodyforms", "_params", "_typecheck"

    def form_init(self, name, params, bodyforms, docstring):
        self._name = name
        self._params = params
        self._docstring = str(docstring)
        self._bodyforms = bodyforms

        min = 0
        max = 0
        while params:
            if params.isSymbol():
                max = None
                break
            min += 1
            max += 1
            params = params.cdr()
        self._minargs = min
        self._maxargs = max

    def _paramss(self):
        if self._params is Nil:
            return "()"
        elif self._params.isSymbol():
            return ". " + self._params.name()
        else:
            return repr(self._params)[1:-1]

    def body(self):
        return self._bodyforms

    def params(self):
        return self._params

    def typename(self):
        return self.__class__.__name__.lower()

    def docstring(self):
        typecheck = getattr(self, "_typecheck", None)
        types = ""
        if typecheck:
            types = "        " + ", ".join(
                [ "{}: {}".format(arg, check[2:].lower())
                  for arg, check in typecheck.items() ]) + "\n"
        return "{} ({} {})\n{}{}".format(
            self.typename(), self._name, self._paramss(), types,
            self._docstring)

    def __str__(self):
        return self.__repr__()
    
    def __repr__(self, seen=set()):
        with utils.printContext(self, seen) as yes:
            if yes: return yes
            name_s = " \"" + str(self._name) + "\"" if self._name else ""
            body_s = []
            for form in self._bodyforms:
                body_s.append(" ")
                body_s.append(form.__repr__(seen))
            return "#<{}{} {}{}>".format(self.typename(), name_s,
                                         self._params or "()", "".join(body_s))


class Lambda(Form):
    """Lambda expression."""
    __slots__ = "_env",

    def __init__(self, params, bodyforms, docstring, name="*anon*"):
        #print(f"Lambda {name}({params}) {repr(docstring)} {bodyforms}")
        # check for declarations and prepare arg checks
        if bodyforms.isPair():
            maybe_declare, other_bodyforms = bodyforms.cxr()
        else:
            maybe_declare = Nil
        self._typecheck = {}

        if maybe_declare.isPair() and maybe_declare.car() is declareSymbol:
            assertions = maybe_declare.cdr()
            bodyforms = other_bodyforms

            for assertion in assertions:
                if not assertion.isList():
                    raise PyleTypeAssertionError(
                        "{}: declare argument is not list: {}", name, assertion)
                typesym, *vars = assertion
                if not typesym.isSymbol():
                        raise PyleTypeAssertionError(
                            "{}: declare type is not symbol: {}", name, typesym)
                for var in vars:
                    if not var.isSymbol():
                        raise PyleTypeAssertionError(
                            "{}: declare variable is not symbol: {}",
                            name, var)
                    if var not in params:
                        raise PyleTypeAssertionError(
                            "{}: declare variable is not in parameters: {}",
                            name, var)
                    trace_if("declare", "assert {} {}", typesym, var)
                    checkname = "is" + str(typesym).title()
                    if not getattr(Object, checkname, None):
                        raise PyleTypeAssertionError(
                            "{}: declare type is unknown: {}", checkname, var)
                    self._typecheck[var] = checkname

        self.form_init(name, params, bodyforms, docstring)
        self._env = pglobal.theEnvironment
        self._special = False

    def isLambda(self):
        return True

    def call(self, arglist):
        self.checkargs(arglist)
        with newEnvironment(self._env):
            try:
                bindValues(self._params, arglist)
                # check argument types where asserted
                for var, checkname in self._typecheck.items():
                    value = var.getValue()
                    if not getattr(value, checkname)():
                        raise PyleTypeAssertionError(
                            "argument {} is declared type {}, "
                            "but value is {:r} ({}) in {}", var,
                            checkname[2:].lower(), value, value.type(),
                            self._name)
                return pglobal.EvalProgn(self._bodyforms)
            except PyleReturn as pr:
                return pr.value()


class Macro(Form):
    """A macro.

    That wasn't quite as tricky as I thought, in particular because I could
    reuse some of the older stuff.

    """
    __slots__ = tuple()
    
    def __init__(self, name, params, bodyforms, docstring):
        self.form_init(name, params, bodyforms, docstring)

    def isMacro(self):
        return True

    def expand(self, arglist):
        trace_if("expand", "({} . {})", self._name, arglist)
        self.checkargs(arglist, True)
        with newEnvironment():
            bindValues(self._params, arglist)
            return pglobal.EvalProgn(self._bodyforms)

    def call(self, arglist):
        raise PyleInvalidCallError("macro used before being defined: {}", self)
        
# EOF
