# Pyle Environment class, nested variable lookup tables

import jpylib as y

import pglobal
from . import *
from .exception import *


class Environment(Object):
    """An environment mapping symbols to values.

    These are chained to reflect nested variable scopes.
    """
    __slots__ = "parent", "level", "data"

    def __init__(self, parent):
        self.parent = parent
        self.level = parent.level + 1 if parent else 0
        self.data = {}

    def __repr__(self, seen=None):
        if self is pglobal.rootEnvironment:
            level = "root"
        else:
            level = self.level
        return "#<{}[{}]{{{}}}>".format(
            self.__class__.__name__, level, len(self.data))

    def __str__(self, seen=None):
        return self.__repr__(seen)

    def __iter__(self):
        for key in self.data.keys():
            yield key

    def isEnvironment(self):
        return True
        
    def update(self, a_dict):
        for k, v in a_dict.items():
            if not k.isSymbol():
                raise PyleDataTypeError("key is not a symbol: {} ({})",
                                        k, k.type())
            self.data[k] = v
        return self

    def getValue(self, sym, must_exist=True):
        """Get a value for sym.

        This is taken from the nearest environment where it can be found, or
        undefined.

        """
        env = self
        while env:
            if sym in env.data:
                return env.data[sym]
            env = env.parent
        if must_exist:
            raise PyleUndefError("unbound variable {}", sym)
        return None

    def bindValue(self, sym, value):
        """Bind a sym to a value in this environment.

        If value is none, make it unbound.
        """
        if value is None:
            if sym in self.data:
                del self.data[sym]
        else:
            assert isinstance(value, Object) ,\
                "bind value for {} not an Object: {}".format(sym, repr(value))
            self.data[sym] = value

    def setValue(self, sym, value):
        """Set the value of sym.

        This is done in the nearest environment where a value is found, or in
        the root environment.

        """
        assert value is None or isinstance(value, Object) ,\
            "set value for {} not an Object: {}".format(sym, repr(value))
        env = self
        while env:
            if sym in env.data:
                y.trace("set {}={} in {}".format(sym, value, env))
                env.bindValue(sym, value)
                return
            env = env.parent
        env = pglobal.rootEnvironment
        if value is not None:           # for makunbound
            y.noticef("setting unbound variable `{}' to {}", sym, repr(value))
        env.bindValue(sym, value)

    def dict(self, local_only=False):
        """Return the environment's mapping as a dict.

        This service is provided for your amusement only. Changes in the dict
        will not be reflected in the environment.

        """
        # Have a list of all envs that we then can go through in reverse.
        all_envs = []
        env = self
        while env:
            all_envs.append(env)
            if local_only:      # only from this environment, if so requested
                break
            env = env.parent

        # Collect the variables from all environments; each
        # environment overrides its parents, so we need to go
        # through in reverse order, from root to current.
        result = {}
        for env in reversed(all_envs):
            result.update(env.data)

        # Remove the keywords and other immutable variables; they
        # are never interesting.
        immutables = []
        for sym in result.keys():
            if sym._immutable:
                immutables.append(sym)
        for sym in immutables:
            del result[sym]

        return result


# Initialise the environment module
pglobal.rootEnvironment = Environment(None)
pglobal.theEnvironment = pglobal.rootEnvironment
