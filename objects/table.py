# Pyle2 tables

import jpylib as y

from . import *
from utils import *

class Table(Callable):
    __slots__ = "data",

    def __init__(self, kvpairs):
        """Return a new table with initial key/value pairs (a list)."""
        self.data = {}
        self._minargs = 1
        self._maxargs = 2
        self._name = "*table*"
        for elem in kvpairs:
            if elem.isPair():
                key, value = elem.cxr()
            else:
                key = elem
                value = Nil
            self.data[key] = value
            
    def __len__(self):
        """Return the size of the table."""
        return len(self.data)

    def __str__(self, seen=set()):
        return self.__repr__(seen)


    def _sortedpairs(self):
        return sorted(list(self.data.items()), key=str)

    def __repr__(self, seen=set()):
        with utils.printContext(self, seen) as yes:
            if yes: return yes
            result = ["#:("]
            for k, v in self._sortedpairs():
                result.append("({} . {})".format(repr(k), v.__repr__(seen)))
            result.append(")")
            return "".join(result)

    def __bool__(self):
        return True

    def __iter__(self):
        raise PyleDataTypeError("cannot iterate over non-list: {}", self)

    def update(self, a_dict):
        for k, v in a_dict.items():
            self.data[k] = v
        return self

    def length(self):
        return self.__len__()

    def isTable(self):
        return True

    def docstring(self):
        return """table (TABLE KEY &optional VALUE) => value
Return the value stored in the TABLE for KEY.
With optional VALUE, set the value."""

    def exists(self, key):
        return key in self.data

    def put(self, key, value):
        self.data[key] = value
        return value

    def get(self, key, default=None):
        if default is None:
            return self.data[key]
        return self.data.get(key, default)

    def delete(self, key):
        value = self.data.get(key, Nil)
        if key in self.data:
            del self.data[key]
        return value

    def pairs(self):
        lc = ListCollector()
        for k, v in self._sortedpairs():
            lc.append(Pair(k, self.data[k]))
        return lc.list()

    def keys(self):
        return Seq2List(self.data.keys())

    def __len__(self):
        return len(self.data)

    def length(self):
        return self.__len__()

    def values(self):
        return Seq2List(self.data.values())

    def call(self, arglist):
        nargs = self.checkargs(arglist)
        if nargs == 1:
            return self.get(arglist.car(), Nil)
        if nargs == 2:
            key, value, *_ = arglist
            return self.put(key, value)
        raise PyleArgCountError(self, nargs, too_many=len > 2)


# EOF
