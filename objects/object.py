# The Object superclass of all Pyle2 objects

import abc

import pglobal
from .exception import *
from .interned import Interned


class Object():
    """This is the abstract base class for all Pyle objects."""

    # The Object has no slots itself, but anything that is to use slots must
    # have an inheritance chain of slot-having ancestors.
    __slots__ = tuple()

    def __init_subclass__(cls, **kwargs):
        """Register the class as an attribute of plobal."""
        super().__init_subclass__(**kwargs)
        setattr(pglobal, cls.__name__, cls)
        #print(f"setattr({repr(pglobal)}, {repr(cls.__name__)}, {cls})")
        
    def __bool__(self):
        return True

    def __iter__(self):
        print("object.__iter:", self)
        raise pglobal.PyleDataTypeError(
            "cannot iterate over non-sequence: {}", self)

    def __format__(self, format_spec):
        if format_spec == "r":
            return repr(self)
        return str(self).__format__(format_spec)

    def pid(self):
        """Return a terse string representation of type and id.

        This shall be used where other means to display the object
        are not present or not feasible, e.g. while encountering a
        recursive __repr__() call in a cyclic data structure.

        """
        return "#<{}{{{:x}}}>".format(type(self).__name__, id(self))

    def dump(self):
        parts = ["#<", self.__class__.__name__, ":"]
        # for item in dir(self):
        #     if item.startswith("__"):
        #         continue
        #     parts.append("\n  {} = {}".format(item, repr(getattr(self, item))))
        # for field, value in self.__dict__.items():
        #     parts.append(" {}={}".format(field, repr(value)))
        
        parts.append(">")
        return "".join(parts)

    def type(self):
        """Return the object's type as a symbol."""
        name = self.__class__.__name__
        if name.startswith("Pyle"):
            name = name[len("Pyle"):]
        return pglobal.Symbol(name.lower())

    def length(self):
        raise pglobal.PyleDataTypeError(
            "cannot get length of non-sequence: {}", self)

    def isPair(self):
        return False

    def isSymbol(self):
        return False

    def isString(self):
        return False

    def isEnvironment(self):
        return False

    def isNumber(self):
        return False

    def isFunction(self):
        return False

    def isMacro(self):
        return False

    def isBuiltin(self):
        return False

    def isLambda(self):
        return False

    def isList(self):
        return False

    def isTable(self):
        return False

    def isCallable(self):
        return False

    def isSpecial(self):
        return False

    def isRegepx(self):
        return False

    def isPort(self):
        return False

    def isRegexp(self):
        return False

    def isVector(self):
        return False

    def isSequence(self):
        return False

    def isComparable(self):
        return False


# The __init_subclass__ does not help for the superclass itself.
pglobal.Object = Object


class Callable(Object):
    """Base class for all callable things."""
    __slots__ = "_minargs", "_maxargs", "_name"
    
    def call(self, arglist):
        raise NotImplementedError()

    def isCallable(self):
        return True

    def docstring(self):
        raise NotImplementedError()

    def checkargs(self, args, isMacro=False):
        """Raise an appropriate exception if the arg count does not fit."""
        nargs = len(args)
        if nargs < self._minargs:
            raise pglobal.PyleArgCountError(self, nargs, too_many=False,
                                            isMacro=isMacro)
        if self._maxargs is None:
            return
        if nargs > self._maxargs:
            raise pglobal.PyleArgCountError(self, nargs, too_many=True,
                                            isMacro=isMacro)
        return nargs
    

class ComparableInterned(Object, metaclass=Interned):
    "Base class for comparable objects with a single value in self._data."
    __slots__ = "__weakref__", "_data"

    def isComparable(self):
        return True

    def __lt__(self, other):
        return self._data < other._data

    def __le__(self, other):
        return self._data <= other._data

    def __eq__(self, other):
        return self is other

    def __ne__(self, other):
        return self is not other

    def __gt__(self, other):
        return self._data > other._data

    def __ge__(self, other):
        return self._data >= other._data

    def __hash__(self):
        return id(self)

