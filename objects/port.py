# Pyle2 I/O ports

import io
import re
import sys
import jpylib as y

import pglobal
from . import *
from utils import *


pglobal.tracePortIO = Symbol("trace-portio")

# I/O case constants

portDirReadSym = Symbol(":input")
portDirWriteSym = Symbol(":output")
portDirRWSym = Symbol(":io")
portAppendSym = Symbol(":append")
portOverwriteSym = Symbol(":overwrite")
portErrorSym = Symbol(":error")
portCreateSym =  Symbol(":create")


class Port(Object):
    __slots__ = ["_file", "_name", "_read", "_write", "_repr", "_closed",
                 "_is_stringio", "_is_interactive", "_unread_ch", "_column",
                 "_line", "_prompt", "_histfile", "_linebuf", "_value"]

    def __init__(self, file, name, can_read=True, can_write=False,
                 is_stringio=False, is_interactive=False, prompt="",
                 history_file=None):
        """Return a new Port for a file object."""
        trace_if(pglobal.tracePortIO, f"Port({repr(file)}, {repr(name)}, {repr(can_read)}, {repr(can_write)}, {repr(is_stringio)}, {repr(is_interactive)}, {repr(prompt)}, {repr(history_file)})")
        self._file = file
        self._name = name
        self._read = can_read
        self._write = can_write
        self._repr = None
        self._closed = False
        self._is_stringio = is_stringio
        self._is_interactive = is_interactive
        if is_interactive:
            self._prompt = prompt
            self._histfile = history_file
            self._linebuf = None
        self._unread_ch = None
        self._column = 0
        self._line = 1
            
    def __str__(self, seen=False):
        return self.__repr__(seen)

    def __repr__(self, seen=False):
        if not self._repr:
            dir = ""
            if self._read:
                dir += "r"
            if self._write:
                dir += "w"
            if self._closed:
                dir = "closed:" + dir
            self._repr =  "#<{}[{}]\"{}\">".format(
                self.__class__.__name__, dir, self._name)
        return self._repr

    def __del__(self):
        if not self._closed:
            self._file.close()

    def isPort(self):
        return True

    def is_interactive(self):
        return self._is_interactive

    def name(self):
        return self._name

    def check_open(self):
        if self._closed:
            raise PyleIOError(self, "not open for reading")

    def check_read(self):
        self.check_open()
        if not self._read:
            raise PyleIOError(self, "not open for reading")

    def check_write(self):
        self.check_open()
        if not self._write:
            raise PyleIOError(self, "not open for writing")

    def check_interactive(self):
        self.check_read()
        if not self._is_interactive:
            raise PyleIOError(self, "not an interactive stream")

    @tracefn_if(pglobal.tracePortIO)
    def readchar(self):
        """Read one character from the port.

        Using this function, it is possible to unread a character using
        unreadchar() and have it back at the next readchar() call. Also, using
        this function, the port keeps track of line and column.

        Return the character as a string, or an empty string at EOF.

        """
        self.check_read()
        if self._unread_ch is None:
            ch = self._readchar_internal()
        else:
            ch = self._unread_ch
            self._unread_ch = None
            result = ch
        if ch == "":
            return ""
        if ch == "\n":
            self._line += 1
            self._column = 0
        else:
            self._column += 1
        trace_if(pglobal.tracePortIO, "readchar returns {}", repr(ch))
        return ch

    @tracefn_if(pglobal.tracePortIO)
    def unreadchar(self, ch):
        assert self._unread_ch is None, f"unreadchar more than once ({ch:r})"
        if ch:
            # On reading e.g. an atom, the Reader treats the empty character
            # (i.e. EOF) like any delimiter and unreads it. We could catch that
            # there, or here. I think it is more cautious to catch it here.
            self._unread_ch = ch
            if ch == "\n":
                self._line -= 1
                self._column = 0
            else:
                self._column -= 1

    @tracefn_if(pglobal.tracePortIO)
    def _readchar_internal(self):
        if self._is_interactive:
            if self._linebuf is None or self._linebuf.eof():
                line = self.readline()
                if line == "":
                    return ""
                self._linebuf = y.StringReader(line)
            return self._linebuf.next() or ""
        else:
            return self._file.read(1)

    @tracefn_if(pglobal.tracePortIO)
    def skip_rest_of_line(self):
        while True:
            ch = self.readchar()
            if not ch or ch == "\n":
                break

    @tracefn_if(pglobal.tracePortIO)
    def column(self):
        return self._column

    @tracefn_if(pglobal.tracePortIO)
    def line(self):
        return self._line

    @tracefn_if(pglobal.tracePortIO)
    def readline(self, eof_error_p=False):
        self.check_read()
        if self._is_interactive:
            prompt = self._prompt
            if callable(prompt) or isinstance(prompt, Callable):
                prompt = prompt()
            try:
                return (input(str(prompt)) or "") + "\n"
            except EOFError as e:
                if eof_error_p:
                    raise e
                return ""
        else:
            return self._file.readline()

    def setHistfile(self, pathname):
        self.check_interactive()
        self._histfile = pathname

    def readHistory(self):
        self.check_interactive()
        if self._histfile:
            utils.read_history_file(self._histfile)

    def saveHistory(self):
        self.check_interactive()
        if self._histfile:
            utils.save_history_file(self._histfile)

    def setPrompt(self, prompt):
        self._prompt = prompt

    @tracefn_if(pglobal.tracePortIO)
    def read(self, count):
        self.check_read()
        return self._file.read(count)

    @tracefn_if(pglobal.tracePortIO)
    def write(self, string):
        self.check_write()
        self._file.write(str(string))

    def flush(self):
        self.check_write()
        self._file.flush()

    def close(self):
        if self._is_stringio:
            self._value = self._file.getvalue()
        self._file.close()
        self._closed = True
        self._repr = None

    def name(self):
        return self._name

    def value(self):
        if self._is_stringio:
            if self._closed:
                return self._value
            return self._file.getvalue()
        return Nil


class StringInputPort(Port):
    def __init__(self, the_string, name="*input-string*"):
        super().__init__(io.StringIO(str(the_string)), name)


pglobal.Stdin = Port(sys.stdin, "*stdin*", True, False, is_interactive=True)
Symbol("*stdin*").bindValue(pglobal.Stdin)
pglobal.Stdout = Port(sys.stdout, "*stdout*", False, True)
Symbol("*stdout*").bindValue(pglobal.Stdout)
pglobal.Stderr = Port(sys.stderr, "*stderr*", False, True)
Symbol("*stderr*").bindValue(pglobal.Stderr)

# EOF
