
import pglobal
from .symbol import Symbol, Nil
from .string import String

# nearly as important as Nil
T = Symbol("t", True, True)


def _sysvar(name, value=None, self_immutable=False, doc=None):
    sym = Symbol(name, self_immutable)
    if value is not None:
        pglobal.rootEnvironment.bindValue(sym, value)
    if doc:
        sym.putprop(Symbol("doc"), String(doc))
    sym.putprop(Symbol("sys"), T)
    return sym


# A few variables needed by the system; I follow SBCLs example in which ones to
# make immutable.

QuoteSymbol = _sysvar("quote")
FunctionSymbol = _sysvar("function")

# These don't need to be immutable, apparently.
UnquoteSymbol = _sysvar("unquote")
QuasiquoteSymbol = _sysvar("quasiquote")
UnquoteSplicingSymbol = _sysvar("unquote-splicing")


# Define some system-specific variables; these should indeed all be here.

pglobal.CurrentLoadFile = _sysvar(
    "*current-load-file*", Nil, False,
    doc="Name of file currently being loaded")

pglobal.LastError = _sysvar(
    "*last-error*", Nil, False, doc="Last error that occured, or nil")

pglobal.CmdArgs = _sysvar(
    "*args*", Nil, False, doc="Command line arguments of the program")

pglobal.PrintStacktraces = _sysvar(
    "*print-stacktraces*", T, False, doc="Print stack traces if non-nil")

pglobal.PrintLoadNotice = _sysvar(
    "*print-load-notice*", T, False,
    doc="Print notices of files being loaded if non-nil")

pglobal.LoadPath = _sysvar(
    "*load-path*", Nil, False,
    doc="List of directories to search for files to be loaded.")

pglobal.Nil = Nil
pglobal.T = T

# EOF
