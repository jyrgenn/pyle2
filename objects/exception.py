# Pyle Exception class, errors and throws
# Should these be Pyle Objects?

import sys
import traceback
import jpylib as y

import pglobal
#from objects import *
import utils
from .string import String


class PyleException(Exception):
    """This is the base class for all exceptions explicitly raised by Pyle.

    All Pyle errors and throws will be derived from this.

    """
    def __init_subclass__(cls, **kwargs):
        """Register the class as an attribute of plobal."""
        super().__init_subclass__(**kwargs)
        setattr(pglobal, cls.__name__, cls)
        #print(f"setattr({repr(pglobal)}, {repr(cls.__name__)}, {cls})")
        

    def __init__(self, message, *args, isError=True):
        """Message is a string to be formatted with args.
        """
        self._message = message
        self.args = args
        if not getattr(self, "name", None):
            name = self.__class__.__name__
            if name.startswith("Pyle"):
                name = name[4:]
            self.name = name
        self.isError = isError
        if isError:
            pglobal.LastError.setValue(String(self))
            if pglobal.raise_exceptions >= 2:
                traceback.print_stack()
                print(self)


    def __str__(self):
        return "{}: {}".format(self.name, self.message())

    def __repr__(self, seen=set()):
        with utils.printContext(self, seen) as yes:
            if yes: return yes
            if self.args:
                exc_string = self._message.format(
                    *map(lambda ob: ob.__repr__(seen), *self.args))
            else:
                exc_string = self._message
            return "#<{} '{}'>".format(self.name, exc_string)

    def message(self):
        return self._message.format(*self.args)


class PyleCantHappenError(PyleException):
    """Shit will happen anyway."""
    pass


class PyleInvalidCallError(PyleException):
    pass


class PyleValueError(PyleException):
    pass

class PyleArgTypeError(PyleException):

    def __init__(self, function, argname, arg, mustbe):
        if not isinstance(function, str):
            function = function.name()
        super().__init__("argument `{}' to ({}) must be {}, but is {}: {}",
                         str(argname), function, str(mustbe).lower(),
                         type(arg).__name__.lower(), repr(arg))


class PyleLoadFileNotFound(PyleException):

    def __init__(self, fname, load_path=None):
        in_path = ""
        if load_path:
            in_path = " in load path " + str(load_path)
        super().__init__("file {} not found{}".format(repr(fname), in_path))


class PyleArgCountError(PyleException):

    def __init__(self, function, given, too_many, isMacro=False):
        """Incorrect number of argument to function.

        If too_many is true, it was too many arguments, otherwise too few.
        """
        needs = "takes "
        if function._minargs == function._maxargs:
            needs += "{}".format(function._minargs)
        else:
            needs += "at least {}".format(function._minargs)
            if function._maxargs is not None:
                needs += ", at most {}".format(function._maxargs)
        if too_many:
            fault = "too many"
        else:
            fault = "too few"
        super().__init__("{} {} {} arguments for{} `{}'; {}",
                         given, "is" if given == 1 else "are",
                         fault, " macro" if isMacro else "",
                         function._name, needs)
            

class PyleImmutableError(PyleException):
    """Error trying to set the value of an immutable variable."""
    pass


class PyleUndefError(PyleException):
    """Error trying to get the value of an undefined variable."""
    pass


class PyleUndefFunctionError(PyleException):
    """Error trying to get the function of symbol without."""
    def __init__(self, symbol):
        super().__init__("function `{}' is not defined", symbol)


class PyleNoFunctionError(PyleException):
    """Cannot call non-function object as a function."""
    def __init__(self, obj):
        super().__init__("`{}' is not a known function", obj)


class PyleDataTypeError(PyleException):
    """Wrong data type in operation."""
    pass


class PyleKeyError(PyleException):
    def __init__(self, table, key):
        super().__init__("key does not exist in table {}: {}", table, key)


class PyleIndexError(PyleException):
    def __init__(self, vector, index):
        super().__init__("index {} out of bounds in vector {}", index, vector)


class PyleReturn(PyleException):
    def __init__(self, returnValue):
        super().__init__("return from function: {}", returnValue,
                         isError=False)

    def value(self):
        return self.args[0]
    

class PyleThrow(PyleException):

    def __init__(self, tag, value):
        self.tag = tag
        self.value = value
        super().__init__("uncaught throw tagged {} with value {}", tag, value,
                         isError=False)

    def __repr__(self, seen=set()):
        with utils.printContext(self, seen) as yes:
            if yes: return yes
            return "#<{s} tag:{t} value:{v}>".\
                format(s=self.__class__.__name__, t=self.tag,
                       v=self.value.__repr__(seen))

    def __str__(self, seen=set()):
        with utils.printContext(self, seen) as yes:
            if yes: return yes
            return "Uncaught throw `{}', value={}".format(
                self.tag, self.value.__repr__(seen))


class PyleReaderEOFError(PyleException):

    def __init__(self, reader, place=None):
        append = ""
        if place is not None:
            append = " " + place
        super().__init__("unexpected EOF at {}:{}:{}{}".format(
            reader.name(), reader.line(), reader.column(), append))


class PyleIOError(PyleException):

    def __init__(self, port, message):
        super().__init__("I/O error on port {}: {}", port, message)


class PyleEOFError(PyleException):

    def __init__(self, port):
        super().__init__("EOF on port {}", port)


class PyleOpenError(PyleException):
    pass


class PyleTypeAssertionError(PyleException):
    pass


class PyleSyntaxError(PyleException):

    def __init__(self, reader, message, offender=None):
        append = ""
        location = ""
        if offender is not None:
            append = ": " + repr(offender)
        if reader:
            location = " in {}:{}:{}".format(
                reader.name(), reader.line(), reader.column())
        super().__init__("{}{}{}".format(message, location, append))


class PyleBuiltinError(PyleException):
    """A Python exception caught in a builtin."""

    def __init__(self, e, builtin_name, args, kwargs, is_error=True):
        argslist = ", ".join(map(repr, args))
        if kwargs:
            argslist += ", " + ", ".join([f"{k}={v:r}" for k, v in kwargs])
        self.name = type(e).__name__
        super().__init__("{} in {}({})".format(e, builtin_name, argslist))
        

class PyleProgramError(PyleException):
    pass

class PyleProgramAbort(PyleException):
    pass


pglobal.PyleException = PyleException
