# Pyle String class (do we need much here? at all? yes, repr w/ special chars)

from .object import *


class String(ComparableInterned):
    """A string object."""
    __slots__ = tuple() 

    def __init__(self, s):
        """Accept any kind of argument and str() it."""
        self._data = str(s)

    def isString(self):
        return True

    def __str__(self):
        return self._data

    def __len__(self):
        return len(self._data)
        
    def value(self):
        return self._data

    def __repr__(self, seen=False):
        """For now; repr of unprintable chars will follow."""
        result = ['"']
        for ch in self._data:
            if ch in pglobal.specialChar2escaped:
                result.append("\\")
                result.append(pglobal.specialChar2escaped[ch])
            elif ch.isprintable():
                result.append(ch)
            else:
                result.append("\\")
                codepoint = ord(ch)
                if codepoint <= 0xff:
                    result.append("x")
                    result.append(format(codepoint, "02x"))
                elif codepoint <= 0xffff:
                    result.append("u")
                    result.append(format(codepoint, "04x"))
                else:
                    result.append("U")
                    result.append(format(codepoint, "08x"))
        result.append('"')
        return "".join(result)

    def length(self):
        return self.__len__()

    def append(self, other):
        return String(self._data + str(other))

    def substr(self, start=None, end=None):
        if start is None and end is None:
            return self
        if start is None:
            return String(self._data[:end])
        if end is None:
            return String(self._data[start:])
        return String(self._data[start:end])

