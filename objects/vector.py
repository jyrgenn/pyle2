# Pyle2 vectors

import jpylib as y

from . import *
from utils import *

class Vector(Callable):
    __slots__ = "data",

    def __init__(self, elems):
        """Return a new vector with initial elements."""
        self.data = []
        self._minargs = 1
        self._maxargs = 2
        self._name = "*vector*"
        for elem in elems:
            assert isinstance(elem, Object), "element not Object: " + repr(elem)
            self.data.append(elem)
            
    def __len__(self):
        """Return the size of the vector."""
        return len(self.data)

    def __str__(self, seen=set()):
        return self.__repr__(seen)

    def __repr__(self, seen=set()):
        with utils.printContext(self, seen) as yes:
            if yes: return yes
            return "#(" + " ".join(
                map(lambda el: el.__repr__(seen), self.data)) + ")"

    def __bool__(self):
        return True

    def __iter__(self):
        for elem in self.data:
            yield elem

    def length(self):
        return self.__len__()

    def isVector(self):
        return True

    def isSequence(self):
        return True

    def docstring(self):
        return """vector (VECTOR INDEX &optional VALUE) => value
Return the value of the VECTOR slot at INDEX.
With optional VALUE, set the value."""

    def set(self, index, value):
        assert isinstance(value, Object), "set to non-Object: " + repr(value)
        try:
            self.data[index] = value
            return value
        except IndexError:
            raise PyleIndexError(self, index)

    def get(self, index, default=None):
        try:
            return self.data[index]
        except IndexError:
            if default:
                return default
            raise PyleIndexError(self, index)

    def call(self, arglist):
        nargs = self.checkargs(arglist)
        index, restargs = arglist.cxr()
        if not index.isNumber():
            raise PyleDataTypeError("vector index must be number, is {}", index)
        index = index.value()
        if nargs == 1:
            return self.get(index)
        if nargs == 2:
            newvalue = restargs.car()
            return self.set(index, newvalue)
        raise PyleArgCountError(self, nargs, too_many=len > 2)


# EOF
