# Pyle2 eval function

import jpylib as y

import pglobal
from utils import *
from objects import *
from .macroexpand import macroexpandForm


breakSymbol = Symbol("*break-eval*")

def EvalProgn(forms):
    """Eval each of forms (a List), and return the last value."""
    y.trace("EvalProgn", forms)
    result = Nil
    for form in forms:
        result = Eval(form)
    return result


def evalFun(obj, reclevel=0, show=None):
    # print("evalFun(", obj, reclevel, ")")
    if obj and reclevel <= 2:
        if obj.isCallable():
            return obj
        if obj.isSymbol():
            return evalFun(obj.function() or obj.getValue(must_exist=False),
                           reclevel + 1, show or obj)
        return evalFun(Eval(obj), reclevel+1, show or obj)
    raise PyleNoFunctionError(show or obj)


def evalArgs(forms):
    lc = ListCollector()
    for form in forms:
        lc.append(Eval(form))
    return lc.list()


_eval_level = 0

def Eval(form, expandMacros=False):
    global _eval_level
    savedLevel = _eval_level
    trace_if("eval", "Eval[{}]: {:r}", savedLevel, form)
    pglobal.evalCount += 1
    _eval_level += 1
    if _eval_level > pglobal.maxEvalLevel:
        pglobal.maxEvalLevel = _eval_level
    if expandMacros:
        form = macroexpandForm(form)
    break_if(breakSymbol, "Eval[{}]: {:r}", _eval_level, form)
    try:
        if form.isSymbol():
            value = form.getValue()
        elif form.isPair():
            func, args = form.cxr()
            func = evalFun(func)
            if not func.isSpecial():
                args = evalArgs(args)
            value = func.call(args)
        else:
            value = form
        print_if(breakSymbol, "[{}] => {:r}", _eval_level, value)
        return value
    except (PyleThrow, PyleReturn) as throw:
        raise throw
    except Exception as e:
        if pglobal.PrintStacktraces.getValue() and not pglobal.inErrset:
            env = pglobal.theEnvironment
            the_dict = ""
            if env is not pglobal.rootEnvironment:
                the_dict = str(env.dict(local_only=True))
            print("#{} Eval {}\n   in {}{}".format(
                savedLevel, form, env, the_dict))
        raise e
    finally:
        _eval_level = savedLevel


pglobal.Eval = Eval
pglobal.EvalProgn = EvalProgn
