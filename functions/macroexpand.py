# Pyle2: expand macros

import jpylib as y

from objects import *

def expandList(list):                   # return new list, haveExpanded
    haveExpanded = False
    lc = ListCollector()
    # cannot use simple iteration over the list, which would not
    # handle improper lists
    while list.isPair():
        elem, did = expandFormRecurse(list.car())
        lc.append(elem)
        haveExpanded = haveExpanded or did
        list = list.cdr()
    if list:                            # last cdr of improper list
        elem, did = expandFormRecurse(list)
        lc.lastcdr(elem)
        haveExpanded = haveExpanded or did
    return lc.list(), haveExpanded

def expandFormRecurse(form):            # return expanded form, haveExpanded
    if form.isPair():
        head = form.car()
        if head.isSymbol():
            args = form.cdr()
            maybeMacro = head.function()
            if maybeMacro and maybeMacro.isMacro():
                result = maybeMacro.expand(args), True
            else:
                expargs, did = expandList(args)
                result = Pair(head, expargs), did
        else:
            result = expandList(form)
    else:
        result = form, False
    return result

def macroexpandForm(form):
    needExpand = True                   # learning to live witout post-checked
    while needExpand:                   # loops
        y.debug("need to expand:", form)
        form, needExpand = expandFormRecurse(form)
    y.debug("expand returns:", form)
    return form
