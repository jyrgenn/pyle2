(require 'regtests)

(test-is "fset" (progn (fset 'fooo (lambda (n) (+ n n)))
                        (fooo 34))
         "68")
(test-err "lambda 1" (lambda)
          #/too few arguments for/)
(test-err "lambda 2" (lambda 3)
          #/params must be symbol or list/)
(test-is "lambda 3" (lambda (n) (+ 3 n)) "#<lambda (n) (+ 3 n)>")
(test-is "defun" (progn (defun lala (n) (* n n))
                        (lala 123))
         "15129")

(defun f () nil)
(defmacro g () nil)
(defvar foomly)

(test-is "functionp" (map #'functionp (list #'f #'g foomly))
         '(t t nil))

(defparameter f-arglist '(n x . other))
(defparameter f-body '((list n x other)))
(defparameter f-docstring "A sample function with a complicated arglist and...
...a multi-line docstring.")
(defparameter sample-f
  `(defun sample-function ,f-arglist ,f-docstring ,@f-body))

(eval sample-f)

(defparameter anon1 (lambda (n p)
                      "Be fruitful and multiply!"
                      (* n p)))

(test-is "function-body 1" (function-body #'sample-function)
         f-body)
(test-err "function-body 2" (function-body #'cdr)
          #/type builtin has no body/)
(test-is "function-body 3" (function-body anon1)
         '((* n p)))

(defun function-docstring (fun) (doc fun nil t))

(test-is "function-docstring 1" (function-docstring #'sample-function)
         (string "lambda (sample-function n x . other)\n" f-docstring))
(test-is "function-docstring 2" (function-docstring #'cdr)
         "(cdr list)

Return the cdr of LIST.")
(test-is "function-docstring 3" (function-docstring anon1)
         "lambda (None n p)\nBe fruitful and multiply!")

(test-is "function-params 1" (function-params #'sample-function)
         f-arglist)
(test-is "function-params 2" (function-params #'with-open-file)
         '(file-declaration . bodyforms))
(test-is "function-params 3" (function-params anon1)
         '(n p))

(test-is "function-call 1" (sample-function 3 2.22)
         '(3 2.22 nil))

;; check that modifying code works as it should
(setf (cadar (function-body #'sample-function)) 17)
(test-is "function-call 2" (sample-function 3 2.22)
         '(17 2.22 nil))

;; check that self-modifying code works as it should
(defun changeling ()
  (let ((n 0))
    (setf (car (cdr (car (car (cdr (car (function-body #'changeling)))))))
          (+ n 1))
    n))

(test-is "function-call 3" (changeling) 0)
(test-is "function-call 4" (changeling) 1)
(test-is "function-call 5" (changeling) 2)
(test-is "function-call 6" (changeling) 3)
(test-is "function-call 7" (changeling) 4)

(done-testing)
