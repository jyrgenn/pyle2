(require 'regtests)

;;; EOF handling
(test-err "read EOF err" (read (open "/dev/null")) #/EOF on port/)
(test-is "read EOF nil" (read (open "/dev/null") nil) nil)
(test-is "read EOF value" (read (open "/dev/null") nil 'hänschen-kline)
         'hänschen-kline)

(test-err "read EOF err 2" (with-open-file (f "/dev/null")
                           (read f))
          #/EOF on port/)
(test-is "read EOF nil 2" (with-open-file (f "/dev/null")
                          (read f nil))
         nil)
(test-is "read EOF value 2" (with-open-file (f "/dev/null")
                            (read f  nil 'hänschen-kline))
         'hänschen-kline)

(done-testing)
