(require 'regtests)

(defvar l '(3 4 5))
(test-is "push 0" (progn (push (* 3 4) l) l) '(12 3 4 5))
(test-is "push 1" (push (* 3 4) l) '(12 12 3 4 5))

(makunbound 'foo)
(test-err "push 2e" (push (* 3 4) foo) #/unbound variable foo/)
(test-is "push 3" (let ((foo 1337)) (push (* 3 4) foo))
         '(12 . 1337))
(test-err "push 4e" (push (* 3 4) (list 12 23))
         #/is not symbol/)

(test-is "pop 0" (pop l) 12)
(test-is "pop 1" l '(12 3 4 5))
(setq l ())
(test-is "pop 2" (pop l) nil)

(done-testing)
