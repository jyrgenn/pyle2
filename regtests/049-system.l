(require 'regtests)

(defun apropos-list (match)
  (apropos match t))

(test-is "apropos string 1" (> (length (apropos-list "a")) 200) t)
(test-is "apropos string 2" (= (length (apropos-list "^a")) 0) t)

;; the correctness of this *may* change some time; currently we are at
;; 50 matches
(test-is "apropos regexp" (let ((len (length (apropos-list #/^a/))))
                            (and (> len 40)
                                 (< len 100)))
         t)

(defparameter a 19)
(defparameter b 21)

(test-is "assertion good" (assert (< a b) "Apfel < Birne") nil)
(incf a 2)
(test-err "assertion fail 1" (assert (< a b) "Apfel < Birne")
          #/assertion failed: .* Apfel < Birne/)
(test-err "assertion fail 2" (assert (< a b))
          #/assertion failed: `\(< a b\)'/)
(test-err "assertion fail 3" (assert (< a b) (format nil "{} < {}" 'a 'b))
          #/assertion failed: `\(< a b\)'; a < b/)
(test-err "assertion fail 4" (assert (< a b) (format nil "{} < {}" a b))
          #/assertion failed: `\(< a b\)'; 21 < 21/)

(test-is "declare good" (declare (number a b)) t)
(test-err "declare err" (declare (int a b))
          #/declared type for variable a is unknown/)
(test-err "declare fail" (declare (string a))
          #/variable a is declared type string, but value is 21 \(number\)/)

(defun fac-decl (n)
  "Return the faculty of N."
  (declare (number n))
  (if (zerop n)
      1
    (* n (fac-decl (1- n)))))

(test-is "fac-decl good" (fac-decl 7) 5040)
(test-err "fac-decl err" (fac-decl "7")
          #/argument n is declared type number, but value is "7" \(string\)/)

;; double-decl
(defun nakes-and-adders (a b)
  "Return the sum of A and B."
  (declare (number a b))
  (+ a b))

(test-is "adder 0" (nakes-and-adders 3 4) 7)
(test-err "adder 1" (nakes-and-adders "3" 4)
          #/argument a is declared type number, but value is "3" \(string\)/)
(test-err "adder 2" (nakes-and-adders 3 '|4|)
          #/argument b is declared type number, but value is |4| (symbol)/)

;; two. decls. Wouldn't work with CL, which only allows one declare statement.
(fmakunbound 'nakes-and-adders)
(defun nakes-and-adders (a b)
  "Return the sum of A and B."
  (declare (number b))
  (declare (number a))
  (+ a b))

(test-is "adder 0a" (nakes-and-adders 3 4) 7)
(test-err "adder 1a" (nakes-and-adders "3" 4)
          #/variable a is declared type number, but value is "3" \(string\)/)
(test-err "adder 2a" (nakes-and-adders 3 '|4|)
          #/variable a is declared type number, but value is |4| \(string\)/)


(done-testing)
