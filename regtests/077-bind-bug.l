(require 'regtests)

;; this has been fine for long
(test-is "destructure-list" (let (((a b) '("c"))) (list b a)) '(nil "c"))

;; this bombed in previous versions due to cxr on a string
(test-is "destructure-string" (let (((a b) "c")) (list b a)) '(nil nil))

(done-testing)
