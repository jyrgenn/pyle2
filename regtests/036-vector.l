(require 'regtests)

(defparameter v (make-vector 3 4))
(test-is "made vector" v #(4 4 4))

(setf v #(14 24 34))
(test-is "new vector" v #(14 24 34))

(test-err "vector-get el 3" (vector-get v 3) #/index 3 out of bounds/)

(test-is "vector-get 0" (vector-get v 0) 14)
(test-is "vector-get 1" (vector-get v 1) 24)
(test-is "vector-get 2" (vector-get v 2) 34)
(test-err "vector-get 3" (vector-get v 3) #/index 3 out of bounds/)
(test-is "vector-get -1" (vector-get v -1) 34)
(test-err "vector-get -4" (vector-get v -4) #/index -4 out of bounds/)

(test-is "vector-set 1" (vector-set v 1 5) 5)

(test-is "vector after set" v "#(14 5 34)")

(test-is "vector-get 0a" (vector-get v 0) 14)
(test-is "vector-get 1a" (vector-get v 1) 5)
(test-is "vector-get 2a" (vector-get v 2) 34)

(test-is "vector-set 0b" (vector-set v 0 6) 6)
(test-is "vector-set 1b" (vector-set v 1 7) 7)
(test-is "vector-set 2b" (vector-set v 2 8) 8)
(test-err "vector-set 3b" (vector-set v 3 9) #/index 3 out of bounds/)
(test-is "vector-set -1b" (vector-set v -1 0) 0)

;; same as above, with vector as callable
(setf v #(14 24 34))
(test-is "vector-get 0/c" (v 0) 14)
(test-is "vector-get 1/c" (v 1) 24)
(test-is "vector-get 2/c" (v 2) 34)
(test-err "vector-get 3/c" (v 3) #/index 3 out of bounds/)
(test-is "vector-get -1/c" (v -1) 34)
(test-err "vector-get -4/c" (v -4) #/index -4 out of bounds/)

(test-is "vector-set 1/c" (v 1 5) 5)

(test-is "vector after set/c" v "#(14 5 34)")

(test-is "vector-get 0a/c" (v 0) 14)
(test-is "vector-get 1a/c" (v 1) 5)
(test-is "vector-get 2a/c" (v 2) 34)

(test-is "vector-set 0b/c" (v 0 6) 6)
(test-is "vector-set 1b/c" (v 1 7) 7)
(test-is "vector-set 2b/c" (v 2 8) 8)
(test-err "vector-set 3b/c" (v 3 9) #/index 3 out of bounds/)
(test-is "vector-set -1b/c" (v -1 0) 0)
(test-err "vector-set -4b/c" (v -4 0) #/index -4 out of bounds/)
(test-is "vector again" v "#(6 7 0)")

;; same as above, with aref
(setf v #(14 24 34))
(test-is "aref 0" (aref v 0) 14)
(test-is "aref 1" (aref v 1) 24)
(test-is "aref 2" (aref v 2) 34)
(test-err "aref 3" (aref v 3) #/index 3 out of bounds/)
(test-is "aref -1" (aref v -1) 34)
(test-err "aref -4" (aref v -4) #/index -4 out of bounds/)

(test-is "vector-set 1/a" (vector-set v 1 5) 5)

(test-is "vector after set/a" v "#(14 5 34)")

(test-is "aref 0a" (aref v 0) 14)
(test-is "aref 1a" (aref v 1) 5)
(test-is "aref 2a" (aref v 2) 34)

(test-is "vector type" (type-of v) "vector")

(test-is "make-vector 1" (make-vector 3 'a) #(a a a))
(test-is "make-vector 2" (make-vector 5 (let ((n 6)) (lambda () (incf n))))
         #(7 8 9 10 11))

(done-testing)
