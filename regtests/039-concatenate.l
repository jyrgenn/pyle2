(require 'regtests)

(test-is "concat v->vector"
         (concatenate 'vector #(2) #(3 4 5 6) #(a bc d) #("huhu" 13))
         #(2 3 4 5 6 a bc d "huhu" 13))

(test-is "concat v->list"
         (concatenate 'list #(2) #(3 4 5 6) #(a bc d) #("huhu" 13))
         '(2 3 4 5 6 a bc d "huhu" 13))

(test-is "concat l->list"
         (concatenate 'list '(2) '(3 4 5 6) '(a bc d) '("huhu" 13))
         '(2 3 4 5 6 a bc d "huhu" 13))

(test-is "concat l->vector"
         (concatenate 'vector '(2) '(3 4 5 6) '(a bc d) '("huhu" 13))
         #(2 3 4 5 6 a bc d "huhu" 13))

(test-is "concat mixed->list"
         (concatenate 'list '(2) #(3 4 5 6) '(a bc d) '("huhu" 13))
         '(2 3 4 5 6 a bc d "huhu" 13))

(test-is "concat mixed->vector"
         (concatenate 'vector '(2) '(3 4 5 6) #(a bc d) '("huhu" 13))
         #(2 3 4 5 6 a bc d "huhu" 13))

(test-is "concat 1->list" (concatenate 'list '(2)) '(2))

(test-is "concat 1->vector" (concatenate 'vector '(2)) #(2))

(test-is "concat empty->list" (concatenate 'list) '())

(test-is "concat empty->vector" (concatenate 'vector) #())

(test-err "concat unknown type"
          (concatenate 'symbol '(2) '(3 4 5 6) #(a bc d) '("huhu" 13))
          #/concatenate: unknown type argument `symbol'/)


(done-testing)
