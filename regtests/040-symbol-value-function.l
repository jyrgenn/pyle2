(require 'regtests)

(test-is "symbol name simple" (intern "lalala") "lalala")
(test-is "symbol name blanks" (intern "la|la la") "la|la la")
(test-is "symbol name readable" (prin1-to-string (intern "la|la la"))
         "|la\\|la la|")
(test-is "symbol name 1" (symbol-name 'lalala) "lalala")
(test-is "symbol name 2" (symbol-name '|la la la|) "la la la")

(test-is "symbol name #a" (prin1-to-string (intern "#a")) "|#a|")
(test-is "symbol name a#" (prin1-to-string (intern "a#")) "a#")
(test-is "symbol name ,a" (prin1-to-string (intern ",a")) "|,a|")
(test-is "symbol name a," (prin1-to-string (intern "a,")) "a,")
(test-is "symbol name a{" (prin1-to-string (intern "a{")) "a{")
(test-is "symbol name a}" (prin1-to-string (intern "a}")) "a}")
(test-is "symbol name a[" (prin1-to-string (intern "a[")) "a[")
(test-is "symbol name a]" (prin1-to-string (intern "a]")) "a]")
(test-is "symbol name a|" (prin1-to-string (intern "a|")) "|a\\||")
(test-is "symbol name a(" (prin1-to-string (intern "a(")) "|a(|")
(test-is "symbol name a)" (prin1-to-string (intern "a)")) "|a)|")
(test-is "symbol name a\"" (prin1-to-string (intern "a\"")) "|a\"|")
(test-is "symbol name a'" (prin1-to-string (intern "a'")) "|a'|")
(test-is "symbol name a;" (prin1-to-string (intern "a;")) "|a;|")
(test-is "symbol name a\\" (prin1-to-string (intern "a\\")) "|a\\\\|")
(test-is "symbol name a`" (prin1-to-string (intern "a`")) "|a`|")
(test-is "symbol name ." (prin1-to-string (intern ".")) "|.|")
(test-is "symbol name ," (prin1-to-string (intern ",")) "|,|")
(test-is "symbol name 119" (prin1-to-string (intern "119")) "|119|")
(test-is "symbol name 1.3e4" (prin1-to-string (intern "1.3e4")) "|1.3e4|")
(test-is "symbol name 1,3e4" (prin1-to-string (intern "1,3e4")) "1,3e4")

(defvar funsym 'double)
(defun double (n) (* 2 n))
(test-is "symbol-function double" (symbol-function funsym) #'double)

;;(setf trace-builtin t)
(defvar nofunsym 'foomly)
(fmakunbound 'foomly)
(test-err "sym-func double trouble" (symbol-function nofunsym)
          #/function `foomly' is not defined/)

(defvar gugu 1337)
(defvar gigi 'gugu)
(test-form "symbol-value 1" (symbol-value gigi) 1337)

(makunbound 'gipsnich)
(test-err "symbol-value 2" (symbol-value 'gipsnich)
          #/unbound variable/)

;;; tests of uninterned symbols removed, because we don't have them
;;; any more

(done-testing)
