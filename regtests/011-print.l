(require 'regtests)

(fset 'print-to-string #'princs)

(defmacro list-print (expr)
  "Binds s to output port."
  `(let (ret)
     (list (with-output-to-string (s)
             (setf ret ,expr))
           ret)))

(test-is "print string" (list-print (print "dubidubidu?" s))
         '("\n\"dubidubidu?\" " "dubidubidu?"))
(test-is "print symbol" (list-print (print 'a s)) '("\na " a))
(test-is "print number" (list-print (print 3.1415926589793 s))
         '("\n3.1415926589793 " 3.1415926589793))
(test-is "print cons" (list-print (print (cons (cons 3 4) nil) s))
         '("\n((3 . 4)) " ((3 . 4))))
(test-is "print-to-string string" (print-to-string "lala2") "lala2")
(test-is "print-to-string number" (print-to-string 3) "3")
(test-is "print-to-string symbol" (print-to-string 'sisismi) 'sisismi)
(test-is "print-to-string cons" (print-to-string (cons (cons 5 6) nil))
         "((5 . 6))")

(done-testing)
