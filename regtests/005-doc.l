(require 'regtests)

(defun dog (collar &optional leash) "Waff!" (list 'dog collar leash))
(test-is "docstring 1" (doc #'dog nil t)
         "lambda (dog collar &optional leash)\nWaff!")
(test-is "docstring 2" (doc 'dog t t)
         "lambda (dog collar &optional leash)")
(defun cat (&optional collar) t)
(test-is "docstring 3" (doc 'cat nil t)
         "lambda (cat &optional collar)")
(test-is "docstring 4" (doc 'cat t t)
         "lambda (cat &optional collar)")
(test-is "docstring 5" (doc 'symbolp t t)
         "lambda (symbolp ob)")
(test-is "docstring 6" (doc 'symbolp nil t)
         "lambda (symbolp ob)\nReturn t iff OB is a symbol, otherwise nil.")
(test-is "docstring 7" (doc 'if t t)
         "macro (if condition if-clause . else-clauses)")

(test-is "docstring table brief" (doc #:() t t)
         "table (TABLE KEY &optional VALUE) => value")

(test-is "docstring table long" (doc #:() nil t)
         "table (TABLE KEY &optional VALUE) => value
Return the value stored in the TABLE for KEY.
With optional VALUE, set the value.")

(test-is "docstring vector brief" (doc #() t t)
         "vector (VECTOR INDEX &optional VALUE) => value")

(test-is "docstring vector long" (doc #() nil t)
         "vector (VECTOR INDEX &optional VALUE) => value
Return the value of the VECTOR slot at INDEX.
With optional VALUE, set the value.")

(test-is "docstring regexp brief" (doc #// t t)
         "regexp (REGEXP STRING &optional ALL) => matches")

(test-is "docstring regexp long" (doc #// nil t)
         "regexp (REGEXP STRING &optional ALL) => matches
If STRING matches REGEXP, return list of match and sub-matches, else
nil. With optional third argument ALL non-nil, a list of match lists for
(potentially) multiple matches is returned.

Regular expression syntax is that of the Python regexp package, which is
largely similar to that of the Perl and Go languages. A \"(?flags)\"
specification in the regexp can modify the behaviour of the match in the
current group. Possible flags are, among others:

i  case-insensitive (default false)
m  multi-line mode: ^ and $ match begin/end line in addition to begin/end
   text (default false)
s  let . match
   (default false)")

(done-testing)
