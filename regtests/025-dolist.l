(require 'regtests)

(test-is "dolist 0"
         (let (result)
           (dolist (el '(a b c d e f g h i) (nreverse result))
             (push el result)))
         '(a b c d e f g h i))

(test-is "doseq 10"
         (let (result)
           (doseq (el #(a b c d e f g h i) (apply #'string (nreverse result)))
             (push el result)))
         "abcdefghi")

(done-testing)
