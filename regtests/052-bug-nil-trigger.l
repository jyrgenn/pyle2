(require 'regtests)

(test-is "99 sort vector 1" (sort #(60) #'>) #(60))
(test-err "sort string err" (sort "W" #'>)
          #/declared type sequence, but value is .*string/)

(done-testing)
