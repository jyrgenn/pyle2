(require 'regtests)

(defparameter url "http://golang.org/pkg/regexp/#Regexp.FindStringSubmatch")
(defparameter urlre #/^(([^:\/?\#]+):\/\/)?([^\/?\#:]*)(:([0-9]+))?(\/.*)?/)

;;; first, matches using regexp-match

(test-is "regexp read" #/assertion failed: `(< a b)'; a < b/
         #/assertion failed: `(< a b)'; a < b/)

(test-is "regexp 1" (regexp-match #/bc+/ "abcdef") '("bc"))
(test-is "regexp 2"
         (regexp-match #/^(http:\/\/)([a-zA-Z0-9\.-]+)(:([0-9]+))?\/?$/
                        url)
         nil)
(test-is "regexp 3" (regexp-match urlre url)
         '("http://golang.org/pkg/regexp/#Regexp.FindStringSubmatch" "http://"
           "http" "golang.org" "" "" "/pkg/regexp/#Regexp.FindStringSubmatch"))

(test-is "regexp ignore-case" (regexp-match #/(?i)e./ "Ein Hase und ein Igel")
         '("Ei"))
(test-is "regexp all-matches" (regexp-match #/e./ "Ein Hase und ein Igel" t)
         '("e " "ei" "el"))
(test-is "regexp ignore+all" (regexp-match #/(?i)e./ "Ein Hase und ein Igel" t)
         '("Ei" "e " "ei" "el"))
(test-is "regexp ign+all+grp" (regexp-match #/(?i)e(.)/ "Ein Hase und ein Igel"
                                            t)
         '("i" " " "i" "l"))
(test-is "regexp longer" (regexp-match #/e.*e/ "Ein Hase und ein Igel")
         '("e und ein Ige"))
(test-is "regexp ungreedy" (regexp-match #/e.*?e/ "Ein Hase und ein Igel")
         '("e und e"))
(test-is "regexp ignore+ungreedy"
         (regexp-match #/(?i)e.*?e/ "Ein Hase und ein Igel")
         '("Ein Hase"))
(test-is "regexp ign+ungr-nl" (regexp-match #/(?i)e.*?e/
                                             "Ein\nHase und ein Igel")
         '("e und e"))
(test-is "regexp ign+ungr+dot" (regexp-match #/(?is)e.*?e/
                                              "Ein\nHase und ein Igel")
         '("Ein\nHase"))
(test-not "regexp -multi" (regexp-match #/^Hase .*$/ "Ein\nHase und\n ein Igel")
          )
(test-not "regexp -mult+ungr"
         (regexp-match #/^Hase .*?$/ "Ein\nHase und\n ein Igel"))
(test-is "regexp multi+ungr"
         (regexp-match #/(?m)^Hase .*$/ "Ein\nHase und\n ein Igel")
         '("Hase und"))

;;; then, matches with the regexp itself as callable

(test-is "#/re/ 1" (#/bc+/ "abcdef") '("bc"))
(test-not "#/re/ 2"
          (#/^(http:\/\/)([a-zA-Z0-9\.-]+)(:([0-9]+))?\/?$/
              url))
(test-is "#/re/ 3" (urlre url)
         '("http://golang.org/pkg/regexp/#Regexp.FindStringSubmatch" "http://"
           "http" "golang.org" "" "" "/pkg/regexp/#Regexp.FindStringSubmatch"))

(test-is "#/re/ ignore-case" (#/(?i)e./ "Ein Hase und ein Igel")
         '("Ei"))
(test-is "#/re/ all-matches" (#/e./ "Ein Hase und ein Igel" t)
         '("e " "ei" "el"))
(test-is "#/re/ ignore+all" (#/(?i)e./ "Ein Hase und ein Igel" t)
         '("Ei" "e " "ei" "el"))
(test-is "#/re/ longer" (#/e.*e/ "Ein Hase und ein Igel")
         '("e und ein Ige"))
(test-is "#/re/ ungreedy" (#/e.*?e/ "Ein Hase und ein Igel")
         '("e und e"))
(test-is "#/re/ ignore+ungreedy"
         (#/(?i)e.*?e/ "Ein Hase und ein Igel")
         '("Ein Hase"))
(test-is "#/re/ ign+ungr-nl" (#/(?i)e.*?e/
                                             "Ein\nHase und ein Igel")
         '("e und e"))
(test-is "#/re/ ign+ungr+dot" (#/(?is)e.*?e/
                                              "Ein\nHase und ein Igel")
         '("Ein\nHase"))
(test-is "#/re/ -multi" (#/^Hase .*$/ "Ein\nHase und\n ein Igel")
         '())
(test-is "#/re/ -mult+ungr"
         (#/^Hase .*?$/ "Ein\nHase und\n ein Igel")
         '())
(test-is "#/re/ multi+ungr"
         (#/(?m)^Hase .*?$/ "Ein\nHase und\n ein Igel")
         '("Hase und"))

;; check that #'regexp handles regexps correctly

(test-is "#'regexp string" (let ((re (regexp "^.*$")))
                              (and (regexpp re)
                                   re))
         #/^.*$/)

(test-is "#'regexp bumber" (let ((re (regexp "444")))
                              (and (regexpp re)
                                   re))
         #/444/)

(test-is "#'regexp regexp" (let ((re (regexp #r{^.*$})))
                              (and (regexpp re)
                                   re))
         #/^.*$/)

(test-is "replace 1" (regexp-replace "b(..)" "hubaluba" "c\\1") "hucaluba")
(test-is "replace 2" (regexp-replace "b(.)" "hubaluba" "c\\1") "hucaluca")

(test-is "replace 3" (regexp-replace #/a(..)/ "mango bardy hard schoomaly"
                                    "\\1b")
         "mngbo brdby hrdb schoomlyb")

(test-is "replace 4" (regexp-replace #/a(..)/ "mango bardy hard schoomaly"
                                    "\\1gooey")
         "mnggooeyo brdgooeyy hrdgooey schoomlygooey")
(test-is "replace 5" (regexp-replace #/a(..)/ "mango bardy hard schoomaly"
                                    "gooey\\1")
         "mgooeyngo bgooeyrdy hgooeyrd schoomgooeyly")
(test-is "replace 6" (regexp-replace #/a(..)/ "mango bardy hard schoomaly"
                                    "gooey")
         "mgooeyo bgooeyy hgooey schoomgooey")

(done-testing)
