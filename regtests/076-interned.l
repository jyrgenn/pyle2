(require 'regtests)

(test-is "interned symbol" (id 'l337) (id 'l337))
(test-is "interned string" (id "l337") (id "l337"))
(test-is "interned number" (id 1337) (id 1337))


(done-testing)
