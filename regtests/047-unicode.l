(require 'regtests)

;; the following worked for an invalid code point, but we refuse these
;; already on reading (which is good); I did not yet find a code point
;; that is not a control char and not graphic to fit the intent of
;; this test case

(test-is "inside BMP" (prin1-to-string "gnuddle") "\"gnuddle\"")
(test-is "outside BMP" (prin1-to-string "\U000e01EF") "\"\U000E01EF\"")
(test-is "*far* outside BMP" (prin1-to-string "\U001001EF") "\"\\U001001ef\"")
(test-is "control char" (prin1-to-string "\33") "\"\\x1b\"")
(test-is "big char" (prin1-to-string "\U00027777") "\"𧝷\"")
(test-is "I like Chinese" "于尔根尼克尔森" "于尔根尼克尔森")
(test-is "pile of poo" "\U0001f4a9" "💩")

(test-is "bell character" (prin1-to-string "\x07a") "\"\\aa\"")

(test-is "bell character '\\a'" (prin1-to-string "\a") "\"\\a\"")
(test-is "backspace '\\b'" (prin1-to-string "\b") "\"\\b\"")
(test-is "form feed '\\f'" (prin1-to-string "\f") "\"\\f\"")
(test-is "newline '\\n'" (prin1-to-string "\n") "\"\\n\"")
(test-is "carriage return '\\r'" (prin1-to-string "\r") "\"\\r\"")
(test-is "tabulator '\\t'" (prin1-to-string "\t") "\"\\t\"")
(test-is "vertical tab '\\v'" (prin1-to-string "\v") "\"\\v\"")
(test-is "double quote '\\\"'" (prin1-to-string "\"") "\"\\\"\"")
(test-is "backslash '\\\'" (prin1-to-string "\\") "\"\\\\\"")



(done-testing)
