(require 'regtests)

(test-is "shell 1" (shell-command "ls -d regte*" t) "regtests\n")
(test-err "shell 2" (shell-command "ls -d regte" t) #/ such file /)
(test-is "shell 3" (shell-command ":" t) "")
(done-testing)
