(require 'regtests)

(test-is "elements l 0" (elements ()) nil)
(test-is "elements l 1" (elements '(123)) '(123))
(test-is "elements l n" (elements '(123 456 789)) '(123 456 789))

(test-is "elements v 0" (elements #()) nil)
(test-is "elements v 1" (elements #(123)) '(123))
(test-is "elements v n" (elements #(123 456 789)) '(123 456 789))


(done-testing)
