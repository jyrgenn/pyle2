#!/usr/bin/env python3
# read the dependency atoms from print-lcore-deps.l and print the DOT graph

import os
import sys


defined_in = {}                         # sym => fname
uses = {}                               # fname => { syms }


def defines_sym(fname, sym):
    if sym in defined_in:
        print(f"WARN: {sym} already in {defined_in[sym]}, now in {fname}")
    defined_in[sym] = fname

def uses_sym(fname, sym):
    sym_set = uses.get(fname) or set()
    sym_set.add(sym)
    uses[fname] = sym_set


for line in sys.stdin:
    tag, fname, sym = line.split()
    dict(define=defines_sym, use=uses_sym)[tag](os.path.basename(fname), sym)

edges = set()
for fname, syms in uses.items():
    for sym in syms:
        defd = defined_in.get(sym)
        if defd and defd != fname:
            # edge = (fname, sym, defd)
            edge = (fname, defd)
            edges.add(edge)


print("digraph {")
for edge in edges:
    print('"', '" -> "'.join(edge), '"', sep="")
print("}")
