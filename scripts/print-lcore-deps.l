#!/usr/bin/env lingo

(defvar do-blab nil)

(let (((arg1 . rest) sys:args))
  (when (eq arg1 "-v")
    (setf do-blab t)
    (setf sys:args rest)))

(defun blab (format &rest args)
  (when do-blab
    (apply #'format t format args)))
  

(defvar def-place #:() "def => file")

(defun defines (fname def indent)
  (format t "define %v %v\n" fname def)
  (blab "%v- def: %v:%v\n" indent fname def)
  (def-place def fname))

(defvar sym-uses #:() "file => { sym => t, ...}")

(defun uses (fname sym indent)
  (blab "%v- use: %v:%v\n" indent fname sym)
  (format t "use %v %v\n" fname sym)
  (let ((sym-set (or (sym-uses fname) #:())))
    (sym-set sym t)
    (sym-uses fname sym-set)))

(defun stem (fname)
  (cadr (#r{^lcore/(.*).l$} fname)))


(defun handle-expr (e fname &optional (indent ""))
  (blab "%vhandle %v\n" indent e)
  (when (and e (listp e))
    (let (((head . rest) e))
      (cond ((member head '(defun defmacro))
             (uses fname head indent)
             (let (((defname params . bodyforms) rest))
               (defines fname defname indent)
               (dolist (form bodyforms)
                 (handle-expr form fname (string indent "  ")))))
            ((member head '(let let*))
             (let (((defs . bodyforms) rest))
               (if (listp defs)
                   (dolist (def defs)
                     (when (listp def)
                       (handle-expr (cadr def) fname (string indent "  ")))))
               (dolist (form bodyforms)
                 (handle-expr form fname (string indent "  ")))))
            ((symbolp head)             ;function call
             (uses fname head indent)
             (dolist (arg rest)
               (handle-expr arg fname (string indent "  "))))
            (t (dolist (expr e)
                 (handle-expr expr fname (string indent "  "))))))))
                 
(defun print-graph ()
  (format t "digraph {\n")
  (dolist (pair (table-pairs sym-uses))
    (let (((fname . syms) pair))
      (dolist (sym (table-keys syms))
        (let ((in (def-place sym)))
          (when in
            (format t "    \"%v\" (%v)-> \"%v\"\n"
                    (stem fname) sym (stem in)))))))
  (format t "}\n"))
  
(defun read-or-throw-eof (port)
  (let ((result (errset (read port) nil)))
    (if (null result)
        (throw 'eof nil)
      (car result))))

(defun readin ()
  (unless sys:args
    (error "no file name arguments"))
  (dolist (fname sys:args)
    (catch 'eof
      (with-open-file (f fname)
        (blab "reading %v\n" fname)
        (while t
          (handle-expr (read-or-throw-eof f) fname "  "))))))
  
(defun showdefs ()
  (dolist (def (sort (table-keys def-place) #'<))
    (format t "%v : %v\n" def (def-place def))))

(defun showuses ()
  (dolist (file (sort (table-keys sym-uses) #'<))
    (format t "%v uses\n" file)
    (let ((uses (sym-uses file)))
      (dolist (use (sort (table-keys uses) #'<))
        (format t "  - %v\n" use)))))

;; (print def-place)
;; (print sym-uses)
;; (print 't)

(readin)
;; (showdefs)
;; (showuses)
